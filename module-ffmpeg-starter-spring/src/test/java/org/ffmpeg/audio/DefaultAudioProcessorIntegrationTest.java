package org.ffmpeg.audio;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessStatus;
import org.ffmpeg.api.multimedia.MultiMediaRequest;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.io.FFmpegFileSystem.createTempFile;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.SUCCESS;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.SUCCESS_WITH_WARNINGS;
import static org.ffmpeg.asserter.MultiMediaAsserter.assertMultiMedia;

public class DefaultAudioProcessorIntegrationTest extends AbstractSpringIntegrationTest {

    private AudioProcessor<FFmpegProcessRequest, FFmpegProcessResponse> audioProcessor;

    @Before
    public void before() {
        audioProcessor = getContext().getAudioProcessor()
                .orElseThrow(() -> new UnsupportedOperationException("No audio processor bean registed !"));
    }

    @Test
    public void whenRetrievingAudioCodecs_theReturnSetOfCodecs() {
        //act
        Set<Codec> result = audioProcessor.getAudioCodecs();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.AUDIO)).isTrue();
    }

    @Test
    public void whenRetrievingAudioDecoders_theReturnSetOfDecoders() {
        //act
        Set<Decoder> result = audioProcessor.getAudioDecoders();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.AUDIO)).isTrue();
    }

    @Test
    public void whenRetrievingAudioEncoders_theReturnSetOfEncoders() {
        //act
        Set<Encoder> result = audioProcessor.getAudioEncoders();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.AUDIO)).isTrue();
    }

    @Test
    public void givenAudioFile_whenProcessing_thenReturnSuccess() {
        //prepare
        DefaultFFmpegProcessRequest request = createDefaultAudioProcessRequest()
                .addGlobalOption("-vsync", 2)
                .build();

        //act
        Optional<FFmpegProcessResponse> response = audioProcessor.process(request);

        //check
        verifyResponse(response, SUCCESS);
        verifyOutput(request);
    }

    @Ignore("FIXME")
    @Test
    public void givenAudioFile_whenProcessing_thenReturnSuccessWithWarnings() {
        //prepare
        DefaultFFmpegProcessRequest request = createDefaultAudioProcessRequest().build();

        //act
        Optional<FFmpegProcessResponse> response = audioProcessor.process(request);

        //check
        verifyResponse(response, SUCCESS_WITH_WARNINGS);
        verifyOutput(request);
    }

    private DefaultFFmpegProcessRequest.DefaultFFmpegProcessRequestBuilder createDefaultAudioProcessRequest() {
        return DefaultFFmpegProcessRequest.builder()
                .source(getDefaultAudioSample())
                .target(createTempFile("test_", ".mp3"))
                .overwriteOutputFiles(true)
                .attributes(AudioProcessAttributes.builder()
                        .format("mp3")
                        .audioAttributes(AudioAttributes.builder()
                                .bitRate(320)
                                .sampleRate(44100)
                                .channels(2)
                                .codec("mp3")
                                .build()
                        ).build()
                );
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private void verifyResponse(Optional<FFmpegProcessResponse> response, FFmpegProcessStatus successWithWarnings) {
        assertThat(response.isPresent()).isTrue();
        assertThat(response.get().getStatus()).isEqualTo(successWithWarnings);
        assertThat(response.get().getMessages()).isEmpty();
    }

    private void verifyOutput(DefaultFFmpegProcessRequest request) {
        MultiMediaRequest multiMediaRequest = DefaultMultiMediaRequest.builder().resource(request.getTarget()).build();
        Optional<MultiMediaResponse> info = getContext().getPlatform().getFramework().getMultiMediaParser().parse(multiMediaRequest);
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(320)
                .hasDuration(39920)
                .hasFormat("mp3")
                .hasStreamCount(1)
                .containsStream(AudioStream.builder()
                        .index(0)
                        .codecName("mp3")
                        .codecType(CodecType.AUDIO)
                        .channels(2)
                        .sampleRate(44100)
                        .build()
                );
    }
}