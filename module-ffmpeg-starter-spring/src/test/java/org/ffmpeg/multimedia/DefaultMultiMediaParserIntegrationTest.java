package org.ffmpeg.multimedia;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.api.multimedia.MultiMediaRequest;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.audio.AudioStream;
import org.ffmpeg.video.VideoSize;
import org.ffmpeg.video.VideoStream;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.asserter.MultiMediaAsserter.assertMultiMedia;

public class DefaultMultiMediaParserIntegrationTest extends AbstractSpringIntegrationTest {

    private MultiMediaParser<MultiMediaRequest, MultiMediaResponse> multiMediaParser;

    @Before
    public void before() {
        multiMediaParser = getContext().getPlatform().getFramework().getMultiMediaParser();
    }

    @Test
    public void givenAudioFile_whenExtractingMultiMediaInfo_thenReturnInfo() {
        //prepare
        DefaultMultiMediaRequest request = newRequest(getDefaultAudioSample());

        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(request);

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(128)
                .hasDuration(39880)
                .hasFormat("flac")
                .hasStreamCount(1)
                .containsStream(AudioStream.builder()
                        .index(0)
                        .codecName("flac")
                        .codecType(CodecType.AUDIO)
                        .channels(2)
                        .sampleRate(96000)
                        .build()
                );
    }

    @Test
    public void givenVideoFile_whenExtractingMultiMediaInfo_thenReturnInfo() {
        //prepare
        DefaultMultiMediaRequest request = newRequest(getDefauktVideoSample());

        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(request);

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(200)
                .hasDuration(3140)
                .hasFormat("avi")
                .hasStreamCount(1)
                .containsStream(VideoStream.builder()
                        .index(0)
                        .codecName("mpeg4 (FMP4 / 0x34504D46)")
                        .codecType(CodecType.VIDEO)
                        .frameRate(35.0f)
                        .size(new VideoSize(256, 240))
                        .build()
                );
    }

    private DefaultMultiMediaRequest newRequest(Path resource) {
        return DefaultMultiMediaRequest.builder().resource(resource).build();
    }
}