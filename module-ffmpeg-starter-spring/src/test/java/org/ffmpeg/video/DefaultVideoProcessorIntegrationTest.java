package org.ffmpeg.video;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.multimedia.MultiMediaRequest;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.io.FFmpegFileSystem.createTempFile;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.SUCCESS;

public class DefaultVideoProcessorIntegrationTest extends AbstractSpringIntegrationTest {

    private VideoProcessor<FFmpegProcessRequest, FFmpegProcessResponse> videoProcessor;

    @Before
    public void before() {
        videoProcessor = getContext().getVideoProcessor()
                .orElseThrow(() -> new UnsupportedOperationException("No video processor bean registed !"));
    }

    @Test
    public void whenRetrievingVideoCodecs_theReturnSetOfCodecs() {
        //act
        Set<Codec> result = videoProcessor.getVideoCodecs();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.VIDEO));
    }

    @Test
    public void whenRetrievingVideoDecoders_theReturnSetOfDecoders() {
        //act
        Set<Decoder> result = videoProcessor.getVideoDecoders();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.VIDEO));
    }

    @Test
    public void whenRetrievingVideoEncoders_theReturnSetOfEncoders() {
        //act
        Set<Encoder> result = videoProcessor.getVideoEncoders();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result.stream().allMatch(value -> value.getCodecType() == CodecType.VIDEO));
    }

    @Test
    public void givenVideoFile_whenProcessing_thenReturnTrue() {
        Path source = getDefauktVideoSample();
        Path target = createTempFile("test_", ".mkv");

        DefaultFFmpegProcessRequest request = DefaultFFmpegProcessRequest.builder()
                .source(source)
                .target(target)
                .overwriteOutputFiles(true)
                .attributes(VideoProcessAttributes.builder()
                        .format("avi")
                        .videoAttributes(VideoAttributes.builder().codec("copy").build())
                        .build()
                )
                .addGlobalOption("-vsync", 2)
                .build();

        //act
        Optional<FFmpegProcessResponse> response = videoProcessor.process(request);

        //check
        assertThat(response.isPresent()).isTrue();
        assertThat(response.get().getStatus()).isEqualTo(SUCCESS);

        MultiMediaRequest multiMediaRequest = DefaultMultiMediaRequest.builder().resource(target).build();
        Optional<MultiMediaResponse> info = getContext().getPlatform().getFramework().getMultiMediaParser().parse(multiMediaRequest);
        assertThat(info.isPresent()).isTrue();
    }
}