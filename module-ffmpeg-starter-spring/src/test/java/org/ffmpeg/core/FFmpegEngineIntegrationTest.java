package org.ffmpeg.core;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.codec.muxing.demuxer.DeMuxer;
import org.ffmpeg.api.core.codec.muxing.muxer.Muxer;
import org.ffmpeg.api.core.common.Color;
import org.ffmpeg.api.core.common.Device;
import org.ffmpeg.api.core.common.Filter;
import org.ffmpeg.api.core.common.Format;
import org.ffmpeg.api.core.common.HelpMode;
import org.ffmpeg.api.core.common.Layout;
import org.ffmpeg.api.core.common.PixelFormat;
import org.ffmpeg.api.core.common.Protocol;
import org.ffmpeg.api.core.common.SampleFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.codec.CodecType.AUDIO;
import static org.ffmpeg.api.core.codec.CodecType.SUBTITLE;
import static org.ffmpeg.api.core.codec.CodecType.VIDEO;

public class FFmpegEngineIntegrationTest extends AbstractSpringIntegrationTest {

    private FFmpeg ffmpeg;

    @Before
    public void before() {
        ffmpeg = getContext().getPlatform().getFramework();
        assertThat(ffmpeg).isNotNull();
    }

    @Test
    public void getBitStreamFilters() {
        //act
        Set<String> result = ffmpeg.getBitStreamFilters();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result).contains("noise");
    }

    @Test
    public void getBuildConfigurations() {
        //act
        Set<String> result = ffmpeg.getBuildConfigurations();

        //check
        assertThat(result).isNotEmpty();
        assertThat(result).contains("--enable-libx264", "--enable-libx265");
    }

    @Test
    public void getCodecs() {
        assertThat(ffmpeg.getCodecs(AUDIO)).isNotEmpty();
        assertThat(ffmpeg.getCodecs(SUBTITLE)).isNotEmpty();
        assertThat(ffmpeg.getCodecs(VIDEO)).isNotEmpty();
    }

    @Test
    public void getColors() {
        //act
        Set<Color> result = ffmpeg.getColors();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getCpuFlags() {
        //act
        List<String> result = ffmpeg.getCpuFlags("mmx");

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getDecoders() {
        assertThat(ffmpeg.getDecoders(AUDIO)).isNotEmpty();
        assertThat(ffmpeg.getDecoders(SUBTITLE)).isNotEmpty();
        assertThat(ffmpeg.getDecoders(VIDEO)).isNotEmpty();
    }

    @Test
    public void getDemuxers() {
        //act
        Set<DeMuxer> result = ffmpeg.getDemuxers();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getDevices() {
        //act
        Set<Device> result = ffmpeg.getDevices();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getEncoders() {
        assertThat(ffmpeg.getEncoders(AUDIO)).isNotEmpty();
        assertThat(ffmpeg.getEncoders(SUBTITLE)).isNotEmpty();
        assertThat(ffmpeg.getEncoders(VIDEO)).isNotEmpty();
    }

    @Test
    public void getFilters() {
        //act
        Set<Filter> result = ffmpeg.getFilters();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getFormats() {
        //act
        Set<Format> results = ffmpeg.getFormats();

        //check
        assertThat(results).isNotEmpty();
    }

    @Test
    public void getHelp() {
        assertThat(ffmpeg.getHelp()).isNotNull();
        assertThat(ffmpeg.getHelp(HelpMode.FULL)).isNotNull();
        assertThat(ffmpeg.getHelp(HelpMode.LONG)).isNotNull();
    }

    @Test
    public void getLicense() {
        //act
        String result = ffmpeg.getLicense();

        //check
        assertThat(result).isNotNull();
    }

    @Test
    public void getLayouts() {
        //act
        Set<Layout> result = ffmpeg.getLayouts();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getName() {
        //act
        String result = ffmpeg.getName();

        //check
        assertThat(result).isEqualTo("ffmpeg");
    }

    @Test
    public void getMuxers() {
        //act
        Set<Muxer> result = ffmpeg.getMuxers();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getPixelFormats() {
        //act
        Set<PixelFormat> result = ffmpeg.getPixelFormats();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getProtocols_Input() {
        //act
        Set<Protocol> result = ffmpeg.getProtocols(true);

        //check
        assertThat(result).hasSize(31);
    }

    @Test
    public void getProtocols_Output() {
        //act
        Set<Protocol> result = ffmpeg.getProtocols(false);

        //check
        assertThat(result).hasSize(26);
    }

    @Test
    public void getReport() {
        //act
        String result = ffmpeg.getReport();

        //check
        assertThat(result).isNotNull();
    }

    @Test
    public void getSampleFormats() {
        //act
        Set<SampleFormat> result = ffmpeg.getSampleFormats();

        //check
        assertThat(result).isNotEmpty();
    }

    @Test
    public void getVersion() {
        //act
        String resultDefault = ffmpeg.getVersion();
        String resultLong = ffmpeg.getVersion(true);
        String resultShort = ffmpeg.getVersion(false);

        //check
        assertThat(resultDefault).isNotNull();
        assertThat(resultLong).isNotNull();
        assertThat(resultShort).isNotNull();
    }
}
