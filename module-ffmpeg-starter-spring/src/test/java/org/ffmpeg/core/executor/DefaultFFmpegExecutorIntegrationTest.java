package org.ffmpeg.core.executor;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultFFmpegExecutorIntegrationTest extends AbstractSpringIntegrationTest {

    private FFmpegExecutor executor;

    @Before
    public void before() {
        executor = getContext().getExecutorFactory().newInstance("-version");
        assertThat(executor).isNotNull();
        assertThat(executor.isExecuted()).isFalse();
    }

    @After
    public void after() {
        executor.destroy();
        assertThat(executor.isExecuted()).isTrue();
    }

    @Test
    public void givenExecutor_whenExecuting_thenProcessNormally() {
        try(FFmpegBufferedReader reader = executor.execute()) {
            assertThat(reader).isNotNull();
            assertThat(executor.isExecuted()).isTrue();
            assertThat(executor.isRedirectErrorStream()).isTrue();
            assertThat(executor.getExecutorPath()).isNotNull();
            assertThat(executor.getCommands()).isNotEmpty();
            assertThat(executor.getErrorStream().isPresent()).isTrue();
            assertThat(executor.getInputStream().isPresent()).isTrue();
            assertThat(executor.getOutputStream().isPresent()).isTrue();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}