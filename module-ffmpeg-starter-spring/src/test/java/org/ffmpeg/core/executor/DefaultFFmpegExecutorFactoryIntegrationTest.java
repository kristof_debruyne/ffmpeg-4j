package org.ffmpeg.core.executor;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultFFmpegExecutorFactoryIntegrationTest extends AbstractSpringIntegrationTest {

    @Test
    public void givenExecutorFactory_whenCreatingNewInstance_thenReturnNewInstance() {
        //act
        FFmpegExecutor executor = getContext().getExecutorFactory().newInstance(getExecutorPath(), "-version");

        //check
        assertThat(executor).isNotNull();
        assertThat(executor.getExecutorPath()).isNotNull();
        assertThat(executor.isRedirectErrorStream()).isTrue();
        assertThat(executor.getCommands()).isNotEmpty();
        assertThat(executor.getCommands()).contains("-version");

        FFmpegExecutor executor2 = getContext().getExecutorFactory().newInstance(getExecutorPath(),"-version");
        assertThat(executor).isNotSameAs(executor2);
    }
}
