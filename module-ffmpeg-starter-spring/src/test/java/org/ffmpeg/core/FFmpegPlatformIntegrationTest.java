package org.ffmpeg.core;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class FFmpegPlatformIntegrationTest extends AbstractSpringIntegrationTest {

    private FFmpegPlatform platform;

    @Before
    public void before() {
        platform = getContext().getPlatform();
    }

    @Test
    public void givenContext_whenCheckingForExistingExtension_thenReturnTrue() {
        //act
        assertThat(platform.containsExtension("ffprobe")).isTrue();
    }

    @Test
    public void givenContext_whenCheckingForNonExistingExtension_thenReturnTrue() {
        //act & check
        assertThat(platform.containsExtension("bla")).isFalse();
    }

    @Test
    public void givenContext_whenGettingExtensionCount_thenReturnTrue() {
        //act & check
        assertThat(platform.getExtensionCount()).isGreaterThan(0);
    }

    @Test
    public void givenContext_whenGettingExistingExtensionByName_thenReturnExtension() {
        //act
        Optional<FFmpegExtension> extension = platform.getExtension("ffprobe");

        //check
        assertThat(extension).isNotNull();
        assertThat(extension.isPresent()).isTrue();
    }

    @Test
    public void givenContext_whenGettingExistingExtensionByName_thenReturnEmptyOptional() {
        //act
        Optional<FFmpegExtension> extension = platform.getExtension("bla");

        //check
        assertThat(extension).isNotNull();
        assertThat(extension.isPresent()).isFalse();
    }

    @Test
    public void givenContext_whenGettingFramework_thenReturnFramework() {
        //act & check
        assertThat(platform.getFramework()).isNotNull();
    }

    @Test
    public void givenContext_whenGettingToolkit_thenReturnToolkit() {
        //act & check
        assertThat(platform.getToolkit()).isNotNull();
    }
}
