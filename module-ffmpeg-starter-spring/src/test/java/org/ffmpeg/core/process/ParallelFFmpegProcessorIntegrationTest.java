package org.ffmpeg.core.process;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subscribers.TestSubscriber;
import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.audio.AudioAttributes;
import org.ffmpeg.audio.AudioProcessAttributes;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.ffmpeg.api.core.io.FFmpegFileSystem.createTempFile;

@SuppressWarnings("SameParameterValue")
public class ParallelFFmpegProcessorIntegrationTest extends AbstractSpringIntegrationTest {

    private final Scheduler scheduler = Schedulers.computation();
    private final TestSubscriber<FFmpegProcessResponse> subscriber = new TestSubscriber<>();

    private ReactiveFFmpegProcessor<FFmpegProcessRequest, FFmpegProcessResponse> processor;

    @Before
    public void before() {
        processor = getReactiveContext().getProcessor();
    }

    @Test
    public void givenListWithThreeProcessRequests_whenProcesingWithParallelismSetToOne_thenProcessSequential() throws Exception {
        //prepare
        List<DefaultFFmpegProcessRequest> requests = newAudioProcessRequestList(3);

        //act
        processor.process(() -> Flowable.fromIterable(requests), scheduler, 1).subscribe(subscriber);

        //check
        subscriber.await();
        subscriber.assertNoErrors();
        subscriber.assertValueCount(3);
        subscriber.assertComplete();
    }

    @Test
    public void givenListWithNineProcessRequests_whenProcesingWithParallelismSetEqualToAvailableProcessors_thenProcessParallel() throws Exception {
        //prepare
        List<DefaultFFmpegProcessRequest> requests = newAudioProcessRequestList(9);

        //act
        processor.process(() -> Flowable.fromIterable(requests), scheduler, processor.getAvailableProcessors()).subscribe(subscriber);

        //check
        subscriber.await();
        subscriber.assertNoErrors();
        subscriber.assertValueCount(9);
        subscriber.assertComplete();
    }

    //*** HELPER METHODS ***//

    private DefaultFFmpegProcessRequest newAudioProcessRequest(Path source) {
        return DefaultFFmpegProcessRequest.builder()
                .source(source)
                .target(createTempFile("test_", ".mp3"))
                .overwriteOutputFiles(true)
                .attributes(AudioProcessAttributes.builder()
                        .format("mp3")
                        .audioAttributes(AudioAttributes.builder()
                                .bitRate(320)
                                .sampleRate(44100)
                                .channels(2)
                                .codec("mp3")
                                .build()
                        ).build()
                )
                .addGlobalOption("-vsync", 2)
                .build();
    }

    private List<DefaultFFmpegProcessRequest> newAudioProcessRequestList(int count) {
        return IntStream.range(0, count)
                .mapToObj(index -> newAudioProcessRequest(resolveDataResource(String.format("audio/sample%d.flac", ++index))))
                .collect(Collectors.toList());
    }
}
