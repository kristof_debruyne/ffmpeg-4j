package org.ffmpeg.context;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.core.common.About;
import org.ffmpeg.api.core.executor.FFmpegExecutorResponse;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.subtitle.processor.SubtitleProcessor;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class FFmpegContextIntegrationTest extends AbstractSpringIntegrationTest {

    @Test
    public void givenContext_whenGettingAboutInformation_thenReturnInformation() {
        //act
        About about = getContext().getAboutInformation();

        //check
        assertThat(about).isNotNull();
    }

    @Test
    public void givenArgument_whenExecuting_thenReturnResult() {
        //act
        FFmpegExecutorResponse result = getContext().run("-version");

        //check
        assertThat(result).isNotNull();
    }

    @Test
    public void givenContext_whenGettingExecutorFactory_thenReturnFactory() {
        //act & check
        assertThat(getContext().getExecutorFactory()).isNotNull();
    }

    @Test
    public void givenContext_whenGettingProcessorFactory_thenReturnFactory() {
        //act & check
        assertThat(getContext().getProcessorFactory()).isNotNull();
    }

    @Test
    public void givenContext_whenGettingAudioProcessor_thenReturnProcessor() {
        //act
        Optional<AudioProcessor<FFmpegProcessRequest, FFmpegProcessResponse>> audioProcessor = getContext().getAudioProcessor();

        //check
        assertThat(audioProcessor).isNotNull();
        assertThat(audioProcessor.isPresent()).isTrue();
    }

    @Test
    public void givenContext_whenGettingSubtitleProcessor_thenReturnProcessor() {
        //act
        Optional<SubtitleProcessor<FFmpegProcessRequest, FFmpegProcessResponse>> subtitleProcessor = getContext().getSubtitleProcessor();

        //check
        assertThat(subtitleProcessor).isNotNull();
        assertThat(subtitleProcessor.isPresent()).isTrue();
    }

    @Test
    public void givenContext_whenGettingVideoProcessor_thenThrowException() {
        //act
        Optional<VideoProcessor<FFmpegProcessRequest, FFmpegProcessResponse>> videoProcessor = getContext().getVideoProcessor();

        //check
        assertThat(videoProcessor).isNotNull();
        assertThat(videoProcessor.isPresent()).isTrue();
    }
}
