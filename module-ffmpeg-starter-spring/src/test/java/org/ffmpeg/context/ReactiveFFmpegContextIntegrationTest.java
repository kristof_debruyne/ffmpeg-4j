package org.ffmpeg.context;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ReactiveFFmpegContextIntegrationTest extends AbstractSpringIntegrationTest {

    @Test
    public void givenReactiveContext_whenAccessing_thenReturnNonNullBeans() {
        assertThat(getReactiveContext()).isNotNull();
        assertThat(getReactiveContext().getContext()).isNotNull();
        assertThat(getReactiveContext().getProcessor()).isNotNull();
    }
}
