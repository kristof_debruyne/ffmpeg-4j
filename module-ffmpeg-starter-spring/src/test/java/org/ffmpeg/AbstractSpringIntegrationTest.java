package org.ffmpeg;

import lombok.Getter;
import lombok.NonNull;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.rx.api.context.ReactiveFFmpegContext;
import org.ffmpeg.spring.config.EnableFFmpeg;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ActiveProfiles("integration-test")
@ContextConfiguration(classes = AbstractSpringIntegrationTest.FFmpegIntegrationConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractSpringIntegrationTest extends AbstractFFmpegTest {

    @Autowired
    @NonNull
    @Getter
    private FFmpegContext context;

    @Autowired
    @NonNull
    @Getter
    private ReactiveFFmpegContext reactiveContext;

    @EnableFFmpeg
    @Configuration
    static class FFmpegIntegrationConfiguration { }
}