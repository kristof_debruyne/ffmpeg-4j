package org.ffmpeg.extension.probe;

import org.ffmpeg.AbstractSpringIntegrationTest;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.exception.FFmpegConfigurationException;
import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.api.multimedia.MultiMediaAware;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.api.multimedia.MultiMediaRequest;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.audio.AudioStream;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;
import org.ffmpeg.extension.probe.multimedia.ProbeRawMultiMediaResponse;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.ffmpeg.video.VideoSize;
import org.ffmpeg.video.VideoStream;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.asserter.MultiMediaAsserter.assertMultiMedia;

public class ProbeMultiMediaParserIntegrationTest extends AbstractSpringIntegrationTest {

    private MultiMediaParser<MultiMediaRequest, MultiMediaResponse> multiMediaParser;

    @Before
    public void before() {
        multiMediaParser = getContext().getPlatform().getProbeFramework()
                .map(MultiMediaAware::getMultiMediaParser)
                .orElseThrow(() -> new UnsupportedOperationException("No probe multimedia parser bean registed !"));
    }

    //*** UNSUPPORTED (DEFAULT / CSV / FLAT / INI ) ***//

    @Test(expected = FFmpegConfigurationException.class)
    public void givenAudioFile_whenExtractingMultiMediaInfoFromDefault_thenThrowException() {
        multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.DEFAULT).build());
    }

    @Test(expected = FFmpegConfigurationException.class)
    public void givenAudioFile_whenExtractingMultiMediaInfoFromCsv_thenThrowException() {
        multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.CSV).build());
    }

    @Test(expected = FFmpegConfigurationException.class)
    public void givenAudioFile_whenExtractingMultiMediaInfoFromFlat_thenThrowException() {
        multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.FLAT).build());
    }

    @Test(expected = FFmpegConfigurationException.class)
    public void givenAudioFile_whenExtractingMultiMediaInfoFromIni_thenThrowException() {
        multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.INI).build());
    }

    //*** RAW ***//

    @Test
    public void givenAudioFile_whenExtractingRawMultiMediaInfoFromXml_thenReturnRawInfo() {
        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.XML).rawResponse(true).build());

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get()).isInstanceOf(ProbeRawMultiMediaResponse.class);
    }

    //*** SUPPORTED (JSON) ***//

    @Test
    public void givenAudioFile_whenExtractingMultiMediaInfoFromJson_thenReturnInfo() {
        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.JSON).build());

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(3340)
                .hasDuration(39876)
                .hasFormat("flac")
                .hasStreamCount(2)
                .containsStream(AudioStream.builder()
                        .index(0)
                        .codecName("flac")
                        .codecType(CodecType.AUDIO)
                        .channels(2)
                        .sampleRate(96000)
                        .build()
                );
    }

    @Test
    public void givenVideoFile_whenExtractingMultiMediaInfoFromJson_thenReturnInfo() {
        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(newRequest(getDefauktVideoSample(), OutputFormat.JSON).build());

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(736)
                .hasDuration(3142)
                .hasFormat("avi")
                .hasStreamCount(1)
                .containsStream(VideoStream.builder()
                        .index(0)
                        .codecName("indeo4")
                        .codecType(CodecType.VIDEO)
                        .frameRate(35.0f)
                        .size(new VideoSize(256, 240))
                        .build()
                );
    }

    //*** SUPPORTED (XML) ***//

    @Test
    public void givenAudioFile_whenExtractingMultiMediaInfoFromXml_thenReturnInfo() {
        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(newRequest(getDefaultAudioSample(), OutputFormat.XML).build());

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(3340)
                .hasDuration(39876)
                .hasFormat("flac")
                .hasStreamCount(2)
                .containsStream(AudioStream.builder()
                        .index(0)
                        .codecName("flac")
                        .codecType(CodecType.AUDIO)
                        .channels(2)
                        .sampleRate(96000)
                        .build()
                );
    }

    @Test
    public void givenVideoFile_whenExtractingMultiMediaInfoFromXml_thenReturnInfo() {
        //act
        Optional<MultiMediaResponse> info = multiMediaParser.parse(newRequest(getDefauktVideoSample(), OutputFormat.XML).build());

        //check
        assertThat(info.isPresent()).isTrue();
        assertMultiMedia(info.get())
                .isInstanceOf(DefaultMultiMediaResponse.class)
                .hasBitRate(736)
                .hasDuration(3142)
                .hasFormat("avi")
                .hasStreamCount(1)
                .containsStream(VideoStream.builder()
                        .index(0)
                        .codecName("indeo4")
                        .codecType(CodecType.VIDEO)
                        .frameRate(35.0f)
                        .size(new VideoSize(256, 240))
                        .build()
                );
    }

    private ProbeMultiMediaRequest.ProbeMultiMediaRequestBuilder newRequest(Path resource, OutputFormat outputFormat) {
        return ProbeMultiMediaRequest.builder().rawResponse(false).resource(resource).outputFormat(outputFormat);
    }
}