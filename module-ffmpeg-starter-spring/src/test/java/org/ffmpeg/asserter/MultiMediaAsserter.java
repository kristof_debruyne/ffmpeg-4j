package org.ffmpeg.asserter;

import lombok.NonNull;
import org.assertj.core.api.AbstractAssert;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;

import javax.annotation.Nonnull;

import static org.assertj.core.api.Assertions.assertThat;

public class MultiMediaAsserter extends AbstractAssert<MultiMediaAsserter, MultiMediaResponse> {

    private MultiMediaAsserter(MultiMediaResponse response, Class<?> selfType) { super(response, selfType); }

    @Nonnull
    public static MultiMediaAsserter assertMultiMedia(@NonNull MultiMediaResponse response) {
        return new MultiMediaAsserter(response, MultiMediaAsserter.class);
    }

    @Nonnull
    public MultiMediaAsserter hasBitRate(int bitRate) {
        DefaultMultiMediaResponse response = assertDefaultMultiMediaResponse();
        assertThat(response.getBody().getBitRate()).isEqualTo(bitRate);
        return this;
    }

    @Nonnull
    public MultiMediaAsserter hasFormat(String format) {
        DefaultMultiMediaResponse response = assertDefaultMultiMediaResponse();
        assertThat(response.getBody().getFormat()).isEqualTo(format);
        return this;
    }

    @Nonnull
    public MultiMediaAsserter hasDuration(long duration) {
        DefaultMultiMediaResponse response = assertDefaultMultiMediaResponse();
        assertThat(response.getBody().getDuration()).isEqualTo(duration);
        return this;
    }

    @Nonnull
    public MultiMediaAsserter hasStreamCount(int streamCount) {
        DefaultMultiMediaResponse response = assertDefaultMultiMediaResponse();
        assertThat(response.getBody().getStreams()).isNotEmpty();
        assertThat(response.getBody().getStreams().size()).isEqualTo(streamCount);
        return this;
    }

    public <T> MultiMediaAsserter containsStream(@NonNull T stream) {
        DefaultMultiMediaResponse response = assertDefaultMultiMediaResponse();
        assertThat(response.getBody().getStreams()).isNotEmpty();
        assertThat(response.getBody().getStreams().stream().anyMatch(entry -> entry.equals(stream)));
        return this;
    }


    private DefaultMultiMediaResponse assertDefaultMultiMediaResponse() {
        DefaultMultiMediaResponse response = (DefaultMultiMediaResponse) actual;
        assertThat(response.getBody()).isNotNull();
        return response;
    }
}
