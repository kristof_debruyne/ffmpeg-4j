package org.ffmpeg.spring.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.config.FFmpegProperties;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.config.FFplayBootstrapConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;

import javax.annotation.PostConstruct;

import static org.ffmpeg.api.extension.play.common.FFplayConstants.FFPLAY_QUALIFIER;

/**
 * FFplay Spring configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFplayBootstrapConfigurer
 */
@Configuration
@RequiredArgsConstructor
@Slf4j
class FFplayConfiguration implements FFplayBootstrapConfigurer {

    @Getter
    private final FFmpegProperties properties;

    @PostConstruct
    private void init() {
        log.info("Initializing FFPLAY ... Home variable: {}", getPlayHomeVariable());
    }

    @Bean
    @Description("FFplay framework instance for retrieving framework specific information.")
    public FFplay ffplay(@Autowired FFmpegToolkit toolkit,
                         @Autowired(required = false) @Qualifier(FFPLAY_QUALIFIER) FFmpegExecutorFactory executorFactory) {
        return createPlayFramework(executorFactory, toolkit);
    }

    @Bean
    @Qualifier(FFPLAY_QUALIFIER)
    @Description("FFplay executor factory for creating executor instances.")
    public FFmpegExecutorFactory playExecutorFactory() { return createPlayExecutorFactory(); }
}
