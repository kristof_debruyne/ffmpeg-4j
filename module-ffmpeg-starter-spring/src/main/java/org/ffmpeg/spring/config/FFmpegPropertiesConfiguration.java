package org.ffmpeg.spring.config;

import org.ffmpeg.api.config.FFmpegProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.PropertySource;

/**
 * FFmpeg properties configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProperties
 */
@Configuration
@PropertySource(value = "classpath:ffmpeg.properties")
class FFmpegPropertiesConfiguration {

    @Value("${ffmpeg.redirectErrorStream:true}")
    private boolean redirectErrorStream;

    @Value("${ffmpeg.executorPath}")
    private String executorPath;

    @Value("${ffmpeg.playExecutorPath:null}")
    private String playExecutorPath;

    @Value("${ffmpeg.probeExecutorPath:null}")
    private String probeExecutorPath;

    @Value("${ffmpeg.probeXsdPath:/ffprobe.xsd}")
    private String probeXsdPath;

    @Value("${ffmpeg.probeStrictXmlValidation:false}")
    private boolean probeStrictXmlValidation;

    @Bean
    @Description("FFmpeg properties (see ffmpeg.properties)")
    public FFmpegProperties properties() {
        final DefaultFFmpegProperties properties = new DefaultFFmpegProperties();
        properties.setRedirectErrorStream(redirectErrorStream);
        properties.setExecutorPath(executorPath); //FFMPEG
        properties.setPlayExecutorPath(playExecutorPath); //FFPLAY
        properties.setProbeExecutorPath(probeExecutorPath); //FFPROBE
        properties.setProbeXsdPath(probeXsdPath); //FFPROBE
        properties.setProbeStrictXmlValidation(probeStrictXmlValidation); //FFPROBE
        return properties;
    }
}
