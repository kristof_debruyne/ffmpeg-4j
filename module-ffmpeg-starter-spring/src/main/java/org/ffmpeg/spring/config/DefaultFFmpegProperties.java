package org.ffmpeg.spring.config;

import lombok.Data;
import org.ffmpeg.api.config.FFmpegProperties;

@Data
public class DefaultFFmpegProperties implements FFmpegProperties {

    private boolean redirectErrorStream;
    private boolean probeStrictXmlValidation;
    private String executorPath;
    private long bufferTimeInSeconds;
    private String playExecutorPath;
    private String probeExecutorPath;
    private String probeXsdPath;
}
