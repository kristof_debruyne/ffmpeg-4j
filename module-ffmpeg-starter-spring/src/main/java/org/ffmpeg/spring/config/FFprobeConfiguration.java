package org.ffmpeg.spring.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.config.FFmpegProperties;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;
import org.ffmpeg.api.extension.probe.output.OutputFormatParserRegistry;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.config.FFprobeBootstrapConfigurer;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;

import static org.ffmpeg.api.extension.probe.common.FFprobeConstants.FFPROBE_QUALIFIER;

/**
 * FFprobe Spring configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFprobeBootstrapConfigurer
 */
@Configuration
@RequiredArgsConstructor
@Slf4j
class FFprobeConfiguration implements FFprobeBootstrapConfigurer {

    @Getter
    @NonNull
    private final FFmpegProperties properties;

    @PostConstruct
    private void init() {
        log.info("Initializing FFPROBE ... Home variable: {}", getProbeHomeVariable());
    }

    @Bean
    @Qualifier(FFPROBE_QUALIFIER)
    @Description("FFprobe multimedia info parser (extended).")
    public MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> probeMultiMediaParser(
            @Autowired(required = false) @Qualifier(FFPROBE_QUALIFIER) FFmpegExecutorFactory executorFactory,
            @Autowired OutputFormatParserRegistry<OutputFormatParser> registry) {
        return createProbeMultiMediaParser(executorFactory, registry);
    }

    @Bean
    @Description("FFprobe framework instance for retrieving framework specific information.")
    public FFprobe ffprobe(@Autowired FFmpegToolkit toolkit,
                           @Autowired(required = false) @Qualifier(FFPROBE_QUALIFIER) FFmpegExecutorFactory executorFactory,
                           @Autowired(required = false) @Qualifier(FFPROBE_QUALIFIER) MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> multiMediaParser) {
        return createProbeFramework(executorFactory, toolkit, multiMediaParser);
    }

    @Bean
    @Qualifier(FFPROBE_QUALIFIER)
    @Description("FFprobe executor factory for creating executor instances.")
    public FFmpegExecutorFactory probeExecutorFactory() { return createProbeExecutorFactory(); }

    @Bean
    @Description("JSON object mapper for JSON output format parser.")
    public ObjectMapper objectMapper() {
        return createObjectMapper();
    }

    @Bean
    @Description("XSD schema to validate XML output format.")
    public Schema xsdSchema() throws Exception { return createSchema(properties.getProbeXsdPath()); }

    @Bean
    @Scope("prototype")
    @Description("XML document builder factory for XML output format parser.")
    public DocumentBuilderFactory documentBuilderFactory(@Autowired(required = false) Schema xsdSchema) { return createDocumentBuilderFactory(xsdSchema); }

    @Bean
    @Description("Output format parser registry containing output format parsers (JSON, XML, ...)")
    public OutputFormatParserRegistry<OutputFormatParser> outputFormatParserRegistry(@Autowired FFmpegToolkit toolkit,
                                                                                     @Autowired ObjectMapper objectMapper,
                                                                                     @Autowired(required = false) DocumentBuilderFactory documentBuilderFactory,
                                                                                     @Autowired(required = false) Schema xsdSchema) {
        return createOutputFormatParserRegistry(toolkit, objectMapper, documentBuilderFactory, xsdSchema);
    }
}
