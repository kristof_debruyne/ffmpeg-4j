package org.ffmpeg.spring.config;

import org.ffmpeg.aop.FFmpegAopConfiguration;
import org.ffmpeg.core.spring.FFmpegReactiveConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Enable FFMPEG framework wrapper.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegConfiguration
 */
@Configuration
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({
        FFmpegAopConfiguration.class,
        FFmpegPropertiesConfiguration.class,
        FFmpegConfiguration.class,
        FFmpegReactiveConfiguration.class,
        FFprobeConfiguration.class,
        FFplayConfiguration.class
})
public @interface EnableFFmpeg {

}
