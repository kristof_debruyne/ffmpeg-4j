package org.ffmpeg.api.core.options;

import org.ffmpeg.api.core.options.vo.FFmpegOptionValue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.ANYWHERE;

public class FFmpegOptionValueUnitTest {

    @Rule
    public ExpectedException EXPECTED_EXCEPTION = ExpectedException.none();

    //CONSTRUCTOR (option)

    @Test(expected = NullPointerException.class)
    public void givenNullOption_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null);
    }

    @Test
    public void givenNonNullOption_whenInvokingConstructor_thenCreateObject() {
        //act
        FFmpegOptionValue value = new FFmpegOptionValue(GenericOption.VERSION);

        //check
        assertThat(value).isNotNull();
        assertThat(value.getOption()).isEqualTo(GenericOption.VERSION);
    }

    //CONSTRUCTOR (option + value)

    @Test(expected = NullPointerException.class)
    public void givenNullOptionAndNullValue_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void givenNullOptionAndValue_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null, "/some/path");
    }

    @Test
    public void givenOptionAndValue_whenInvokingConstructor_thenCreateObject() {
        //act
        FFmpegOptionValue value = new FFmpegOptionValue(MainOption.INPUT_SOURCE, "/some/path");

        //check
        assertThat(value).isNotNull();
        assertThat(value.getOption()).isEqualTo(MainOption.INPUT_SOURCE);
        assertThat(value.getLocation()).isEqualTo(ANYWHERE);
        assertThat(value.getValue()).isEqualTo("/some/path");
    }

    //CONSTRUCTOR (option + value + location)

    @Test(expected = NullPointerException.class)
    public void givenNullOptionAndNullValueAndNullLocation_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void givenNullOptionAndValueAndNullLocation_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null, "/some/path", null);
    }

    @Test(expected = NullPointerException.class)
    public void givenNullOptionAndNullValueAndLocation_whenInvokingConstructor_thenThrowException() {
        new FFmpegOptionValue(null, null, FFmpegOptionLocation.GLOBAL);
    }

    @Test
    public void givenOptionAndValueAndAnywhereLocation_whenInvokingConstructor_thenCreateObject() {
        //act
        FFmpegOptionValue value = new FFmpegOptionValue(MainOption.INPUT_SOURCE, "/some/path", ANYWHERE);

        //check
        assertThat(value).isNotNull();
        assertThat(value.getOption()).isEqualTo(MainOption.INPUT_SOURCE);
        assertThat(value.getLocation()).isEqualTo(ANYWHERE);
        assertThat(value.getValue()).isEqualTo("/some/path");
    }

    @Test
    public void givenOptionThatRequiresLocation_whenInvokingConstructor_thenCreateObject() {
        //act
        FFmpegOptionValue value = new FFmpegOptionValue(MainOption.SEEK_POSITION, 0, FFmpegOptionLocation.INPUT);

        //check
        assertThat(value).isNotNull();
        assertThat(value.getOption()).isEqualTo(MainOption.SEEK_POSITION);
        assertThat(value.getLocation()).isEqualTo(FFmpegOptionLocation.INPUT);
        assertThat(value.getValue()).isEqualTo(0);
    }
}