package org.ffmpeg.api.core.options;

import org.ffmpeg.api.core.common.Disposition;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FFmpegOptionValuePredicatesTest extends AbstractFFmpegTest {

    @Test
    public void areKeyValuePairs() {
        assertThat(FFmpegOptionValuePredicates.areKeyValuePairs().test("key=value")).isTrue();
        assertThat(FFmpegOptionValuePredicates.areKeyValuePairs().test("key=value key=value")).isTrue();
        assertThat(FFmpegOptionValuePredicates.areKeyValuePairs().test("test")).isFalse();
    }

    @Test
    public void isDate() {
        assertThat(FFmpegOptionValuePredicates.isDate().test("now")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("2021-01-31 12:30:45.666")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("2021-01-31T12:30:45.666")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("2021-01-31 12:30:45.666Z")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("2021-01-31T12:30:45.666Z")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("20210131 123045.666")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("20210131T123045.666")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("20210131 123045.666Z")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDate().test("20210131T123045.666Z")).isTrue();
    }

    @Test
    public void isTime() {
        assertThat(FFmpegOptionValuePredicates.isTime().test(0)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test(0.2)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("55")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("0.2")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("23.189")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("200s")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("200ms")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("200us")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isTime().test("12:03:45")).isTrue();
    }

    @Test
    public void isDecimalWithoutStreamSpecifier() {
        assertThat(FFmpegOptionValuePredicates.isDecimal().test(1)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDecimal().test(1L)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDecimal().test(0.0D)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDecimal().test(0.0F)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDecimal().test("test")).isFalse();
    }

    @Test
    public void isDecimalWithStreamSpecifier() {
        assertThat(FFmpegOptionValuePredicates.isDecimal(true).test(":a:1 50.2")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDecimal(true).test(":a:1 test")).isFalse();
    }

    @Test
    public void isDevice() {
        assertThat(FFmpegOptionValuePredicates.isDevice().test(1)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDevice().test(1L)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDevice().test(0.0D)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDevice().test(0.0F)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDevice().test("test")).isFalse();
        assertThat(FFmpegOptionValuePredicates.isDevice().test("test,key=value,key=value")).isTrue();
    }

    @Test
    public void isDisposition() {
        assertThat(FFmpegOptionValuePredicates.isDisposition().test(Disposition.DEFAULT.getValue())).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDisposition().test(":a:1 default")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isDisposition().test(":a:1 test")).isFalse();
    }

    @Test
    public void isNumberWithoutStreamSpecifier() {
        assertThat(FFmpegOptionValuePredicates.isNumber().test(1)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isNumber().test(1L)).isTrue();
        assertThat(FFmpegOptionValuePredicates.isNumber().test(0.0D)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isNumber().test(0.0F)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isNumber().test("test")).isFalse();
    }

    @Test
    public void isNumberWithStreamSpecifier() {
        assertThat(FFmpegOptionValuePredicates.isNumber(true).test(":a:1 666")).isTrue();
        assertThat(FFmpegOptionValuePredicates.isNumber(true).test(":a:1 test")).isFalse();
    }

    @Test
    public void isResource() {
        assertThat(FFmpegOptionValuePredicates.isResource().test(getDefaultAudioSample().toAbsolutePath().toString())).isTrue();
        assertThat(FFmpegOptionValuePredicates.isResource().test(getDefaultAudioSample().toAbsolutePath().getParent().toString())).isFalse();
    }

    @Test
    public void isText() {
        assertThat(FFmpegOptionValuePredicates.isText().test(1)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isText().test(1L)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isText().test(0.0D)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isText().test(0.0F)).isFalse();
        assertThat(FFmpegOptionValuePredicates.isText().test("test")).isTrue();
    }
}