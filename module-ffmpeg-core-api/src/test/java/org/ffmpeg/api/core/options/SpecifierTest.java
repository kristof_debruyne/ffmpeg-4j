package org.ffmpeg.api.core.options;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.options.specifier.Specifier.Stream.validateStreamSpecifier;

public class SpecifierTest {

    @Test(expected = NullPointerException.class)
    public void givenStreamSpecifierIsNull_whenValidating_thenReturnFalse() {
        validateStreamSpecifier(null);
    }

    @Test
    public void givenStreamSpecifierIsEmptyString_whenValidating_thenReturnFalse() {
        assertThat(validateStreamSpecifier("")).isFalse();
    }

    @Test
    public void givenStreamSpecifierContainsInvalidType_whenValidating_thenReturnFalse() {
        assertThat(validateStreamSpecifier(":x:1 test")).isFalse();
    }

    @Test
    public void givenStreamSpecifierIsValidSpecifier_whenValidating_thenReturnTrue() {
        assertThat(validateStreamSpecifier(": test")).isTrue();
        assertThat(validateStreamSpecifier(":1 test")).isTrue();
        assertThat(validateStreamSpecifier(":a:1 test")).isTrue();
        assertThat(validateStreamSpecifier(":d:1 test")).isTrue();
        assertThat(validateStreamSpecifier(":s:1 test")).isTrue();
        assertThat(validateStreamSpecifier(":t:1 test")).isTrue();
        assertThat(validateStreamSpecifier(":v:1 test")).isTrue();
        assertThat(validateStreamSpecifier("test")).isTrue();
    }
}