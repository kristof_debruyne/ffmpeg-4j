package org.ffmpeg.api.extension.probe.options;

import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProbeOptionValuePredicatesTest extends AbstractFFmpegTest {

    @Test
    public void isOutputFormat() {
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.DEFAULT.getValue())).isTrue();
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.CSV.getValue())).isTrue();
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.FLAT.getValue())).isTrue();
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.INI.getValue())).isTrue();
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.JSON.getValue())).isTrue();
        assertThat(ProbeOptionVlauePredicates.isOutputFormat().test(OutputFormat.XML.getValue())).isTrue();
    }
}