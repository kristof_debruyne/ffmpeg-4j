package org.ffmpeg.api.extension.probe.multimedia;

import org.ffmpeg.api.multimedia.MultiMediaResponse;

/**
 * Probe multimedia response interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see MultiMediaResponse
 */
public interface ProbeMultiMediaResponse extends MultiMediaResponse {

}