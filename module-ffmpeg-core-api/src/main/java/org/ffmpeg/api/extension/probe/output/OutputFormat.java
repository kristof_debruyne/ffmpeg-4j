package org.ffmpeg.api.extension.probe.output;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum OutputFormat {

    DEFAULT("default") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return asList(
                    OutputFormatOption.of("nk", "nokey", Boolean.class, false),
                    OutputFormatOption.of("nw", "noprint_wrappers", Boolean.class, false)
            );
        }
    },
    CSV("csv") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return asList(
                    OutputFormatOption.of("nk", "nokey", Boolean.class, false),
                    OutputFormatOption.of("s", "item_sep", Character.class, ','),
                    OutputFormatOption.of("e", "escape", String[].class, new String[]{"c", "csv", "none"}),
                    OutputFormatOption.of("p", "print_section", Boolean.class, true)
            );
        }
    },
    FLAT("flat") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return asList(
                    OutputFormatOption.of("h", "hierarchical", Boolean.class, true),
                    OutputFormatOption.of("s", "sep_char", Character.class, '.')
            );
        }
    },
    INI("ini") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return singletonList(OutputFormatOption.of("h", "hierarchical", Boolean.class, true));
        }
    },
    JSON("json") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return singletonList(OutputFormatOption.of("c", "compact", Boolean.class, false));
        }
    },
    XML("xml") {
        @Override
        public List<OutputFormatOption<?>> getOptions() {
            return asList(
                    OutputFormatOption.of("q", "fully_qualified", Boolean.class, false),
                    OutputFormatOption.of("x", "xsd_strict", Boolean.class, false)
            );
        }
    };

    @Getter
    private final String value;

    public abstract List<OutputFormatOption<?>> getOptions();

}