package org.ffmpeg.api.extension;

import org.ffmpeg.api.core.FFmpegEngine;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;

/**
 * FFmpeg extension interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "FFmpeg extension",
        description = "All FFMPEG extension will extend this interface for marcation."
)
public interface FFmpegExtension extends FFmpegEngine {

    /**
     * Gets extension name.
     *
     * @return name (never null)
     */
    @Nonnull
    String getName();
}