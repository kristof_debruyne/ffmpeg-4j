package org.ffmpeg.api.extension.probe.options;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionProperties;
import org.ffmpeg.api.core.options.MainOption;

import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;
import static org.ffmpeg.api.extension.probe.options.ProbeOptionVlauePredicates.isOutputFormat;

/**
 * Supported FFprobe main options.
 * <p>
 * URL: https://www.ffmpeg.org/ffprobe.html#Main-options
 *
 * @author Sikke303
 * @since 1.0
 * @see ComplexFFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum ProbeMainOption implements ComplexFFmpegOption {

    BYTE_BINARY_PREFIX {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-byte_binary_prefix")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    FORCE_FORMAT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return MainOption.FORCE_FORMAT.getProperties();
        }
    },
    INPUT_SOURCE {
        @Override
        public FFmpegOptionProperties getProperties() {
            return MainOption.INPUT_SOURCE.getProperties();
        }
    },
    OUTPUT_FORMAT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-of")
                    .aliases(singletonList("-print_format"))
                    .locations(singletonList(INPUT))
                    .valuePredicate(isOutputFormat())
                    .build();
        }
    },
    PREFIX {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-prefix")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    PRETTY {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-pretty")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SEXAGESIMAL {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sexagesimal")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SECTIONS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sections")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SELECT_STREAMS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-select_streams")
                    .locations(singletonList(INPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    SHOW_CHAPTERS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_chapters")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_DATA {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_data")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_DATA_HASG {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_data_hash")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    SHOW_ENTRIES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_entries")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    SHOW_ERROR {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_error")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_FORMAT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_format")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_FORMAT_ENTRY {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_format_entry")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    SHOW_FRAMES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_frames")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_PACKETS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_packets")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_PROGRAMS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_programs")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    SHOW_STREAMS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-show_streams")
                    .locations(singletonList(INPUT))
                    .build();
        }
    },
    UNIT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-unit")
                    .locations(singletonList(INPUT))
                    .build();
        }
    }
}
