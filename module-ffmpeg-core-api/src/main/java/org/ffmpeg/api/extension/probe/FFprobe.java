package org.ffmpeg.api.extension.probe;

import org.ffmpeg.api.core.FFmpegEngine;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.multimedia.MultiMediaAware;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;

/**
 * FFprobe interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegEngine
 * @see FFmpegExtension
 * @see MultiMediaAware
 */
@FFmpegComponent(
        name = "FFprobe",
        description = "FFprobe is an extension to the FFMPEG framework. It can be used for retrieving multimedia information.",
        link = "https://www.ffmpeg.org/ffprobe.html"
)
public interface FFprobe extends FFmpegExtension, MultiMediaAware {

    @Nonnull
    default String getDocumentationUrl() { return "https://www.ffmpeg.org/ffprobe.html"; }
}