package org.ffmpeg.api.extension.play;

import org.ffmpeg.api.core.FFmpegEngine;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;

/**
 * FFplay interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegEngine
 * @see FFmpegExtension
 */
@FFmpegComponent(
        name = "FFplay",
        description = "FFplay is an extension to the FFMPEG framework. It can be used for playback and recording.",
        link = "https://www.ffmpeg.org/ffplay.html"
)
public interface FFplay extends FFmpegExtension {

    @Nonnull
    default String getDocumentationUrl() { return "https://www.ffmpeg.org/ffplay.html"; }
}