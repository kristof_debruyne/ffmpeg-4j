package org.ffmpeg.api.extension.probe.output;

import javax.annotation.Nonnull;

/**
 * Output format parser registry interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see OutputFormatParser
 * @param <T> parser type
 */
public interface OutputFormatParserRegistry<T extends OutputFormatParser> {

    /**
     * Gets parser for given output format.
     *
     * @param outputFormat output format (required)
     * @return parser (never null)
     */
    @Nonnull
    T getParser(@Nonnull OutputFormat outputFormat);

    /**
     * Registers parser into registry.
     *
     * @param parser parser (required)
     */
    void register(@Nonnull T parser);
}
