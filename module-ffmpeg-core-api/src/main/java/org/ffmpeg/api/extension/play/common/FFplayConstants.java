package org.ffmpeg.api.extension.play.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FFplayConstants {

    public static final String FFPLAY_HOME= "FFPLAY_HOME";
    public static final String FFPLAY_QUALIFIER= "ffplay";
}
