package org.ffmpeg.api.extension.probe.output;

import org.ffmpeg.api.multimedia.MultiMediaResponse;

import javax.annotation.Nonnull;

/**
 * Output format parser interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see OutputFormat
 */
public interface OutputFormatParser {

    /**
     * Gets outout format for this parser.
     *
     * @return output format (never null)
     */
    @Nonnull
    OutputFormat getOutputFormat();

    /**
     * Parses content to multimedia information.
     *
     * @param content content (required)
     * @param <T> multimedia response type
     * @return multimedia response (never null)
     */
    @Nonnull
    <T extends MultiMediaResponse> T parse(@Nonnull String content);
}
