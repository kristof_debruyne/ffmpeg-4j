package org.ffmpeg.api.extension.probe.output;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class OutputFormatOption<T> {

    private final String key;

    private final String alias;

    private final Class<T> type;

    private final T defaultValue;
}
