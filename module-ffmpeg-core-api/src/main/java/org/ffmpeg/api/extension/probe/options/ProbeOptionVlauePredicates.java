package org.ffmpeg.api.extension.probe.options;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.ffmpeg.api.core.options.FFmpegOptionValuePredicates;
import org.ffmpeg.api.extension.probe.output.OutputFormat;

import javax.annotation.Nonnull;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Arrays.stream;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProbeOptionVlauePredicates extends FFmpegOptionValuePredicates {

    private static final Pattern PROBE_OUTPUT_FORMAT_PATTERN = compile("(.+?)(=([a-z ]))?", CASE_INSENSITIVE);

    @Nonnull
    public static Predicate<Object> isOutputFormat() {
        return (Object rawValue) -> {
            if(isText().test(rawValue)) {
                String value = (String) rawValue; //CAST
                Matcher matcher = PROBE_OUTPUT_FORMAT_PATTERN.matcher(value);
                if(matcher.matches()) {
                    return stream(OutputFormat.values()).anyMatch(format -> matcher.group(1).equals(rawValue));
                }
            }
            return false;
        };
    }
}
