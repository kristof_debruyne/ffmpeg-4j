package org.ffmpeg.api.extension.probe.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FFprobeConstants {

    public static final String FFPROBE_HOME= "FFPROBE_HOME";
    public static final String FFPROBE_QUALIFIER= "ffprobe";
}
