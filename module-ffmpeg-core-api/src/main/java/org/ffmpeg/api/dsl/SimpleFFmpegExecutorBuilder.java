package org.ffmpeg.api.dsl;

import org.ffmpeg.api.core.options.SimpleFFmpegOption;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Simple FFMPEG executor builder. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorBuilder
 */
public interface SimpleFFmpegExecutorBuilder extends FFmpegExecutorBuilder {

    @Nonnull
    FFmpegExecutorBuilder registerOption(@Nonnull SimpleFFmpegOption option);

    @Nonnull
    FFmpegExecutorBuilder registerOption(@Nonnull SimpleFFmpegOption option, @Nullable Object value);
}
