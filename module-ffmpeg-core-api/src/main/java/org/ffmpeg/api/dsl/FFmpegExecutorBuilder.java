package org.ffmpeg.api.dsl;

import org.ffmpeg.api.core.common.LogLevel;
import org.ffmpeg.api.core.common.LogMode;
import org.ffmpeg.api.core.executor.FFmpegExecutor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;

/**
 * FFMPEG executor builder interface. <br><br>
 *
 * Easy way to create executor instances. <br><br>
 *
 * There are several flavors of creators: <br>
 * - simple executor: to execute stand alone commands (simple) <br>
 * - complex executor: to execute combined commands (complex) <br>
 * - raw executor: to execute raw commands (for advanced users only) <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutor
 */
public interface FFmpegExecutorBuilder {

    /**
     * Gets executor path.
     *
     * @return executor path (never null)
     */
    @Nullable
    Path getExecutorPath();

    /**
     * Gets output path.
     *
     * @return output path or null
     */
    @Nullable
    String getOutputPath();

    /**
     * Gets commands for execution.
     *
     * @return commands (never null)
     */
    @Nonnull
    String[] getCommands();

    /**
     * Builds executor instance.
     *
     * @return executor (never null)
     */
    @Nonnull
    FFmpegExecutor build();

    /**
     * Sets executor path.
     *
     * @param executorPath executor path (required)
     * @return builder (never null)
     */
    @Nonnull
    FFmpegExecutorBuilder overrideExecutorPath(@Nonnull Path executorPath);

    /**
     * Sets log level.
     *
     * @param logLevel log level (required)
     * @return builder (never null)
     */
    @Nonnull
    FFmpegExecutorBuilder withLogLevel(@Nonnull LogLevel logLevel);

    /**
     * Sets log modes.
     *
     * @param logModes log modes (required)
     * @return builder (never null)
     */
    @Nonnull
    FFmpegExecutorBuilder withLogModes(@Nonnull LogMode ... logModes);

    /**
     * Sets output path.
     *
     * @param outputPath output path (required)
     * @return builder (never null)
     */
    @Nonnull
    FFmpegExecutorBuilder withOutputPath(@Nonnull String outputPath);
}