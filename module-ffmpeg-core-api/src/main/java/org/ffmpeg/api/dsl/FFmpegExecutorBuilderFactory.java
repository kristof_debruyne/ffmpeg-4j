package org.ffmpeg.api.dsl;

import javax.annotation.Nonnull;

/**
 * FFMPEG executor Builder factory interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see org.ffmpeg.api.dsl.FFmpegExecutorBuilder
 */
public interface FFmpegExecutorBuilderFactory {

    /**
     * Creates new executor builder for given type.
     *
     * @param type class type (required)
     * @param <T> return type
     * @return executor builder instance (never null)
     */
    @Nonnull
    <T extends FFmpegExecutorBuilder> T ofType(@Nonnull Class<T> type);
}
