package org.ffmpeg.api.dsl;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Raw FFMPEG executor builder. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorBuilder
 */
@ParametersAreNonnullByDefault
public interface RawFFmpegExecutorBuilder extends FFmpegExecutorBuilder {

    @Nonnull
    FFmpegExecutorBuilder withCommands(@Nonnull String ...commands);
}