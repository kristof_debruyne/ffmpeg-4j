package org.ffmpeg.api.dsl;

import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Complex FFMPEG executor builder. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorBuilder
 */
public interface ComplexFFmpegExecutorBuilder extends FFmpegExecutorBuilder {

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(@Nonnull ComplexFFmpegOption option);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(@Nonnull ComplexFFmpegOption option, @Nonnull FFmpegOptionLocation location);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(@Nonnull ComplexFFmpegOption option, @Nullable Object value);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(@Nonnull ComplexFFmpegOption option, @Nullable Object value, @Nonnull FFmpegOptionLocation location);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nonnull FFmpegOptionLocation location);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nullable Object value);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nullable Object value, @Nonnull FFmpegOptionLocation location);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerGlobalOption(@Nonnull String option, @Nullable Object value);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerInputOption(@Nonnull String option, @Nullable Object value);

    @Nonnull
    ComplexFFmpegExecutorBuilder registerOutputOption(@Nonnull String option, @Nullable Object value);
}