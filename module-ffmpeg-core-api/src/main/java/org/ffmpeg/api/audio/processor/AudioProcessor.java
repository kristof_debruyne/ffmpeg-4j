package org.ffmpeg.api.audio.processor;


import lombok.NonNull;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Audio processor interface. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessor
 */
@FFmpegComponent(
		name = "Audio processor",
		description = "Processor that can handle audio streams."
)
public interface AudioProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> extends FFmpegProcessor<Request, Response> {

	/**
	 * Checks if codec is an audio codec or not.
	 *
	 * @param name name (required)
	 * @return boolean audio codec (true) or not (false)
	 */
	boolean isAudioCodec(@NonNull String name);

	/**
	 * Checks if decoder is an audio decoder or not.
	 *
	 * @param name name (required)
	 * @return boolean audio decoder (true) or not (false)
	 */
	boolean isAudioDecoder(@NonNull String name);

	/**
	 * Checks if encoder is an audio encoder or not.
	 *
	 * @param name name (required)
	 * @return boolean audio encoder (true) or not (false)
	 */
	boolean isAudioEncoder(@NonNull String name);

	/**
	 * Gets available audio codecs. <br>
	 *
	 * @return String[] array with available audio codecs
	 */
	@Nonnull
	Set<Codec> getAudioCodecs();

	/**
	 * Gets available audio decoders. <br>
	 *
	 * @return String[] array with available audio decoders
	 */
	@Nonnull
	Set<Decoder> getAudioDecoders();

	/**
	 * Gets available audio encoders. <br>
	 *
	 * @return String[] array with available audio encoders
	 */
	@Nonnull
	Set<Encoder> getAudioEncoders();
}