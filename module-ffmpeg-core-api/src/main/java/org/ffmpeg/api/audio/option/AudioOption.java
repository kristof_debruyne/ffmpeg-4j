package org.ffmpeg.api.audio.option;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionProperties;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.GLOBAL;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isNumber;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;

/**
 * Supported FFmpeg audio options.
 *
 * URL's:
 * - https://www.ffmpeg.org/ffmpeg.html#Audio-Options
 * - https://www.ffmpeg.org/ffmpeg.html#Advanced-Audio-Options
 *
 * @author Sikke303
 * @since 1.0
 * @see ComplexFFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum AudioOption implements ComplexFFmpegOption {

    BIT_RATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-b:a")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    BLOCK{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-an")
                    .locations(asList(INPUT, OUTPUT))
                    .build();
        }
    },
    CHANNELS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-ac")
                    .locations(asList(INPUT, OUTPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    CODEC{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-acodec")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    FILTER_GRAPH{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-af")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    FRAME{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-aframe")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    SAMPLE_RATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-ar")
                    .locations(asList(INPUT, OUTPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    GUESS_LAYOUT{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-guess_layout_max")
                    .advanced(true)
                    .locations(asList(GLOBAL, INPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    QUALITY{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-aq")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    TAG{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-atag")
                    .advanced(true)
                    .locations(asList(GLOBAL, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    }
}
