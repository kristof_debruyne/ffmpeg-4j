package org.ffmpeg.api.core.process;

import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Predicate;

/**
 * FFmpeg process listener.
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "FFmpeg process listener",
        description = "A listener that will be triggered when process has changed."
)
@FunctionalInterface
public interface FFmpegProcessListener {

    /**
     * Mehtod that will be invoked when the process new line.
     *
     * @param callback callback (optional)
     * @param duration duration (required)
     * @return predicate that checks if process is finished or not
     */
    @Nonnull
    Predicate<String> onProcessLine(@Nullable FFmpegProcessCallback callback, @Nonnegative long duration);
}