package org.ffmpeg.api.core.codec.muxing.demuxer;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.core.codec.muxing.AbstractMuxer;

/**
 * Demuxer container (-demuxers).
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractMuxer
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DeMuxer extends AbstractMuxer {

    private static final long serialVersionUID = 0L;

    @Builder
    private DeMuxer(@NonNull String name, @NonNull String description) {
        super(name, description);
    }
}
