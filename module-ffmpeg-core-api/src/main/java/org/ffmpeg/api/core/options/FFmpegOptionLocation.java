package org.ffmpeg.api.core.options;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Supported option locations.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum FFmpegOptionLocation {

    /**
     * Anywhere scope (place doesn't matter)
     */
    ANYWHERE,
    /**
     * Global scope (goes right after ffmpeg)
     */
    GLOBAL,
    /**
     * Input scope (goes after global and before -i)
     */
    INPUT,
    /**
     * Output scope (goes after -i)
     */
    OUTPUT
}
