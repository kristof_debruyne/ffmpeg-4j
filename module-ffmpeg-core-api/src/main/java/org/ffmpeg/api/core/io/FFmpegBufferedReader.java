package org.ffmpeg.api.core.io;

import lombok.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Buffered reader for FFMPEG operations. <br><br>
 * 
 * A reader extending {@link BufferedReader} for FFMPEG operations. <br><br>
 * 
 * If a line read with {@link #readLine()} is not useful for the calling code, <br>
 * it can be re-inserted in the stream. <br><br>
 * 
 * The same line will be returned again at the next {@link #readLine()} call. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see BufferedReader
 */
public class FFmpegBufferedReader extends BufferedReader {

	private final List<String> lines = new ArrayList<>();

	public FFmpegBufferedReader(@NonNull final Reader reader) {
		super(reader);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String readLine() throws IOException {
		if (lines.size() > 0) {
			return lines.remove(0);
		} else {
			return super.readLine();
		}
	}

	/**
	 * Reloads a line in the stream. <br>
	 * 
	 * @param line line
	 */
	public void reloadLine(@NonNull final String line) {
		lines.add(0, line);
	}
}