package org.ffmpeg.api.core.common;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.ffmpeg.api.core.FFmpegObject;
import org.ffmpeg.api.core.codec.CodecType;

/**
 * Stream container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@AllArgsConstructor
@SuperBuilder
@Getter
@EqualsAndHashCode
@ToString
public abstract class Stream implements FFmpegObject {

    private final int index;
    private final String codecName;
    private final String codecDescription;

    @NonNull
    private final CodecType codecType;
}
