package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported log modes.
 *
 * @author Sikke303
 * @since 1.0
 * @see LogLevel
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum LogMode {

    LEVEL("level"),
    REPEAT("repeat");

    @Getter
    private final String value;
}
