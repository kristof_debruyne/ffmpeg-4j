package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported level levels.
 *
 * @author Sikke303
 * @since 1.0
 * @see LogMode
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum LogLevel {

    QUIET("quiet", "-8"),
    PANIC("panic", "0"),
    FATAL("fatal", "8"),
    ERROR("error", "16"),
    WARNING("warning", "24"),
    INFO("info", "32"),
    VERBOSE("verbose", "40"),
    DEBUG("debug", "48"),
    TRACE("trace", "56");

    @Getter
    private final String level;

    @Getter
    private final String value;
}