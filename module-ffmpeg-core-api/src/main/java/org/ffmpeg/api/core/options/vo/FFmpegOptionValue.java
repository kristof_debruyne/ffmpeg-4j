package org.ffmpeg.api.core.options.vo;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.core.options.FFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.isNull;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.ANYWHERE;

/**
 * Option value container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
@Getter
@ToString(callSuper = true)
public final class FFmpegOptionValue extends AbstractFFmpegOptionValue<FFmpegOption> {

    public FFmpegOptionValue(@NonNull FFmpegOption option) { this(option, null); }

    public FFmpegOptionValue(@NonNull FFmpegOption option, @Nullable Object value) { this(option, value, ANYWHERE); }

    public FFmpegOptionValue(@NonNull FFmpegOption option, @Nullable Object value, @NonNull FFmpegOptionLocation location) {
        super(option, value, location);
    }

    @Nonnull
    @Override
    public String getArgument() {
        return getOption().getProperties().getArgument();
    }

    @Nonnegative
    @Override
    public int getOrder() { return getOption().getProperties().getOrder(); }

    @Nonnull
    @Override
    public String[] toArray() {
        if(isNull(getValue())) {
            return new String[] { getOption().getProperties().getArgument() };
        }
        return new String[] { getOption().getProperties().getArgument(), String.valueOf(getValue())};
    }
}
