package org.ffmpeg.api.core.options;

import lombok.NonNull;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * FFmpeg option interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOptionProperties
 */
public interface FFmpegOption extends FFmpegObject {

    /**
     * Gets option properties.
     *
     * @return option properties (never null)
     */
    @NonNull
    FFmpegOptionProperties getProperties();
}