package org.ffmpeg.api.core.validator;

import javax.annotation.Nonnull;

/**
 * Validator interface. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @param <T> type
 */
public interface FFmpegValidator<T> {

	/**
	 * Gets validator name.
	 *
	 * @return name (never null)
	 */
	@Nonnull
	String getName();

	/**
	 * Validates input.
	 *
	 * @param input input (required)
	 * @throws org.ffmpeg.api.exception.FFmpegValidationException if input isn't valid
	 */
	void validate(@Nonnull T input);
}