package org.ffmpeg.api.core.options.specifier;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

/**
 * Stream specifiers. <br><br>
 *
 * URL: https://www.ffmpeg.org/ffmpeg.html#Stream-specifiers
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Specifier {

    public static class Metadata {
        private static final Pattern METADATA_SPECIFIER_PATTERN = compile("^-([a-z]*?)(|:|:[advst]:\\d+)\\s(.*)$", CASE_INSENSITIVE);

        @Nonnull
        public static Matcher newMetaDataSpecifierMatcher(@NonNull final String value) {
            return METADATA_SPECIFIER_PATTERN.matcher(value);
        }

        /**
         * Validate metadata specifier (per-metadata).
         *
         * @param value value (required)
         * @return boolean valid (true) or not (false)
         */
        public static boolean validateMetaDataSpecifier(@NonNull final String value) {
            return newMetaDataSpecifierMatcher(value).matches();
        }
    }

    public static class Stream {
        private static final Pattern STREAM_SPECIFIER_PATTERN = compile("^(|:|:[advst]:\\d+|:\\d+)\\s+(.*)$", CASE_INSENSITIVE);

        @Nonnull
        public static Matcher newStreamSpecifierMatcher(@NonNull final String value) {
            return STREAM_SPECIFIER_PATTERN.matcher(value);
        }

        /**
         * Validate stream specifier (per-stream).
         *
         * @param value value (required)
         * @return boolean valid (true) or not (false)
         */
        public static boolean validateStreamSpecifier(@NonNull final String value) {
            if(StringUtils.contains(value, ":")) {
                return newStreamSpecifierMatcher(value).matches();
            }
            return StringUtils.isNotBlank(value);
        }
    }
}
