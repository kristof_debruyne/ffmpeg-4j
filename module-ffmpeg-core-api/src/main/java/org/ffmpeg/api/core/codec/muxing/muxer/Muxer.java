package org.ffmpeg.api.core.codec.muxing.muxer;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.core.codec.muxing.AbstractMuxer;

/**
 * Muxer container (-muxers).
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractMuxer
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Muxer extends AbstractMuxer {

    private static final long serialVersionUID = 0L;

    @Builder
    private Muxer(@NonNull String name, @NonNull String description) { super(name, description); }
}
