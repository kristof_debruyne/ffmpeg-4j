package org.ffmpeg.api.core.process;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.Map;
import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * FFmpeg process request.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface FFmpegProcessRequest {

    boolean isHideBanner();

    boolean isOverwriteOutputFiles();

    @Nonnull
    UUID getUuid();

    @Nonnull
    Object getAttributes();

    @Nonnull
    Path getSource();

    @Nonnull
    Path getTarget();

    @Nullable
    FFmpegProcessCallback getCallback();

    @Nonnull
    Map<String, Object> getGlobalOptions();

    @Nonnull
    Map<String, Object> getInputOptions();

    @Nonnull
    Map<String, Object> getOutputOptions();

    default boolean isCancelled() {
        return nonNull(getCallback()) && getCallback().isCancelled();
    }
}
