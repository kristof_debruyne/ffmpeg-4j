package org.ffmpeg.api.core.options.vo;


import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Abstract option value container.
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> option type (argument)
 */
@Getter
@ToString
public abstract class AbstractFFmpegOptionValue<T> implements Comparable<AbstractFFmpegOptionValue<T>> {

    private final T option;

    private final Object value;

    private final FFmpegOptionLocation location;

    protected AbstractFFmpegOptionValue(@NonNull T option, @Nullable Object value, @NonNull FFmpegOptionLocation location) {
        this.option = option;
        this.value = value;
        this.location = location;
    }

    /**
     * Gets option argument.
     *
     * @return option argument
     */
    @Nonnull
    public abstract String getArgument();

    /**
     * Gets option sort order.
     *
     * @return option order (never negative)
     */
    @Nonnegative
    public abstract int getOrder();

    /**
     * Converts option value to array (for execution)
     *
     * @return array with option value parameters (neve null)
     */
    @Nonnull
    public abstract String[] toArray();

    /**
     * {@inheritDoc}
     */
    @Override
    public final int compareTo(@NonNull final AbstractFFmpegOptionValue<T> that) {
        if(getOrder() < that.getOrder()) {
            return -1;
        } else if(getOrder() > that.getOrder()) {
            return 1;
        }
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equals(@NonNull final Object that) {
        if(this == that) {
            return true;
        }
        if(that instanceof AbstractFFmpegOptionValue<?>) {
            AbstractFFmpegOptionValue<?> optionValue = (AbstractFFmpegOptionValue<?>) that;
            return Objects.equals(getArgument(), optionValue.getArgument()) && Objects.equals(getLocation(), optionValue.getLocation());
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int hashCode() {
        return Objects.hash(getArgument(), getLocation());
    }
}
