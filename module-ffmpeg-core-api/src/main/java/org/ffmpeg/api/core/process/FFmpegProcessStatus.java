package org.ffmpeg.api.core.process;

/**
 * FFmpeg process statusses.
 *
 * @author Sikke303
 * @since 1.0
 */
public enum FFmpegProcessStatus {

    /**
     * Process is cancelled by user.
     */
    CANCELLED,
    /**
     * Process failed due a system exception.
     */
    ERROR,
    /**
     * Process failed due a business exception.
     */
    FAILED,
    /**
     * Process succeeded but with warnings.
     */
    SUCCESS_WITH_WARNINGS,
    /**
     * Process succeeded.
     */
    SUCCESS
}