package org.ffmpeg.api.core.converter;

import lombok.NonNull;

import javax.annotation.Nonnull;

/**
 * Converter interface. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @param <S> source type
 * @param <T> target type
 */
public interface FFmpegConverter<S, T> {

    /**
     * Gets validator name.
     *
     * @return name (never null)
     */
    @Nonnull
    String getName();

    /**
     * Converts source to target.
     *
     * @param source source (required)
     * @return target (never null)
     */
    @Nonnull
    T convert(@NonNull S source);
}
