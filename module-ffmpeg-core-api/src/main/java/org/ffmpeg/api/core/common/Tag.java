package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Tag container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class Tag implements FFmpegObject {

    private final String album;
    private final String artist;
    private final String comment;
    private final String title;
    private final int year;
}
