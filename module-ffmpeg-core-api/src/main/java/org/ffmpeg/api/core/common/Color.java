package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Color container (-colors).
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Color implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    @NonNull
    private final String name;

    @NonNull
    private final String value;
}
