package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Filter container (-filters).
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class Filter implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    private final boolean timeLineSupport; //T

    private final boolean sliceThreading; //S

    private final boolean commandSupport; //C

    @NonNull
    private final String name;

    @NonNull
    private final String transition;

    @NonNull
    private final String description;
}
