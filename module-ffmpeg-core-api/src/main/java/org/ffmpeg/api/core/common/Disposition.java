package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported disposition values.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Disposition {

    ATTACHED_PIC("attached_pic"),
    CLEAN_EFFECTS("clean_effects"),
    CAPTIONS("captions"),
    COMMENT("comment"),
    DEFAULT("default"),
    DEPENDENT("dependent"),
    DESCRIPTIONS("descriptions"),
    DUB("dub"),
    FORCED("forced"),
    HEARING_IMPAIRED("hearing_impaired"),
    KARAOKE("karaoke"),
    LYRICS("lyrics"),
    METADATA("metadata"),
    ORIGINAL("original"),
    VISUAL_IMPAIRED("visual_impaired");

    @Getter
    private final String value;
}
