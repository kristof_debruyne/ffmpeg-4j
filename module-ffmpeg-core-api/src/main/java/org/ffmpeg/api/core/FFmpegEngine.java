package org.ffmpeg.api.core;

import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.codec.muxing.demuxer.DeMuxer;
import org.ffmpeg.api.core.codec.muxing.muxer.Muxer;
import org.ffmpeg.api.core.common.Color;
import org.ffmpeg.api.core.common.Device;
import org.ffmpeg.api.core.common.Filter;
import org.ffmpeg.api.core.common.Format;
import org.ffmpeg.api.core.common.HelpMode;
import org.ffmpeg.api.core.common.Layout;
import org.ffmpeg.api.core.common.PixelFormat;
import org.ffmpeg.api.core.common.Protocol;
import org.ffmpeg.api.core.common.SampleFormat;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.core.annotation.Experimental;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * FFmpeg engine interface.
 *
 * Command list: https://gist.github.com/tayvano/6e2d456a9897f55025e25035478a3a50
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "Core engine",
        description = "Engine is shared among all FFMPEG tools. It can be used to retrieve various info like encoders, decoders, etc...",
        link = "https://www.ffmpeg.org"
)
public interface FFmpegEngine {

    @Nonnull
    default String getDocumentationUrl() { return "https://www.ffmpeg.org"; }

    /**
     * Gets executor factory.
     *
     * @return executor factory (never null)
     */
    @Nonnull
    FFmpegExecutorFactory getExecutorFactory();

    /**
     * Gets toolkit.
     *
     * @return toolkit (never null)
     */
    @Nonnull
    FFmpegToolkit getToolkit();

    /**
     * Gets bit stream filters.
     * @return set of bit stream filters or empty list (never null)
     */
    @Nonnull
    Set<String> getBitStreamFilters();

    /**
     * Gets build configurations.
     * @return set of build configurations or empty list (never null)
     */
    @Nonnull
    Set<String> getBuildConfigurations();

    /**
     * Gets codecs.
     * @param codecType codec type (required)
     * @return set of codecs or empty list (never null)
     */
    @Nonnull
    Set<Codec> getCodecs(@Nonnull CodecType codecType);

    /**
     * Gets colors.
     * @return set of colors or empty list (never null)
     */
    @Nonnull
    Set<Color> getColors();

    /**
     * Gets cpu flags.
     *
     * @param option option (required)
     * @return list of cpu flags or empty list (never null)
     */
    @Experimental
    @Nonnull
    List<String> getCpuFlags(@Nonnull String option);

    /**
     * Gets decoders.
     *
     * @param codecType codec type (required)
     * @return set of decoders or empty list (never null)
     */
    @Nonnull
    Set<Decoder> getDecoders(@Nonnull CodecType codecType);

    /**
     * Gets demuxers.
     * @return set of demuxers or empty list (never null)
     */
    @Nonnull
    Set<DeMuxer> getDemuxers();

    /**
     * Gets devices.
     * @return set of devices or empty list (never null)
     */
    @Nonnull
    Set<Device> getDevices();

    /**
     * Gets encoders.
     *
     * @param codecType codec type (required)
     * @return set of encoders or empty list (never null)
     */
    @Nonnull
    Set<Encoder> getEncoders(@Nonnull CodecType codecType);

    /**
     * Gets filters.
     * @return set of filters or empty list (never null)
     */
    @Nonnull
    Set<Filter> getFilters();

    /**
     * Gets formats.
     * @return set of formats or empty list (never null)
     */
    @Nonnull
    Set<Format> getFormats();

    /**
     * Gets help.
     * @return help (never null)
     */
    @Nonnull
    String getHelp();

    /**
     * Gets help.
     *
     * @param helpMode help mode (required)
     * @return help (never null)
     */
    @Nonnull
    String getHelp(@Nonnull HelpMode helpMode);

    /**
     * Gets layouts.
     * @return set of layouts or empty list (never null)
     */
    @Nonnull
    Set<Layout> getLayouts();

    /**
     * Gets license.
     * @return license (never null)
     */
    @Nonnull
    String getLicense();

    /**
     * Gets muxers.
     * @return set of muxers or empty list (never null)
     */
    @Nonnull
    Set<Muxer> getMuxers();

    /**
     * Gets name.
     *
     * @return name (never null)
     */
    @Nonnull
    String getName();

    /**
     * Gets pixel formats.
     * @return set of pixel formats or empty list (never null)
     */
    @Nonnull
    Set<PixelFormat> getPixelFormats();

    /**
     * Gets protocols.
     *
     * @param input input (true) or output (false)
     * @return set of protocols or empty list (never null)
     */
    @Nonnull
    Set<Protocol> getProtocols(boolean input);

    /**
     * Gets sample formats.
     * @return set of sample formats or empty list (never null)
     */
    @Nonnull
    Set<SampleFormat> getSampleFormats();

    /**
     * Gets report.
     * @return report (never null)
     */
    @Nonnull
    String getReport();

    /**
     * Gets version.
     *
     * @return version (never null)
     */
    @Nonnull
    String getVersion();

    /**
     * Gets version.
     *
     * @param full full version (true) or not (false)
     * @return version (never null)
     */
    @Nonnull
    String getVersion(boolean full);
}
