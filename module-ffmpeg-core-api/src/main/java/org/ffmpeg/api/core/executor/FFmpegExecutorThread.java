package org.ffmpeg.api.core.executor;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * FFmpeg executor shutdown hook thread. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see Thread
 */
@RequiredArgsConstructor
@Getter
public final class FFmpegExecutorThread extends Thread {

	@NonNull
	private final Process process;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void run() {
		process.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(getName()).toString();
	}
}