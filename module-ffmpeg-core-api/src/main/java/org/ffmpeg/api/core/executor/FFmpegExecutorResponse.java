package org.ffmpeg.api.core.executor;

import org.ffmpeg.api.core.process.FFmpegProcessStatus;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Stream;

/**
 * FFmpeg executor result. <br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface FFmpegExecutorResponse extends Serializable {

    /**
     * Gets status.
     *
     * @return status (never null)
     */
    @Nonnull
    FFmpegProcessStatus getStatus();

    /**
     * Gets result as single value.
     *
     * @return value (never null)
     */
    @Nonnull
    String asValue();

    /**
     * Gets result as raw value.
     *
     * @return raw value (never null)
     */
    @Nonnull
    String asRawValue();

    /**
     * Gets result as multi value.
     *
     * @return values (never null)
     */
    @Nonnull
    List<String> asValues();

    /**
     * Gets result as stream.
     *
     * @param parallel parallel (true) or not (false)
     * @return value stream (never null)
     */
    @Nonnull
    Stream<String> asStream(boolean parallel);
}
