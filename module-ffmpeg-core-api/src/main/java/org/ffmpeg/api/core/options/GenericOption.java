package org.ffmpeg.api.core.options;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.common.HelpMode;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.ANYWHERE;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isNumber;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;

/**
 * Supported FFmpeg generic options. <br><br>
 *
 * URL: https://www.ffmpeg.org/ffmpeg.html#Generic-options
 *
 * @author Sikke303
 * @since 1.0
 * @see SimpleFFmpegOption
 */
@RequiredArgsConstructor
@ToString
public enum GenericOption implements SimpleFFmpegOption {

    BITSTREAM_FILTERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-bsfs")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    BUILD_CONFIGURATION{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-buildconf")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    CPU_FLAGS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-cpuflags")
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(isText())
                    .build();
        }
    },
    CODECS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-codecs")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    COLORS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-colors")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    DECODERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-decoders")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    DEMUXERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-demuxers")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    DEVICES{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-devices")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    ENCODERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-encoders")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    FILTERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-filters")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    FORMATS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-formats")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    HELP{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-help")
                    .aliases(asList("-h", "-help", "--help", "-?"))
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(value -> value instanceof HelpMode)
                    .build();
        }
    },
    LICENSE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-L")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    LAYOUTS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-layouts")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    MAX_ALLOCATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-max_alloc")
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    MUXERS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-muxers")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    PIXEL_FORMATS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-pix_fmts")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    PROTOCOLS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-protocols")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    REPORT{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-report")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    SAMPLE_FORMATS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sample_fmts")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    SINKS{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sinks")
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(isText())
                    .build();
        }
    },
    SOURCES{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sources")
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(isText())
                    .build();
        }
    },
    VERSION{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-version")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    }
}
