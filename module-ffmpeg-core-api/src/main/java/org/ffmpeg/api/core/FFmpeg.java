package org.ffmpeg.api.core;

import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.multimedia.MultiMediaAware;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;

/**
 * FFmpeg interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegEngine
 * @see MultiMediaAware
 */
@FFmpegComponent(
        name = "FFmpeg",
        description = "FFmpeg is a cross platform CLI tool that allows you to encode/decode and process any kind of audio/video stream.",
        link = "https://www.ffmpeg.org"
)
public interface FFmpeg extends FFmpegEngine, MultiMediaAware {

    @Nonnull
    default String getDocumentationUrl() { return "https://www.ffmpeg.org/ffmpeg.html"; }

    /**
     * Gets process listener.
     *
     * @return process listener (never null)
     */
    @Nonnull
    FFmpegProcessListener getProcessListener();
}