package org.ffmpeg.api.core.executor;

import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;

/**
 * FFmpeg executor factory.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutor
 */
@FFmpegComponent(
        name = "Excecutor factory",
        description = "An executor factory create executor prototypes as an executor is intended to use only once."
)
@ParametersAreNonnullByDefault
public interface FFmpegExecutorFactory {

    /**
     * Checks if the error stream will be redirected.
     *
     * @return boolean redirect error stream (true) or not (false)
     */
    boolean isRedirectErrorStream();

    /**
     * Gets default executor path.
     *
     * @return executor path (never null)
     */
    @Nonnull
    Path getExecutorPath();

    /**
     * Creates default new FFmpeg executor instance by request.
     *
     * @param commands executor commands (required)
     * @return ffmpeg executor instance (never null)
     */
    @Nonnull
    FFmpegExecutor newInstance(@Nonnull String ...commands);

    /**
     * Creates default new FFmpeg executor instance by request.
     *
     * @param path executor path (required)
     * @param commands executor commands (required)
     * @return ffmpeg executor instance (never null)
     */
    @Nonnull
    FFmpegExecutor newInstance(@Nonnull Path path, @Nonnull String ...commands);

    /**
     * Creates custom new FFmpeg executor instance by request.
     *
     * @param clazz executor type (required)
     * @param path execytor path (required)
     * @param commands executor commands (required)
     * @return ffmpeg executor instance (never null)
     */
    @Nonnull
    FFmpegExecutor newInstance(@Nonnull Class<? extends FFmpegExecutor> clazz, @Nonnull Path path, @Nonnull String ...commands);
}