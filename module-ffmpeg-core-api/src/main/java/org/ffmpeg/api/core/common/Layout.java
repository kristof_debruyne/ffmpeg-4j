package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Layout container (-layouts).
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class Layout implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    @NonNull
    private final String name;

    @NonNull
    private final String decomposition;
}
