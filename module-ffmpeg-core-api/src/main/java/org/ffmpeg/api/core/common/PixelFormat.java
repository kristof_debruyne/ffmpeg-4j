package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Pixel format container (-pix_fmts).
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class PixelFormat implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    private final boolean inputSupported;

    private final boolean outputSupported;

    private final boolean hardwareAcceleratedFormat;

    private final boolean palettedFormat;

    private final boolean bitStreamFormat;

    @NonNull
    private final String name;

    private final int numberOfComponents;

    private final int bitsPerPixel;
}
