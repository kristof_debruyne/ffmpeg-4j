package org.ffmpeg.api.core.codec.coding.encoder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.AbstractCoder;

/**
 * Encoder class (-encoders).
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractCoder
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Encoder extends AbstractCoder {

    private static final long serialVersionUID = 0L;

    @Builder
    private Encoder(boolean frameLevelThreading,
                    boolean sliceLevelThreading,
                    boolean experimental,
                    boolean bandSupport,
                    boolean directRenderingSupport,
                    CodecType codecType,
                    String name,
                    String description) {
        super(frameLevelThreading, sliceLevelThreading, experimental, bandSupport, directRenderingSupport, codecType, name, description);
    }
}