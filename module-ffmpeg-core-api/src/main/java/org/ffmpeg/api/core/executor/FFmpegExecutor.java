package org.ffmpeg.api.core.executor;

import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Optional;

/**
 * FFmpeg executor interface. <br><br>
 *
 * FFMPEG executors are intended to use only once. <br>
 * This is because an input stream is used during processing FFMPEG operation. <br><br>
 *
 * How to use the executor? <br>
 * 1. Create executor instance <br>
 * 2. Add commands to executor <br>
 * 3. Invoke {@link #execute()} method <br>
 * 4. Invoke {@link #destroy()} method <br><br>
 *
 * The best is to put the {@link #execute()} in a try-with-resources block as executor extends {@link AutoCloseable} interface <br>
 * and the method returns an instance of {@link FFmpegBufferedReader} class. <br><br>
 *
 * Command execution: <br>
 * ffmpeg [global_options] {[input_file_options] -i input_url} ... {[output_file_options] output_url} ... <br><br>
 *
 * There are some ways to create an executor instance: <br>
 * - through executor factory ({@link FFmpegExecutorFactory})<br>
 * - through domain specific language ({@link FFmpegExecutorBuilder}) <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractFFmpegExecutor
 * @see FFmpegExecutorBuilder
 */
@FFmpegComponent(
		name = "FFmpeg executor",
		description = "An executor is a class that allows you to execute any kind of FFMPEG option(s)."
)
public interface FFmpegExecutor extends AutoCloseable {

	/**
	 * Gets redirect error stream. <br>
	 *
	 * @return boolean <code>true</code> redirect error stream, <code>false</code> other wise
	 */
	boolean isRedirectErrorStream();

	/**
	 * Sets redirect error stream. <br>
	 *
	 * <b>Remark:</b> Error stream will be redirected to input stream !
	 *
	 * @param redirectErrorStream redirect error stream (true) or not (false)
	 */
	void setRedirectErrorStream(boolean redirectErrorStream);

	/**
	 * Checks if executor is currently running or not. <br>
	 *
	 * @return boolean <code>true</code> if executor is currently running, <code>false</code> other wise
	 */
	boolean isExecuted();

	/**
	 * Gets FFMPEG error stream. <br>
	 *
	 * @return error stream (never null)
	 */
	@Nonnull
	Optional<InputStream> getErrorStream();

	/**
	 * Gets FFMPEG input stream. <br>
	 *
	 * @return input stream (never null)
	 */
	@Nonnull
	Optional<InputStream> getInputStream();

	/**
	 * Gets FFMPEG output stream. <br>
	 *
	 * @return output stream (never null)
	 */
	@Nonnull
	Optional<OutputStream> getOutputStream();

	/**
	 * Gets executor path. <br>
	 *
	 * @return executor path or null (if not set)
	 */
	@Nullable
	Path getExecutorPath();

	/**
	 * Sets executor path. <br>
	 *
	 * @param executorPath executor path
	 */
	void setExecutorPath(@Nonnull Path executorPath);

	/**
	 * Sets executor commands.
	 *
	 * @return commands (never null)
	 */
	@Nonnull
	String[] getCommands();

	/**
	 * Sets executor commands.
	 *
	 * @param commands commands (required)
	 */
	void setCommands(@Nonnull String ...commands);

	/**
	 * Executes FFMPEG process. <br><br>
	 *
	 * Use this execute method if you want to pass just raw options for executions. <br>
	 *
	 * @return reader (never null)
	 */
	@Nonnull
	FFmpegBufferedReader execute();

	/**
	 * Destroys FFMPEG process. <br>
	 */
	void destroy();

	/**
	 * Registers FFMPEG shutdown hook thread. <br>
	 *
	 * @param thread FFMPEG shutdown hook thread (required)
	 */
	void registerShutdownHook(@Nonnull FFmpegExecutorThread thread);

	/**
	 * Unregisters FFMPEG shutdown hook thread. <br>
	 *
	 * @param thread FFMPEG shutdown hook thread (required)
	 */
	void unregisterShutdownHook(@Nonnull FFmpegExecutorThread thread);
}