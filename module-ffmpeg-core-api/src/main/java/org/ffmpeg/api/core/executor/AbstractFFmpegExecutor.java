package org.ffmpeg.api.core.executor;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.api.exception.FFmpegRuntimeException;
import org.ffmpeg.core.annotation.Tracing;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.nio.file.Path;

import static java.util.Objects.isNull;

/**
 * Abstract class for all FFMPEG executors. <br><br>
 *
 * An executor by nature requires: <br>
 * - executor path <br>
 * - execution commands <br>
 * - redirect error stream (true) or not (false) <br><br>
 *
 * Once those properties are set, you can invoke the {@link #start()} method, <br>
 * which will validate the input and starts a {@link Process} if all goes well. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutor
 * @see ProcessBuilder
 */
@ParametersAreNonnullByDefault
@Getter
@Setter
@NotThreadSafe
public abstract class AbstractFFmpegExecutor implements FFmpegExecutor {

	private boolean redirectErrorStream;

	@NonNull
	private Path executorPath;

	@NonNull
	private String[] commands;

	/**
	 * Starts FFMPEG process.
	 *
	 * @return process (never null)
	 * @throws IOException if anything occurs during executing process
	 * @throws FFmpegRuntimeException if executor path is null or options are empty
	 */
	@Tracing
	@Nonnull
	protected final Process start() throws IOException {
		if(isNull(getExecutorPath())) {
			throw new FFmpegExecutionException("Please set an executor path before executing !");
		}
		String[] args = ArrayUtils.insert(0, getCommands(), getExecutorPath().toAbsolutePath().toString());
		if(ArrayUtils.getLength(args) == 1) {
			throw new FFmpegExecutionException("Please add options before executing !");
		}
		ProcessBuilder processBuilder = new ProcessBuilder().command(args);
		processBuilder.redirectErrorStream(isRedirectErrorStream());
		return processBuilder.start();
	}

	@Override
	public void registerShutdownHook(@NonNull final FFmpegExecutorThread thread) {
		Runtime.getRuntime().addShutdownHook(thread);
	}

	@Override
	public void unregisterShutdownHook(@NonNull final FFmpegExecutorThread thread) {
		Runtime.getRuntime().removeShutdownHook(thread);
	}
}