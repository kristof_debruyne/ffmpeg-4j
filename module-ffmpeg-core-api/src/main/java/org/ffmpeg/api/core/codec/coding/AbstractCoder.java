package org.ffmpeg.api.core.codec.coding;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.core.FFmpegObject;
import org.ffmpeg.api.core.codec.CodecType;

/**
 * Abstract coder class.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public abstract class AbstractCoder implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    private final boolean frameLevelThreading; //F

    private final boolean sliceLevelThreading; //S

    private final boolean experimental; //X

    private final boolean bandSupport; //B

    private final boolean directRenderingSupport; //D

    @NonNull
    private final CodecType codecType;

    @NonNull
    private final String codec;

    @NonNull
    private final String description;
}
