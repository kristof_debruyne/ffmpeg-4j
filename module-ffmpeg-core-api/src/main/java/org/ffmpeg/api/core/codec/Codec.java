package org.ffmpeg.api.core.codec;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Codec container (-codecs).
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Codec implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    private final boolean decodingSupport; //D

    private final boolean encodingSupport; //E

    private final CodecType codecType; //A (audio) - S (subtitle) - V (video)

    private final boolean intraFrameOnly; //I

    private final boolean lossyCompression; //L

    private final boolean losslessCompression; //S

    @NonNull
    private final String name;

    @NonNull
    private final String description;
}
