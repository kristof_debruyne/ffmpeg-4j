package org.ffmpeg.api.core.codec.muxing;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

/**
 * Abstract muxer container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode
@ToString
public abstract class AbstractMuxer implements FFmpegObject {

    private static final long serialVersionUID = 0L;

    @NonNull
    private final String name;

    @NonNull
    private final String description;
}
