package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * FFmpeg constants. <br>
 * 
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FFmpegConstant {

	public static final String FFMPEG_HOME = "FFMPEG_HOME";
	public static final String FFMPEG_QUALIFIER = "ffmpeg";

	public static final String AUDIO = "audio";
	public static final String SUBTITLE = "subtitle";
	public static final String VIDEO = "video";

	public static final String DECODER = "D";
	public static final String ENCODER = "E";

	public static final String DEMUXING = "D";
	public static final String MUXING = "E";

	public static final String MONO = "mono";
	public static final String STEREO = "stereo";

	public static final String NOW = "now";
	public static final String NOT_AVAILABLE = "N/A";

	public static final String BITSTREAM_FORMAT = "B";
	public static final String BAND_SUPPORT = "B";
	public static final String COMMAND_SUPPORT = "C";
	public static final String DIRECT_RENDERING_SUPPORT = "D";
	public static final String EXPERIMENTAL = "X";
	public static final String FRAME_LEVEL_THREADING = "F";
	public static final String HARDWARE_ACCELERATED_FORMAT = "H";
	public static final String INTRA_FRAME_ONLY = "I";
	public static final String INPUT_SUPPORT = "I";
	public static final String LOSSY_COMPRESSION = "L";
	public static final String LOSSLESS_COMPRESSION = "S";
	public static final String OUTPUT_SUPPORT = "O";
	public static final String PALETTED_FORMAT = "P";
	public static final String SLICE_LEVEL_THREADING = "S";
	public static final String TIME_LINE_SUPPORT = "T";

	public static final long NO_VALUE_SET = -1; //Not available
	public static final int PRIMARY_STREAM_INDEX = 0; //Primary stream index
}