package org.ffmpeg.api.core.process;

import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;

/**
 * FFmpeg process response.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface FFmpegProcessResponse {

    /**
     * Checks if process failed or not.
     *
     * @return boolean error (true) or not (false)
     */
    boolean isFailed();

    /**
     * Checks if process ended succesfully or not.
     *
     * @return boolean sucecss (true) or not (false)
     */
    boolean isSuccess();

    /**
     * Gets process status.
     *
     * @return status (never null)
     */
    @Nonnull
    FFmpegProcessStatus getStatus();

    /**
     * Gets warnings (if any).
     *
     * @return warnings or empty collection (never null)
     */
    @Nonnull
    Collection<String> getMessages();

    @Nonnull
    Optional<Path> getTarget();

    /**
     * Checks if there are any warnings or not.
     *
     * @return boolean warnings (true) or not (false)
     */
    boolean hasWarnings();
}
