package org.ffmpeg.api.core.options.vo;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.core.options.FFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.isNull;

/**
 * Raw option value container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
@ToString(callSuper = true)
public final class FFmpegRawOptionValue extends AbstractFFmpegOptionValue<String> {

    @Getter
    private final int order;

    public FFmpegRawOptionValue(@NonNull String option, @Nullable Object value, @NonNull FFmpegOptionLocation location, @Nonnegative int order) {
        super(option, value, location);
        this.order = order;
    }

    @Nonnull
    @Override
    public String getArgument() { return getOption(); }

    @Nonnull
    @Override
    public String[] toArray() {
        if(isNull(getValue())) {
            return new String[] { getOption() };
        }
        return new String[] { getOption(), String.valueOf(getValue())};
    }
}