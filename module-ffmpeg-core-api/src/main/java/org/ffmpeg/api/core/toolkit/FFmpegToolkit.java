package org.ffmpeg.api.core.toolkit;

import org.ffmpeg.api.core.converter.FFmpegConverter;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * FFmpeg toolkit interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "FFmpeg toolkit",
        description = "A toolkit with helper methods like parsing, validating, etc ..."
)
public interface FFmpegToolkit {

    /**
     * Checks if given argument is a complex one or not.
     *
     * @param argument argument (required)
     * @return boolean complex option (true) or not (false)
     */
    boolean isComplexOption(@Nonnull String argument);

    /**
     * Checks if given argument is a simple one or not.
     *
     * @param argument argument (required)
     * @return boolean simple option (true) or not (false)
     */
    boolean isSimpleOption(@Nonnull String argument);

    /**
     * Gets converter by name.
     *
     * @param name converter name (required)
     * @param <T> converter type
     * @return optional converter (never null)
     */
    @Nonnull
    <T extends FFmpegConverter<?, ?>> Optional<T> getConverter(@Nonnull String name);

    /**
     * Register custom converter.
     *
     * @param converter converter (required)
     * @param <T> converter type
     */
    <T extends FFmpegConverter<?, ?>> void registerConverter(@Nonnull T converter);

    /**
     * Gets validator by name.
     *
     * @param name validator name (required)
     * @param <T> validator type
     * @return optional validator (never null)
     */
    @Nonnull
    <T extends FFmpegValidator<?>> Optional<T> getValidator(@Nonnull String name);

    /**
     * Register custom validator.
     *
     * @param validator validator (required)
     * @param <T> validator type
     */
    <T extends FFmpegValidator<?>> void registerValidator(@Nonnull T validator);

    /**
     * Max allocates X number of bytes.
     *
     * @param bytes bytes to allocate
     * @return output (never null)
     */
    @Nonnull
    String maxAllocate(long bytes);

    /**
     * Converts duration to millis.
     *
     * @param duration duration (required)
     * @return duration in millis or -1 (wrong duration format)
     */
    long convertDuration(@Nonnull String duration);

    /**
     * Validates bit depth.
     *
     * @param bitDepth bit depth
     */
    void validateBitDepth(int bitDepth);

    /**
     * Validates bit rate.
     *
     * @param bitRate bit rate
     */
    void validateBitRate(int bitRate);

    /**
     * Validates channel.
     *
     * @param channel channel
     */
    void validateChannel(int channel);

    /**
     * Validates sample rate.
     *
     * @param sampleRate sample rate
     */
    void validateSampleRate(int sampleRate);
}
