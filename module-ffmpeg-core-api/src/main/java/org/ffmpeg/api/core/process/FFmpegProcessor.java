package org.ffmpeg.api.core.process;

import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * FFmpeg processor interface. <br>
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
		name = "FFmpeg processor",
		description = "A processor allows you to process anything by request and returns a response."
)
@FunctionalInterface
public interface FFmpegProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> {

	/**
	 * Processes request.
	 *
	 * @param request process request (required)
	 * @return optional response (never null)
	 */
	Optional<Response> process(@Nonnull Request request);
}
