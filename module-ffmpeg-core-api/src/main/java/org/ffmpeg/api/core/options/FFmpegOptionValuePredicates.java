package org.ffmpeg.api.core.options;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.ffmpeg.api.core.common.Disposition;

import javax.annotation.Nonnull;
import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static java.util.stream.Stream.of;
import static org.ffmpeg.api.core.common.FFmpegConstant.NOW;
import static org.ffmpeg.api.core.options.specifier.Specifier.Metadata.validateMetaDataSpecifier;
import static org.ffmpeg.api.core.options.specifier.Specifier.Stream.newStreamSpecifierMatcher;
import static org.ffmpeg.api.core.options.specifier.Specifier.Stream.validateStreamSpecifier;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FFmpegOptionValuePredicates {

    private static final PathMatcher FILE_RESOURCE_MATCHER = FileSystems.getDefault().getPathMatcher("glob:**.*");

    private static final Pattern DATE_PATTERN = compile("^(\\d{4}-\\d{2}-\\d{2}|\\d{8})([T\\s])(\\d{2,}:\\d{2}:\\d{2}|\\d{6,})(\\.\\d{1,3})?([Z]?)$");
    private static final Pattern TIME_PATTERN = compile("^((\\d+|\\d+\\.\\d{1,3})(s|ms|us)?|(\\d\\d):(\\d\\d):(\\d\\d))$");
    private static final Pattern FRAME_SIZE_PATTERN = compile("\\d+x\\d+", CASE_INSENSITIVE);
    private static final Pattern KEY_VALUE_PATTERN = compile("^(.+=.+)+$", CASE_INSENSITIVE);
    private static final Pattern KEY_VALUE_EXTENDED_PATTERN = compile("^.+(,.+=.+)+$", CASE_INSENSITIVE);

    @Nonnull
    public static Predicate<Object> areKeyValuePairs() { return areKeyValuePairs(false); }

    @Nonnull
    public static Predicate<Object> areKeyValuePairs(boolean metaDataSpecifier) {
        return (Object value) -> {
            if(isText().test(value)) {
                return KEY_VALUE_PATTERN.matcher((String) value).matches();
            }
            return false;
        };
    }

    /**
     * Checks if value is a valid date representation.
     * Resource: https://www.ffmpeg.org/ffmpeg-utils.html#date-syntax
     *
     * @return predicate (never null)
     */
    @Nonnull
    public static Predicate<Object> isDate() {
        return (Object value) -> {
            if(isText().test(value)) {
                if(StringUtils.equalsIgnoreCase((String) value, NOW)) {
                    return true;
                }
                return DATE_PATTERN.matcher((String) value).matches();
            }
            return false;
        };
    }

    /**
     * Checks if value is a valid time representation.
     * Resource: https://www.ffmpeg.org/ffmpeg-utils.html#Time-duration
     *
     * @return predicate (never null)
     */
    @Nonnull
    public static Predicate<Object> isTime() {
        return (Object value) -> {
            if(isText().test(value)) {
                return TIME_PATTERN.matcher((String) value).matches();
            }
            return isDecimal().test(value) || isNumber().test(value);
        };
    }

    @Nonnull
    public static Predicate<Object> isDecimal() { return isDecimal(false); }

    @Nonnull
    public static Predicate<Object> isDecimal(boolean streamSpecifier) {
        return (Object value) -> {
            if(isText().test(value) && streamSpecifier) {
                Matcher matcher = newStreamSpecifierMatcher((String) value);
                if(matcher.matches()) {
                    return NumberUtils.isCreatable(matcher.group(2));
                }
            }
            return value instanceof Float || value instanceof Double;
        };
    }

    @Nonnull
    public static Predicate<Object> isDevice() {
        return (Object value) -> {
            if(isText().test(value)) {
                return KEY_VALUE_EXTENDED_PATTERN.matcher((String) value).matches();
            }
            return false;
        };
    }

    @Nonnull
    public static Predicate<Object> isDisposition() {
        return (Object value) -> {
            if(isText().test(value)) {
                if(of(Disposition.values()).map(Disposition::getValue).anyMatch(disposition -> disposition.equals(value))) {
                    return true;
                }
                Matcher matcher = newStreamSpecifierMatcher((String) value);
                if(matcher.matches()) {
                    return of(Disposition.values()).map(Disposition::getValue)
                            .anyMatch(disposition -> matcher.group(2).contains(disposition));
                }
            }
            return false;
        };
    }

    @Nonnull
    public static Predicate<Object> isFrameSize() {
        return (Object value) -> {
            if(value instanceof String) {
                return FRAME_SIZE_PATTERN.matcher((String) value).matches();
            }
            return false;
        };
    }

    @Nonnull
    public static Predicate<Object> isNumber() { return isNumber(false); }

    @Nonnull
    public static Predicate<Object> isNumber(boolean streamSpecifier) {
        return (Object value) -> {
            if(isText().test(value) && streamSpecifier) {
                Matcher matcher = newStreamSpecifierMatcher((String) value);
                if(matcher.matches()) {
                    return NumberUtils.isCreatable(matcher.group(2));
                }
            }
            return value instanceof Integer || value instanceof Long;
        };
    }

    @Nonnull
    public static Predicate<Object> isResource() {
        return (Object value) -> {
            if(value instanceof String) {
                return FILE_RESOURCE_MATCHER.matches(Paths.get((String) value));
            }
            return false;
        };
    }

    @Nonnull
    public static Predicate<Object> isText() {
        return isText(false, false);
    }

    @Nonnull
    public static Predicate<Object> isText(boolean streamSpecifier, boolean metaDataSpecifier) {
        return (Object value) -> {
            if(!(value instanceof String)) {
                return false;
            }
            if(streamSpecifier) {
                return validateStreamSpecifier((String) value);
            } else if(metaDataSpecifier) {
                return validateMetaDataSpecifier((String) value);
            }
            return true;
        };
    }
}
