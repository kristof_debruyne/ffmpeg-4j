package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported help modes.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum HelpMode {

    LONG("long"),
    FULL("full");

    @Getter
    private final String value;
}