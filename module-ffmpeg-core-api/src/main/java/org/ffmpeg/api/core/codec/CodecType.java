package org.ffmpeg.api.core.codec;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Supported coder types. <br><br>
 *
 * A: audio <br>
 * S: subtitle <br>
 * V: video <br><br>
 *
 * Please check decoders (-decoders) and encoders (-encoders) <br>
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum CodecType {

    AUDIO("A"),
    SUBTITLE("S"),
    VIDEO("V");

    @Getter
    private final String value;

    public static CodecType getByValue(@NonNull final String value) {
        for(CodecType codecType : CodecType.values()) {
            if(codecType.getValue().equals(value)) {
                return codecType;
            }
        }
        throw new IllegalArgumentException("Codec type not supported ! Codec type: " + value);
    }
}