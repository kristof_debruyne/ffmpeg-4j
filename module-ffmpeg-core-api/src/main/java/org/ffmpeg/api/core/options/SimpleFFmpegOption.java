package org.ffmpeg.api.core.options;

/**
 * Simple option interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
public interface SimpleFFmpegOption extends FFmpegOption {

}