package org.ffmpeg.api.core;

import lombok.NonNull;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.util.Optional;

/**
 * FFmpeg platform interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "FFmpeg platform",
        description = "This is the base class to use. From this class, you should be able to interact with FFMPEG and its extensions."
)
public interface FFmpegPlatform {

    /**
     * Gets multimedia information from given resource.
     *
     * @param source source (required)
     * @return optional multimedia response (never null)
     */
    @Nonnull
    Optional<MultiMediaResponse> getMultiMediaInformation(@Nonnull Path source);

    /**
     * Checks if an extension with given name is registered or not.
     *
     * @param name name (required)
     * @return boolean
     */
    boolean containsExtension(@NonNull String name);

    /**
     * Gets FFMPEG extension by name.
     *
     * @param name name (required)
     * @return extension (never null)
     */
    @Nonnull
    Optional<FFmpegExtension> getExtension(@Nonnull String name);

    /**
     * Gets extension count.
     *
     * @return extension count or zero
     */
    @Nonnegative
    int getExtensionCount();

    /**
     * Gets FFMPEG framework instance.
     *
     * @return ffmpeg interface (never null)
     */
    @Nonnull
    FFmpeg getFramework();

    /**
     * Gets FFPROBE framework instance.
     *
     * @return optional ffprobe interface (never null)
     */
    @Nonnull
    Optional<FFprobe> getProbeFramework();

    /**
     * Gets FFPLAY framework instance.
     *
     * @return optional ffplay interface (never null)
     */
    @Nonnull
    Optional<FFplay> getPlayFramework();

    /**
     * Gets toolkit.
     *
     * @return toolkit (never null)
     */
    @Nonnull
    FFmpegToolkit getToolkit();
}
