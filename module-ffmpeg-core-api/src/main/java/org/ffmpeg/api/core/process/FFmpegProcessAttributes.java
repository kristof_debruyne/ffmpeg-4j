package org.ffmpeg.api.core.process;

import java.io.Serializable;

/**
 * FFmpeg process attributes interface. <br>
 * 
 * @author Sikke303
 * @since 1.0
 */
public interface FFmpegProcessAttributes extends Serializable {


}