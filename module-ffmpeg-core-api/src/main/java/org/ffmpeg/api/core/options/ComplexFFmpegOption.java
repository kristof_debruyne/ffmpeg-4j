package org.ffmpeg.api.core.options;

/**
 * Complex option interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
public interface ComplexFFmpegOption extends FFmpegOption {

}