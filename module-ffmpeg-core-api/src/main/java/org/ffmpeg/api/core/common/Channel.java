package org.ffmpeg.api.core.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported channels.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Channel {

    MONO(1),
    STEREO(2);

    @Getter
    private final int value;
}
