package org.ffmpeg.api.core.process;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;

/**
 * Callback for FFMPEG process. <br>
 * 
 * @author Sikke303
 * @since 1.0
 */
@FunctionalInterface
public interface FFmpegProcessCallback {

	/**
	 * Checks if process is cancelled. <br>
	 *
	 * @return boolean cancalled (true) or not (false) = default
	 */
	default boolean isCancelled() {
		return false;
	}

	/**
	 * Updates process progress. <br>
	 * 
	 * @param progress progress value
	 * @param text progress text (optional)
	 */
	void update(@Nonnegative long progress, @Nullable String text);
}