package org.ffmpeg.api.core.options;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.ANYWHERE;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.GLOBAL;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.areKeyValuePairs;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isDate;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isDecimal;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isDisposition;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isNumber;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isTime;

/**
 * Supported FFmpeg main options. <br><br>
 *
 * URL: https://www.ffmpeg.org/ffmpeg.html#Generic-options
 *
 * @author Sikke303
 * @since 1.0
 * @see ComplexFFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum MainOption implements ComplexFFmpegOption {

    ATTACHMENT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-attach")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    ATTACHMENT_DUMP {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-dump_attachment")
                    .locations(singletonList(INPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    CODEC {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-c")
                    .aliases(singletonList("-codec"))
                    .locations(asList(INPUT, OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    DATA_BLOCK {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-dn")
                    .locations(asList(INPUT, OUTPUT))
                    .build();
        }
    },
    DATA_FRAMES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-dframes")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    DEBUG {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-debug_ts")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    },
    DISPOSITION {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-disposition")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isDisposition())
                    .build();
        }
    },
    DO_NOT_OVERWRITE_OUTPUT_FILES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-n")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    },
    DURATION {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-t")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isTime())
                    .build();
        }
    },
    FORCE_FORMAT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-f")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    FILTER_GRAPH {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-filter")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    FILTER_SCRIPT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-filter_script")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    FILTER_THREADS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-filter_threads")
                    .locations(singletonList(GLOBAL))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    FRAMES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-frames")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    INPUT_SOURCE {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-i")
                    .locations(singletonList(INPUT))
                    .order(Integer.MAX_VALUE)
                    .valuePredicate(isText())
                    .build();
        }
    },
    INPUT_OFFSET {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-itsoffset")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isTime())
                    .build();
        }
    },
    INPUT_SCALE {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-itsscale")
                    .locations(singletonList(INPUT))
                    .perStream(true)
                    .valuePredicate(isDecimal(true))
                    .build();
        }
    },
    FILE_SIZE_LIMIT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-fs")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    METADATA {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-metadata")
                    .locations(singletonList(OUTPUT))
                    .perMetaData(true)
                    .valuePredicate(areKeyValuePairs(true))
                    .build();
        }
    },
    OVERWRITE_OUTPUT_FILES {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-y")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    },
    POSITION {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-to")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isTime())
                    .build();
        }
    },
    PRESET {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-pre")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    PROGRESS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-progress")
                    .locations(singletonList(GLOBAL))
                    .valuePredicate(isText())
                    .build();
        }
    },
    PROGRAM {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-program")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(areKeyValuePairs())
                    .build();
        }
    },
    QUALITY_SCALE {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-q")
                    .aliases(singletonList("qscale"))
                    .locations(singletonList(INPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    SEEK_POSITION {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-ss")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isTime())
                    .build();
        }
    },
    SEEK_POSITION_EOF {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sseof")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isDecimal())
                    .build();
        }
    },
    STATS {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-stats")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    },
    STATS_PERIOD_TIME {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-stats_period")
                    .locations(singletonList(GLOBAL))
                    .valuePredicate(isDecimal())
                    .build();
        }
    },
    STDIN_DISABLED {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-nostdin")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    STDIN_ENABLED {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-stdin")
                    .locations(singletonList(ANYWHERE))
                    .build();
        }
    },
    STREAM_LOOP_COUNT {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-stream_loop")
                    .locations(singletonList(INPUT))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    TARGET {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-target")
                    .locations(singletonList(ANYWHERE))
                    .valuePredicate(isText())
                    .build();
        }
    },
    TIMESTAMP {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-timestamp")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isDate())
                    .build();
        }
    }
}
