package org.ffmpeg.api.core.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.FFmpegObject;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * About container.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
@RequiredArgsConstructor(staticName = "of")
@ToString
public final class About implements FFmpegObject {

    private final String author = "Fabrice Bellard";
    private final String applicationName = "FFmpeg";
    private final String applicationType = "Multimedia Framework";
    private final String teamName = "FFmpeg team";
    private final String homepageUrl = "https://ffmpeg.org";
    private final String repositoryUrl = "https://git.ffmpeg.org/ffmpeg.git";
    private final String wikipediaUrl = "https://en.wikipedia.org/wiki/FFmpeg";
    private final List<String> licenses = Arrays.asList("LGPL-2.1+", "GPL-2.0+");
    private final List<String> supportedOperatingSystems = Arrays.asList("Linux", "MacOS", "Windows");
    private final List<String> supportedPlatforms = Arrays.asList("ARM", "PowerPC", "Sparc", "x86");
    private final List<String> writtenIn = Arrays.asList("Assembly", "C");
    private final LocalDate releaseDate = LocalDate.of(2000, 12, 20);
}
