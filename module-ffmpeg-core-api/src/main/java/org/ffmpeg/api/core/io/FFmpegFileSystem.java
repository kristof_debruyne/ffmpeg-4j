package org.ffmpeg.api.core.io;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.exception.FFmpegExecutionException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * File system class.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FFmpegFileSystem {

    /**
     * Gets file extension for given file.
     *
     * @param file file (required)
     * @param includeSeparator include separator (true) or not (false)
     * @return extension (never null)
     */
    @Nonnull
    public static String getFileExtension(@NonNull final Path file, boolean includeSeparator) {
        if(Files.isDirectory(file)) {
            throw new IllegalArgumentException("Path should be a file not a directory ! Path: " + file.toString());
        }
        return StringUtils.substring(file.getFileName().toString(), file.getFileName().toString().indexOf(".") + (includeSeparator ? 0 : 1));
    }

    /**
     * Creates temporary file.
     *
     * @param prefix prefix (required)
     * @param suffix suffix (required)
     * @return path (never null)
     */
    public static Path createTempFile(@NonNull final String prefix, @NonNull final String suffix) {
        try {
            Path tempFile = Files.createTempFile(prefix, suffix);
            tempFile.toFile().deleteOnExit();
            return tempFile;
        } catch (IOException ex) {
            throw new FFmpegExecutionException("Failed to create temporary file for processing !");
        }
    }

    /**
     * Deletes file silently.
     *
     * @param path path (required)
     */
    public static void deleteFile(@NonNull final Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
