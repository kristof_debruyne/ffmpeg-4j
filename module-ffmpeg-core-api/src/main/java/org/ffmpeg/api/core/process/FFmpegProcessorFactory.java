package org.ffmpeg.api.core.process;

import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Processor factory interface. <br>
 *
 * @author Sikke303
 * @since 1.0
 */
@FFmpegComponent(
        name = "Processor factory",
        description = "A processor factory create processor instances."
)
@FunctionalInterface
@ParametersAreNonnullByDefault
public interface FFmpegProcessorFactory {

    /**
     * Creates new processor instance of given type.
     *
     * @param type procssor type (required)
     * @param <T> processor type
     * @return processor instance (never null)
     */
    @Nonnull
    <T extends FFmpegProcessor<?, ?>> T newInstance(@Nonnull Class<T> type);
}
