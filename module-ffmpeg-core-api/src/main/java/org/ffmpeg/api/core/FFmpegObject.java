package org.ffmpeg.api.core;

import org.ffmpeg.core.annotation.FFmpegComponent;

import java.io.Serializable;

/**
 * FFmpeg object interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see Serializable
 */
@FFmpegComponent(
        name = "FFmpeg object",
        description = "Marker interface for all objects that can be serialized."
)
public interface FFmpegObject extends Serializable {

}
