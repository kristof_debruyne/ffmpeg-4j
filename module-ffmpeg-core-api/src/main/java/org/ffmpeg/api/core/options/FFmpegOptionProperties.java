package org.ffmpeg.api.core.options;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static java.util.Objects.isNull;

/**
 * Option properties.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@ToString
public class FFmpegOptionProperties {

    @Getter
    private final int order;

    @Getter
    private final boolean advanced;

    @Getter
    private final boolean perMetaData;

    @Getter
    private final boolean perStream;

    @Getter
    @NonNull
    private final String argument;

    private final List<String> aliases;
    private final List<FFmpegOptionLocation> locations;

    @Getter
    private final Predicate<Object> valuePredicate;

    @Nonnull
    public List<String> getAliases() {
        if(isNull(aliases)) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(aliases);
    }

    @Nonnull
    public List<FFmpegOptionLocation> getLocations() {
        if(isNull(locations) || locations.isEmpty()) {
            return Collections.singletonList(FFmpegOptionLocation.ANYWHERE);
        }
        return Collections.unmodifiableList(locations);
    }
}
