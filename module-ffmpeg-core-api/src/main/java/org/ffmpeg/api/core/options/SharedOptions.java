package org.ffmpeg.api.core.options;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.GLOBAL;

/**
 * Supported FFmpeg shared (generic) options.
 *
 * These options span all ffmpeg tools (ffmpeg, ffprobe, ffplay, etc ...)
 *
 * @author Sikke303
 * @since 1.0
 * @see ComplexFFmpegOption
 * @see GenericOption
 */
@RequiredArgsConstructor
@ToString
public enum SharedOptions implements ComplexFFmpegOption {

    HIDE_BANNER {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-hide_banner")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    },
    LOG_LEVEL {
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-loglevel")
                    .locations(singletonList(GLOBAL))
                    .build();
        }
    }
}
