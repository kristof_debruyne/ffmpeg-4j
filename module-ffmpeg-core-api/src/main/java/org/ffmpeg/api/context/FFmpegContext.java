package org.ffmpeg.api.context;

import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.common.About;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.executor.FFmpegExecutorResponse;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.subtitle.processor.SubtitleProcessor;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * FFmpeg context interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegPlatform
 */
@FFmpegComponent(
		name = "FFmpeg context",
		description = "Each application should have a context running for interacting with FFMPEG."
)
public interface FFmpegContext {

	/**
	 * Gets about information.
	 *
	 * @return about information (never null)
	 */
	@Nonnull
	About getAboutInformation();

	/**
	 * Executes raw commands.
	 *
	 * @param commands commands (required)
	 * @return result (never null)
	 */
	@Nonnull
    FFmpegExecutorResponse run(@Nonnull String ... commands);

	/**
	 * Gets platform instance.
	 *
	 * @return platform (never null)
	 */
	@Nonnull
	FFmpegPlatform getPlatform();

	/**
	 * Gets executor factory.
	 *
	 * @return executor factory (never null)
	 */
	@Nonnull
	FFmpegExecutorFactory getExecutorFactory();

	/**
	 * Gets processor factory.
	 *
	 * @return processor factory (never null)
	 */
	@Nonnull
	FFmpegProcessorFactory getProcessorFactory();

	/**
	 * Gets audio processor.
	 *
	 * @param <Request> request type
	 * @param <Response> response type
	 * @return optional audio processor (never null)
	 */
	@Nonnull
	<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> Optional<FFmpegProcessor<Request, Response>> getDefaultProcessor();

	/**
	 * Gets audio processor.
	 *
	 * @param <Request> request type
	 * @param <Response> response type
	 * @return optional audio processor (never null)
	 */
	@Nonnull
	<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> Optional<AudioProcessor<Request, Response>> getAudioProcessor();

	/**
	 * Gets subtitle processor.
	 *
	 * @param <Request> request type
	 * @param <Response> response type
	 * @return optional subtitle processor (never null)
	 */
	@Nonnull
	<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> Optional<SubtitleProcessor<Request, Response>> getSubtitleProcessor();

	/**
	 * Gets video processor.
	 *
	 * @param <Request> request type
	 * @param <Response> response type
	 * @return optional video processor (never null)
	 */
	@Nonnull
	<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> Optional<VideoProcessor<Request, Response>> getVideoProcessor();
}
