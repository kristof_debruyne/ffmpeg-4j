package org.ffmpeg.api.subtitle.processor;

import lombok.NonNull;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.core.annotation.Experimental;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Subtitle processor interface. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessor
 */
@Experimental
@FFmpegComponent(
        name = "Subtitle processor",
        description = "Processor that can handle subtitle streams."
)
public interface SubtitleProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> extends FFmpegProcessor<Request, Response> {

    /**
     * Checks if codec is an subtitle codec or not.
     *
     * @param name name (required)
     * @return boolean subtitle codec (true) or not (false)
     */
    boolean isSubtitleCodec(@NonNull String name);

    /**
     * Checks if decoder is an subtitle decoder or not.
     *
     * @param name name (required)
     * @return boolean subtitle decoder (true) or not (false)
     */
    boolean isSubtitleDecoder(@NonNull String name);

    /**
     * Checks if encoder is an subtitle encoder or not.
     *
     * @param name name (required)
     * @return boolean subtitle encoder (true) or not (false)
     */
    boolean isSubtitleEncoder(@NonNull String name);

    /**
     * Gets available subtitle codecs. <br>
     *
     * @return String[] array with available subtitle codecs
     */
    @Nonnull
    Set<Codec> getSubtitleCodecs();

    /**
     * Gets available subtitle decoders. <br>
     *
     * @return String[] array with available subtitle decoders
     */
    @Nonnull
    Set<Decoder> getSubtitleDecoders();

    /**
     * Gets available subtitle encoders. <br>
     *
     * @return String[] array with available subtitle encoders
     */
    @Nonnull
    Set<Encoder> getSubtitleEncoders();
}