package org.ffmpeg.api.subtitle.option;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionProperties;

import static java.util.Arrays.asList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isNumber;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;

/**
 * Supported FFmpeg main options.
 *
 * URL's:
 * - https://www.ffmpeg.org/ffmpeg.html#Subtitle-options
 * - https://www.ffmpeg.org/ffmpeg.html#Advanced-Subtitle-options
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum SubtitleOption implements ComplexFFmpegOption {

    BLOCK{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-sn")
                    .advanced(true)
                    .locations(asList(INPUT, OUTPUT))
                    .build();
        }
    },
    CANVAS_SIZE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-canvas_size")
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    CODEC{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-scodec")
                    .advanced(true)
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    FIX_DURATION{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-fix_sub_duration")
                    .build();
        }
    }
}
