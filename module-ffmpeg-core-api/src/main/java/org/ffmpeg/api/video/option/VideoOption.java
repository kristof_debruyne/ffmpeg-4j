package org.ffmpeg.api.video.option;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionProperties;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isFrameSize;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isNumber;
import static org.ffmpeg.api.core.options.FFmpegOptionValuePredicates.isText;

/**
 * Supported FFmpeg video options.
 *
 * URL's:
 * - https://www.ffmpeg.org/ffmpeg.html#Video-Options
 * - https://www.ffmpeg.org/ffmpeg.html#Advanced-Video-options
 *
 * @author Sikke303
 * @since 1.0
 * @see ComplexFFmpegOption
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public enum VideoOption implements ComplexFFmpegOption {

    ASPECT_RATIO{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-aspect")
                    .locations(asList(INPUT, OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    AUTO_ROTATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-autorotate")
                    .locations(singletonList(OUTPUT))
                    .build();
        }
    },
    AUTO_SCALE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-autoscale")
                    .locations(asList(INPUT, OUTPUT))
                    .build();
        }
    },
    BIT_RATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-b:v")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    BLOCK{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-vn")
                    .locations(asList(INPUT, OUTPUT))
                    .build();
        }
    },
    CODEC{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-vcodec")
                    .locations(asList(INPUT, OUTPUT))
                    .valuePredicate(isText())
                    .build();
        }
    },
    FILTER_GRAPH{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-vf")
                    .locations(singletonList(OUTPUT))
                    .build();
        }
    },
    FRAMES{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-vframes")
                    .locations(singletonList(OUTPUT))
                    .valuePredicate(isNumber())
                    .build();
        }
    },
    FRAME_RATE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-r")
                    .locations(asList(INPUT, OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    FRAME_RATE_MAX{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-fpsmax")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    FRAME_SIZE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-s")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isFrameSize())
                    .build();
        }
    },
    PASS_MODE{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-pass")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isNumber(true))
                    .build();
        }
    },
    PASS_LOG{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-passlogfile")
                    .locations(singletonList(OUTPUT))
                    .perStream(true)
                    .valuePredicate(isText(true, false))
                    .build();
        }
    },
    TAG{
        @Override
        public FFmpegOptionProperties getProperties() {
            return FFmpegOptionProperties.builder()
                    .argument("-vtag")
                    .advanced(true)
                    .locations(singletonList(OUTPUT))
                    .build();
        }
    }
}
