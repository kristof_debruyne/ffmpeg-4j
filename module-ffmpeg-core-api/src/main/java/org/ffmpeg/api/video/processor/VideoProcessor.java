package org.ffmpeg.api.video.processor;

import lombok.NonNull;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Set;


/**
 * Video processor interface. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessor
 */
@FFmpegComponent(
		name = "Video processor",
		description = "Processor that can handle video streams."
)
public interface VideoProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> extends FFmpegProcessor<Request, Response> {

	/**
	 * Checks if codec is an video codec or not.
	 *
	 * @param name name (required)
	 * @return boolean video codec (true) or not (false)
	 */
	boolean isVideoCodec(@NonNull String name);

	/**
	 * Checks if decoder is an video decoder or not.
	 *
	 * @param name name (required)
	 * @return boolean video decoder (true) or not (false)
	 */
	boolean isVideoDecoder(@NonNull String name);

	/**
	 * Checks if encoder is an video encoder or not.
	 *
	 * @param name name (required)
	 * @return boolean video encoder (true) or not (false)
	 */
	boolean isVideoEncoder(@NonNull String name);

	/**
	 * Gets available video codecs. <br>
	 *
	 * @return String[] array with available video codecs
	 */
	@Nonnull
	Set<Codec> getVideoCodecs();

	/**
	 * Gets available video encoders. <br>
	 *
	 * @return String[] array with available video encoders
	 */
	@Nonnull
	Set<Decoder> getVideoDecoders();

	/**
	 * Gets available video encoders. <br>
	 *
	 * @return String[] array with available video encoders
	 */
	@Nonnull
	Set<Encoder> getVideoEncoders();
}