package org.ffmpeg.api.config;

/**
 * FFmpeg properties interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface FFmpegProperties {

    boolean isRedirectErrorStream();
    boolean isProbeStrictXmlValidation();
    String getExecutorPath();
    long getBufferTimeInSeconds();
    String getPlayExecutorPath();
    String getProbeExecutorPath();
    String getProbeXsdPath();
}
