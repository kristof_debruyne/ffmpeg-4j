package org.ffmpeg.api.exception;

import lombok.NonNull;

/**
 * Exception thrown when the FFMPEG validation fails.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegRuntimeException
 */
public class FFmpegValidationException extends FFmpegRuntimeException {

    private static final long serialVersionUID = 0L;

    public FFmpegValidationException(@NonNull final String message) {
        super(message);
    }

    public FFmpegValidationException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}
