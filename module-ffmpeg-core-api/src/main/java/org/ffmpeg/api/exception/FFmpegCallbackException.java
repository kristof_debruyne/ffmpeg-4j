package org.ffmpeg.api.exception;

import lombok.NonNull;

/**
 * Exception thrown when the FFMPEG process is cancelled by callback.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegRuntimeException
 */
public class FFmpegCallbackException extends FFmpegRuntimeException {

    private static final long serialVersionUID = 0L;

    public FFmpegCallbackException(@NonNull final String message) {
        super(message);
    }

    public FFmpegCallbackException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}
