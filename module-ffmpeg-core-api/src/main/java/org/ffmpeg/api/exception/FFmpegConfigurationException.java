package org.ffmpeg.api.exception;

import lombok.NonNull;

/**
 * Exception thrown when the FFMPEG configuration is not valid.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegRuntimeException
 */
public class FFmpegConfigurationException extends FFmpegRuntimeException {

    private static final long serialVersionUID = 0L;

    public FFmpegConfigurationException(@NonNull final String message) {
        super(message);
    }

    public FFmpegConfigurationException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}
