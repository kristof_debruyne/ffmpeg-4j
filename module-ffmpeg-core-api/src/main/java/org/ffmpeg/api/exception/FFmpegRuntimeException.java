package org.ffmpeg.api.exception;

import lombok.NonNull;

/**
 * Abstract FFMPEG runtime exception. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see RuntimeException
 */
public abstract class FFmpegRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 0L;

	/**
	 * Constructor
	 * @param message message (required)
	 */
	protected FFmpegRuntimeException(@NonNull final String message) {
		super(message);
	}

	/**
	 * Constructor
	 * @param message message (required)
	 * @param exception exception (required)
	 */
	protected FFmpegRuntimeException(@NonNull final String message, Throwable exception) {
		super(message, exception);
	}
}