package org.ffmpeg.api.exception;

import lombok.NonNull;

/**
 * Exception thrown when the FFMPEG execution is erroring for whatever reason.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegRuntimeException
 */
public class FFmpegExecutionException extends FFmpegRuntimeException {

    private static final long serialVersionUID = 0L;

    public FFmpegExecutionException(@NonNull final String message) {
        super(message);
    }

    public FFmpegExecutionException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}
