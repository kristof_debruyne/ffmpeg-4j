package org.ffmpeg.api.multimedia;

import lombok.NonNull;
import org.ffmpeg.api.core.FFmpegObject;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.common.Stream;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Multimedia info interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MultiMediaInfo extends FFmpegObject {

    /**
     * Gets codec type by stream index.
     *
     * @param index stream index (required)
     * @return codec type (never null)
     */
    @NonNull
    CodecType findCodecTypeByIndex(int index);

    /**
     * Gets bit rate.
     *
     * @return bit rate
     */
    int getBitRate();

    /**
     * Gets duration.
     *
     * @return duration
     */
    long getDuration();

    /**
     * Gets format.
     *
     * @return format (never null)
     */
    @Nonnull
    String getFormat();

    /**
     * Gets streams.
     *
     * @param <T> stream type
     * @return set of streams or empty set (never null)
     */
    @Nonnull
    <T extends Stream> Set<T> getStreams();
}