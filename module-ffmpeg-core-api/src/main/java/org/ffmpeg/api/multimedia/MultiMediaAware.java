package org.ffmpeg.api.multimedia;

import javax.annotation.Nonnull;

/**
 * Multi media aware interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MultiMediaAware {

    /**
     * Gets multi media information parser. <br>
     *
     * @return optional multi media information parser (never null)
     * @param <Request> request type
     * @param <Response> response type
     */
    @Nonnull
    <Request extends MultiMediaRequest, Response extends MultiMediaResponse> MultiMediaParser<Request, Response> getMultiMediaParser();
}
