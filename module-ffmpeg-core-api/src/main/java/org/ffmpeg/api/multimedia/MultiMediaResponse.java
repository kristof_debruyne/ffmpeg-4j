package org.ffmpeg.api.multimedia;

import lombok.NonNull;

import java.io.Serializable;

/**
 * Multimedia response interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MultiMediaResponse extends Serializable {

    /**
     * Gets response body.
     *
     * @param <T> body type
     * @return body (never null)
     */
    @NonNull
    <T> T getBody();
}