package org.ffmpeg.api.multimedia;

import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Multimedia parser interface. <br><br>
 *
 * As this is a functional interface, you can use this as a lambda. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see MultiMediaRequest
 * @see MultiMediaResponse
 */
@FFmpegComponent(
		name = "Multimedia parser",
		description = "Parser that can extract multimedia information by request."
)
@FunctionalInterface
public interface MultiMediaParser<Request extends MultiMediaRequest, Response extends MultiMediaResponse> {

	/**
	 * Extracts multimedia information by request.
	 *
	 * @param request request (required)
	 * @return multimedia information (optional)
	 */
	@Nonnull
	Optional<Response> parse(@Nonnull Request request);
}