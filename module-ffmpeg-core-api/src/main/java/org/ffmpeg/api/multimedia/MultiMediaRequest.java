package org.ffmpeg.api.multimedia;

import javax.annotation.Nonnull;
import java.nio.file.Path;

/**
 * Multi media info request interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MultiMediaRequest {

    /**
     * Gets resource path.
     *
     * @return path (never null)
     */
    @Nonnull
    Path getResource();
}
