package org.ffmpeg.core.spring;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Condition that checks if RX java is on classpath.
 *
 * @author sikke303
 * @since 1.0
 * @see Condition
 */
@ParametersAreNonnullByDefault
class IsRxJavaLoaded implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        try {
            Class.forName(io.reactivex.rxjava3.plugins.RxJavaPlugins.class.getName());
        } catch (Throwable ex) {
            return false;
        }
        return true;
    }
}
