package org.ffmpeg.core.spring;

import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.config.FFmpegProperties;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.rx.api.context.ReactiveFFmpegContext;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueManager;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueObserver;
import org.ffmpeg.rx.context.ReactiveFFmpegContextImpl;
import org.ffmpeg.rx.process.ParallelFFmpegProcessor;
import org.ffmpeg.rx.queue.BufferedFFmpegQueueManager;
import org.ffmpeg.rx.queue.DefaultFFmpegQueueObserver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Reactive FFmpeg Spring configuration.
 *
 * @author Sikke303
 * @since 1.0
 */
@Configuration
@Conditional(IsRxJavaLoaded.class)
@RequiredArgsConstructor
public class FFmpegReactiveConfiguration {

    private final FFmpegProperties properties;

    @Bean
    public ReactiveFFmpegContext reactiveContext(@NonNull FFmpegContext context,
                                                 @NonNull ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor,
                                                 @NonNull ReactiveFFmpegQueueManager<DefaultFFmpegProcessRequest> queueManager,
                                                 @NonNull Scheduler scheduler) {
        return new ReactiveFFmpegContextImpl(context, processor, queueManager, scheduler);
    }

    @Bean
    public ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor(@NonNull FFmpegContext context) {
        return new ParallelFFmpegProcessor(context);
    }

    @Bean
    public ReactiveFFmpegQueueManager<DefaultFFmpegProcessRequest> queueManager(@NonNull ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor) {
        ReactiveFFmpegQueueObserver<List<DefaultFFmpegProcessRequest>> observer = new DefaultFFmpegQueueObserver(processor, Schedulers.computation());
        return new BufferedFFmpegQueueManager(observer, Schedulers.computation(), properties.getBufferTimeInSeconds());
    }
}
