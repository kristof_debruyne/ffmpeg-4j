package org.ffmpeg.test;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@RunWith(MockitoJUnitRunner.class)
@Getter
public abstract class AbstractFFmpegTest {

    private final Path defaultAudioSample = resolveDataResource("audio/sample1.flac");
    private final Path defauktVideoSample = resolveDataResource("video/sample1.avi");
    private final Path executorPath = Paths.get("/ffmpeg");

    @After
    public void tearDown() {
        try(Stream<Path> path = Files.list(Paths.get(System.getProperty("user.dir")))) {
            path.filter(filePath -> StringUtils.endsWithAny(filePath.getFileName().toString(), ".avi", ".log", ".flac"))
                    .forEach(filePath -> {
                        try {
                            Files.delete(filePath);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    protected static Path resolveDataResource(@NonNull final String resourcePath) {
        return Paths.get(Paths.get(System.getProperty("user.dir")).getParent().toAbsolutePath().toString().concat(String.format("/etc/data/%s", resourcePath)));
    }
}
