package org.ffmpeg.spring.boot.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.aop.FFmpegAopConfiguration;
import org.ffmpeg.api.config.FFmpegProperties;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.config.FFmpegBootstrapConfigurer;
import org.ffmpeg.core.spring.FFmpegReactiveConfiguration;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.nonNull;
import static org.ffmpeg.api.core.common.FFmpegConstant.FFMPEG_QUALIFIER;

/**
 * FFmpeg Spring Boot auto configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegBootstrapConfigurer
 */
@AutoConfiguration
@Import({FFmpegPropertiesConfiguration.class, FFmpegAopConfiguration.class, FFmpegReactiveConfiguration.class})
@RequiredArgsConstructor
@Slf4j
class FFmpegAutoConfiguration implements FFmpegBootstrapConfigurer {

    @Getter
    private final FFmpegProperties properties;

    @PostConstruct
    private void init() {
        log.info("Enabling FFMPEG auto configuration ... Home variable: {}", getFFmpegHomeVariable());
    }

    @Bean
    @ConditionalOnMissingBean(FFmpegContext.class)
    @Description("FFmpeg context to interact with the FFMPEG platform and its extensions.")
    public FFmpegContext context(@Autowired FFmpegPlatform platform,
                                 @Autowired FFmpegExecutorFactory executorFactory,
                                 @Autowired FFmpegProcessorFactory processorFactory) {
        return createContext(platform, executorFactory, processorFactory);
    }

    @Primary
    @Bean
    @ConditionalOnMissingBean(MultiMediaParser.class)
    @Description("FFmpeg default multimedia parser.")
    public MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser(
            @Autowired FFmpegExecutorFactory executorFactory, @Autowired FFmpegToolkit toolkit) {
        return createMultiMediaParser(executorFactory, toolkit);
    }

    @Bean
    @ConditionalOnMissingBean(FFmpeg.class)
    @Description("FFmpeg framework instance for retrieving framework specific information.")
    public FFmpeg ffmpeg(@Autowired FFmpegExecutorFactory executorFactory,
                         @Autowired FFmpegProcessListener processListener,
                         @Autowired FFmpegToolkit toolkit,
                         @Autowired MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser) {
        return createFFmpegFramework(executorFactory, processListener, toolkit, multiMediaParser);
    }

    @Primary
    @Bean
    @ConditionalOnMissingBean(FFmpegExecutorFactory.class)
    @Qualifier(FFMPEG_QUALIFIER)
    @Description("FFmpeg executor factory for creating executor instances.")
    public FFmpegExecutorFactory executorFactory() {
        return createExecutorFactory();
    }

    @Bean
    @ConditionalOnMissingBean(FFmpegProcessorFactory.class)
    @Description("FFmpeg processor factory for creating processor instances.")
    public FFmpegProcessorFactory processorFactory(@Autowired FFmpegPlatform platform) { return createProcessorFactory(platform); }

    @Bean
    @ConditionalOnMissingBean(FFmpegProcessListener.class)
    @Description("FFmpeg process listener.")
    public FFmpegProcessListener processListener(@Autowired FFmpegToolkit toolkit) {
        return createProcessListener(toolkit);
    }

    @Bean
    @ConditionalOnMissingBean(FFmpegToolkit.class)
    @Description("FFmpeg toolkit instance.")
    public FFmpegToolkit toolkit(@Autowired FFmpegExecutorFactory executorFactory) { return createToolkit(executorFactory); }

    @Bean
    @ConditionalOnMissingBean(FFmpegPlatform.class)
    @Description("FFmpeg platform instance.")
    public FFmpegPlatform platform(@Autowired FFmpeg ffmpeg,
                                   @Autowired FFmpegToolkit toolkit,
                                   @Autowired Set<FFmpegExtension> extensions) {
        return createPlatform(ffmpeg, toolkit, extensions);
    }

    @Bean
    @Description("FFmpeg extensions collection (ffprobe, ffplay, etc...)")
    public Set<FFmpegExtension> extensions(@Autowired(required = false) FFprobe ffprobe,
                                           @Autowired(required = false) FFplay ffplay) {
        Set<FFmpegExtension> extensions = new HashSet<>();
        if(nonNull(ffprobe)) { extensions.add(ffprobe); }
        if(nonNull(ffplay)) { extensions.add(ffplay); }
        return Collections.unmodifiableSet(extensions);
    }
}
