package org.ffmpeg.spring.boot.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.ffmpeg.api.config.FFmpegProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * FFmpeg properties configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProperties
 */
@Component
@PropertySource(value = "classpath:ffmpeg.properties")
@Getter
@Setter
@ToString(callSuper = true)
class FFmpegPropertiesConfiguration implements FFmpegProperties {

    @Value("${ffmpeg.redirectErrorStream:true}")
    private boolean redirectErrorStream;

    @Value("${ffmpeg.probeStrictXmlValidation:false}")
    private boolean probeStrictXmlValidation;

    @Value("${ffmpeg.executorPath}")
    private String executorPath;

    @Value("${ffmpeg.bufferTimeInSeconds:60}")
    private long bufferTimeInSeconds;

    @Value("${ffmpeg.playExecutorPath:null}")
    private String playExecutorPath;

    @Value("${ffmpeg.probeExecutorPath:null}")
    private String probeExecutorPath;

    @Value("${ffmpeg.probeXsdPath:'/ffprobe.xsd'}")
    private String probeXsdPath;
}