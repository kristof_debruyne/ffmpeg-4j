package org.ffmpeg.extension.probe;

import lombok.Getter;
import lombok.NonNull;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.core.FFmpegEngineImpl;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;

/**
 * Probe framework implementation.
 *
 * @author Sikke303
 * @since 1.0
 * @see org.ffmpeg.api.extension.FFmpegExtension
 */
public class FFprobeImpl extends FFmpegEngineImpl implements FFprobe {

    @Getter
    private final MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> multiMediaParser;

    public FFprobeImpl(@NonNull FFmpegExecutorFactory probeExecutorFactory,
                       @NonNull FFmpegToolkit toolkit,
                       @NonNull MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> multiMediaParser) {
        super(probeExecutorFactory, toolkit);
        this.multiMediaParser = multiMediaParser;
    }
}
