package org.ffmpeg.extension.probe.multimedia;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.common.LogLevel;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;
import org.ffmpeg.api.extension.probe.output.OutputFormatParserRegistry;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.core.AbstractFFmpegSupport;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.multimedia.DefaultMultiMediaParser;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

import static org.ffmpeg.api.core.options.SharedOptions.HIDE_BANNER;
import static org.ffmpeg.api.extension.probe.options.ProbeMainOption.INPUT_SOURCE;
import static org.ffmpeg.api.extension.probe.options.ProbeMainOption.OUTPUT_FORMAT;
import static org.ffmpeg.api.extension.probe.options.ProbeMainOption.PRETTY;
import static org.ffmpeg.api.extension.probe.options.ProbeMainOption.SHOW_FORMAT;
import static org.ffmpeg.api.extension.probe.options.ProbeMainOption.SHOW_STREAMS;

/**
 * Probe multimedia parser implementation.
 *
 * @author Sikke303
 * @since 1.0
 * @see DefaultMultiMediaParser
 * @see ProbeMultiMediaRequest
 */
@ParametersAreNonnullByDefault
@Slf4j
public class ProbeMultiMediaParser extends AbstractFFmpegSupport implements MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> {

    @NonNull
    private final OutputFormatParserRegistry<OutputFormatParser> registry;

    public ProbeMultiMediaParser(FFmpegExecutorFactory executorFactory, OutputFormatParserRegistry<OutputFormatParser> registry) {
        super(executorFactory);
        this.registry = registry;
    }

    @Tracing
    @Nonnull
    @Override
    public Optional<ProbeMultiMediaResponse> parse(@NonNull final ProbeMultiMediaRequest request) {
        String resource = request.getResource().toAbsolutePath().toString();
        log.trace("Extracting multi media information from resource: {}", resource);

        FFmpegExecutorBuilder builder = newExecutorBuilder(ComplexFFmpegExecutorBuilder.class)
                .registerOption(HIDE_BANNER)
                .registerOption(OUTPUT_FORMAT, request.getOutputFormat().getValue())
                .registerOption(PRETTY)
                .registerOption(SHOW_FORMAT)
                .registerOption(SHOW_STREAMS)
                .registerOption(INPUT_SOURCE, resource)
                .withLogLevel(LogLevel.ERROR);

        String body = execute(builder).asRawValue(); //BODY
        if(request.isRawResponse()) {
            return Optional.of(ProbeRawMultiMediaResponse.builder().body(body).build());
        }
        return Optional.of(registry.getParser(request.getOutputFormat()).parse(body));
    }
}
