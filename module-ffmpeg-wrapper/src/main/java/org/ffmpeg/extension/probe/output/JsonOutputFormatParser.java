package org.ffmpeg.extension.probe.output;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.audio.AudioStream;
import org.ffmpeg.multimedia.DefaultMultiMediaInfo;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.ffmpeg.video.VideoSize;
import org.ffmpeg.video.VideoStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.nonNull;
import static org.ffmpeg.api.core.common.FFmpegConstant.AUDIO;
import static org.ffmpeg.api.core.common.FFmpegConstant.VIDEO;

/**
 * JSON output format parser.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractOutputFormatParser
 */
@ParametersAreNonnullByDefault
@Slf4j
public class JsonOutputFormatParser extends AbstractOutputFormatParser<ProbeMultiMediaResponse> {

    @NonNull
    private final ObjectMapper objectMapper;

    public JsonOutputFormatParser(@NonNull ObjectMapper objectMapper, @NonNull FFmpegToolkit toolkit) {
        super(toolkit);
        this.objectMapper = objectMapper;
    }

    @Nonnull
    @Override
    public OutputFormat getOutputFormat() { return OutputFormat.JSON; }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public ProbeMultiMediaResponse parse(@NonNull final String content) {
        log.trace("Parsing JSON content: {}", content);
        DefaultMultiMediaInfo.DefaultMultiMediaInfoBuilder mediaInfoBuilder = DefaultMultiMediaInfo.builder();
        try {
            JsonNode rootNode= objectMapper.readTree(content); //READ

            JsonNode formatNode = rootNode.get(NODE_FORMAT); //FORMAT
            if(nonNull(formatNode)) {
                mediaInfoBuilder.bitRate(parseBitRate(formatNode.get(BIT_RATE).asText()));
                mediaInfoBuilder.duration(parseDuration(formatNode.get(DURATION).asText()));
                mediaInfoBuilder.format(formatNode.get(FORMAT_NAME).asText());
            }

            JsonNode streamsNode = rootNode.findValue(NODE_STREAMS); //STREAMS
            if(nonNull(streamsNode)) {
                streamsNode.forEach(node -> {
                    switch (node.findValue(CODEC_TYPE).asText()) {
                        case AUDIO -> mediaInfoBuilder.stream(parseAudioStream(node));
                        case VIDEO -> mediaInfoBuilder.stream(parseVideoStream(node));
                    }
                });
            }
        } catch (JsonProcessingException ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            throw new FFmpegExecutionException("Failed to parse multimedia information from JSON !");
        }
        return DefaultMultiMediaResponse.builder().body(mediaInfoBuilder.build()).build(); //BUILD
    }

    private AudioStream parseAudioStream(JsonNode node) {
        return AudioStream.builder()
                .index(node.findValue(INDEX).asInt())
                .codecName(node.findValue(CODEC_NAME).asText())
                .codecType(CodecType.valueOf(node.findValue(CODEC_TYPE).asText().toUpperCase()))
                .channels(node.findValue(CHANNELS).intValue())
                .sampleRate(parseSampleRate(node.findValue(SAMPLE_RATE).asText()))
                .build();
    }

    private VideoStream parseVideoStream(JsonNode node) {
        return VideoStream.builder()
                .index(node.findValue(INDEX).asInt())
                .codecName(node.findValue(CODEC_NAME).asText())
                .codecType(CodecType.valueOf(node.findValue(CODEC_TYPE).asText().toUpperCase()))
                .frameRate(parseFrameRate(node.findValue(FRAME_RATE).asText()))
                .size(new VideoSize(node.findValue(WIDTH).intValue(), node.findValue(HEIGHT).intValue()))
                .build();
    }
}
