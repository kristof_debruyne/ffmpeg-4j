package org.ffmpeg.extension.probe.multimedia;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;

/**
 * Probe raw multimedia response (unprocessed).
 * 
 * @author Sikke303
 * @since 1.0
 * @see ProbeMultiMediaResponse
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@ToString
public class ProbeRawMultiMediaResponse implements ProbeMultiMediaResponse {

	private static final long serialVersionUID = 0L;

	@NonNull
	private final String body;
}
