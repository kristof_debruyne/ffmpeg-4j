package org.ffmpeg.extension.probe.multimedia;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.api.multimedia.MultiMediaRequest;

import java.nio.file.Path;

/**
 * Probe multimedia request.
 *
 * @author Sikke303
 * @since 1.0
 * @see MultiMediaRequest
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@ToString
public class ProbeMultiMediaRequest implements MultiMediaRequest {

    private final boolean rawResponse;

    @NonNull
    private final OutputFormat outputFormat;

    @NonNull
    private final Path resource;
}
