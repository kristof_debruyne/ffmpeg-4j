package org.ffmpeg.extension.probe.output;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.audio.AudioStream;
import org.ffmpeg.multimedia.DefaultMultiMediaInfo;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.ffmpeg.video.VideoSize;
import org.ffmpeg.video.VideoStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;

import static java.lang.Integer.parseInt;
import static java.util.Objects.nonNull;
import static org.ffmpeg.api.core.common.FFmpegConstant.AUDIO;
import static org.ffmpeg.api.core.common.FFmpegConstant.VIDEO;

/**
 * XML output format parser. <br><br>
 *
 * Schema used for validation: ffprobe.xsd
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractOutputFormatParser
 */
@ParametersAreNonnullByDefault
@Slf4j
public class XmlOutputFormatParser extends AbstractOutputFormatParser<ProbeMultiMediaResponse> {

    private final boolean strictXmlValidation;

    @NonNull
    private final DocumentBuilderFactory documentBuilderFactory;

    @NonNull
    private final Schema xsdSchema;

    public XmlOutputFormatParser(boolean strictXmlValidation,
                                 @NonNull DocumentBuilderFactory documentBuilderFactory,
                                 @NonNull Schema xsdSchema,
                                 @NonNull FFmpegToolkit toolkit) {
        super(toolkit);
        this.strictXmlValidation = strictXmlValidation;
        this.documentBuilderFactory = documentBuilderFactory;
        this.xsdSchema = xsdSchema;
    }

    @Nonnull
    @Override
    public OutputFormat getOutputFormat() { return OutputFormat.XML; }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public ProbeMultiMediaResponse parse(@NonNull final String content) {
        DefaultMultiMediaInfo.DefaultMultiMediaInfoBuilder mediaInfoBuilder = DefaultMultiMediaInfo.builder();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder(); //PREPARE
            Document document = documentBuilder.parse(new InputSource(new StringReader(content))); //PARSE

            validateXml(document); //VALIDATE

            Element rootNode = document.getDocumentElement();
            if(nonNull(rootNode)) {
                NodeList probeNodes = rootNode.getChildNodes();
                for(int x = 0; x < probeNodes.getLength(); x++) {
                    Node node = probeNodes.item(x);
                    if(node.getLocalName().equals(NODE_FORMAT)) {
                        NamedNodeMap formatAttributes = node.getAttributes(); //FORMAT ATTRIBUTES
                        mediaInfoBuilder.bitRate(parseBitRate(formatAttributes.getNamedItem(BIT_RATE).getNodeValue()));
                        mediaInfoBuilder.duration(parseDuration(formatAttributes.getNamedItem(DURATION).getNodeValue()));
                        mediaInfoBuilder.format(formatAttributes.getNamedItem(FORMAT_NAME).getNodeValue());
                    } else if(node.getLocalName().equals(NODE_STREAMS)) {
                        NodeList streamNodes = node.getChildNodes();
                        for(int streamIndex = 0; streamIndex < streamNodes.getLength(); streamIndex++) {
                            Node steamNode = streamNodes.item(streamIndex);
                            NamedNodeMap streamAttributes = steamNode.getAttributes(); //STREAM ATTRIBUTES
                            switch(streamAttributes.getNamedItem(CODEC_TYPE).getNodeValue()) {
                                case AUDIO:
                                    mediaInfoBuilder.stream(AudioStream.builder()
                                            .index(parseInt(streamAttributes.getNamedItem(INDEX).getNodeValue()))
                                            .codecName(streamAttributes.getNamedItem(CODEC_NAME).getNodeValue())
                                            .codecType(CodecType.valueOf(streamAttributes.getNamedItem(CODEC_TYPE).getNodeValue().toUpperCase()))
                                            .channels(parseInt(streamAttributes.getNamedItem(CHANNELS).getNodeValue()))
                                            .sampleRate(parseSampleRate(streamAttributes.getNamedItem(SAMPLE_RATE).getNodeValue()))
                                            .build()
                                    );
                                    break;
                                case VIDEO:
                                    mediaInfoBuilder.stream(VideoStream.builder()
                                            .index(parseInt(streamAttributes.getNamedItem(INDEX).getNodeValue()))
                                            .codecName(streamAttributes.getNamedItem(CODEC_NAME).getNodeValue())
                                            .codecType(CodecType.valueOf(streamAttributes.getNamedItem(CODEC_TYPE).getNodeValue().toUpperCase()))
                                            .frameRate(parseFrameRate(streamAttributes.getNamedItem(FRAME_RATE).getNodeValue()))
                                            .size(new VideoSize(
                                                    parseInt(streamAttributes.getNamedItem(WIDTH).getNodeValue()), parseInt(streamAttributes.getNamedItem(HEIGHT).getNodeValue()))
                                            )
                                            .build()
                                    );
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            throw new FFmpegExecutionException("Failed to parse multimedia information from XML !");
        }
        return DefaultMultiMediaResponse.builder().body(mediaInfoBuilder.build()).build();
    }

    /**
     * Validates XML with XSD schema.
     *
     * @param document document (required)
     */
    private void validateXml(@NonNull Document document) {
        try {
            Validator validator = xsdSchema.newValidator();
            validator.validate(new DOMSource(document));
        } catch (IOException | SAXException ex) {
            if(strictXmlValidation) {
                throw new FFmpegValidationException("Failed to validate XML against XSD schema !", ex);
            }
        }
    }
}
