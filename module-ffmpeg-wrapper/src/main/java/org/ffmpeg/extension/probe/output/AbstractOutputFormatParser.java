package org.ffmpeg.extension.probe.output;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;

import java.math.BigDecimal;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.indexOf;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * Abstract output format parser for common logic.
 *
 * @author Sikke303
 * @since 1.0
 * @see OutputFormatParser
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
abstract class AbstractOutputFormatParser<T extends ProbeMultiMediaResponse> implements OutputFormatParser {

    protected static final String NODE_FORMAT = "format";
    protected static final String NODE_STREAMS = "streams";

    protected static final String CHANNELS = "channels";
    protected static final String BIT_RATE = "bit_rate";
    protected static final String DURATION = "duration";
    protected static final String CODEC_NAME = "codec_name";
    protected static final String CODEC_TYPE = "codec_type";
    protected static final String FORMAT_NAME = "format_name";
    protected static final String FRAME_RATE = "r_frame_rate";
    protected static final String INDEX = "index";
    protected static final String SAMPLE_RATE = "sample_rate";
    protected static final String HEIGHT = "height";
    protected static final String WIDTH = "width";

    @NonNull
    private final FFmpegToolkit toolkit;

    /**
     * Parses duration to number.
     *
     * @param duration duration (required)
     * @return duration in millis
     */
    protected long parseDuration(@NonNull final String duration) {
        return toolkit.convertDuration(duration);
    }

    /**
     * Parses bit rate to number.
     *
     * @param _bitRate bit rate (required)
     * @return bit rate (Kbit/s)
     */
    protected int parseBitRate(@NonNull final String _bitRate) {
        String bitRate = trimToEmpty(_bitRate.substring(0, indexOf(_bitRate, " ") + 1));
        if(containsIgnoreCase(_bitRate, "Mbit/s")) {
            return new BigDecimal(bitRate).multiply(new BigDecimal("1000")).intValue();
        }
        return new BigDecimal(bitRate).intValue();
    }

    /**
     * Parses frame rate to number.
     *
     * @param _frameRate frame rate (required)
     * @return frame rate per seconds
     */
    protected int parseFrameRate(@NonNull final String _frameRate) {
        String frameRate = trimToEmpty(_frameRate.substring(0, indexOf(_frameRate, "/")));
        return new BigDecimal(frameRate).intValue();
    }

    /**
     * Parses sample rate to number.
     *
     * @param _sampleRate sample rate (required)
     * @return bit rate (Kbit/s)
     */
    protected int parseSampleRate(@NonNull final String _sampleRate) {
        String sampleRate = trimToEmpty(_sampleRate.substring(0, indexOf(_sampleRate, " KHz")));
        return new BigDecimal(sampleRate).multiply(new BigDecimal("1000")).intValue();
    }
}
