package org.ffmpeg.extension.probe.output;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.exception.FFmpegConfigurationException;
import org.ffmpeg.api.extension.probe.output.OutputFormat;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;
import org.ffmpeg.api.extension.probe.output.OutputFormatParserRegistry;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashSet;
import java.util.Set;

/**
 * Default Output format parser registry.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
@Slf4j
public class DefaultOutputFormatParserRegistry implements OutputFormatParserRegistry<OutputFormatParser> {

    private final Set<OutputFormatParser> registry = new HashSet<>();

    @Nonnull
    @Override
    public OutputFormatParser getParser(@NonNull final OutputFormat outputFormat) {
        return registry.stream()
                .filter(outputFormatParser -> outputFormatParser.getOutputFormat() == outputFormat)
                .findAny()
                .orElseThrow(() -> new FFmpegConfigurationException("No output format parser registered for format: " + outputFormat));
    }

    @Override
    public void register(@NonNull final OutputFormatParser parser) {
        if(registry.stream().anyMatch(outputFormatParser -> outputFormatParser.getOutputFormat() == parser.getOutputFormat())) {
            throw new FFmpegConfigurationException("Already registerd output format parser for format: " + parser.getOutputFormat());
        }
        log.trace("Registering output format parser for format: {}", parser.getOutputFormat());
        registry.add(parser);
    }
}
