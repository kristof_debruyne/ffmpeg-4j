package org.ffmpeg.extension.play;

import lombok.NonNull;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.core.FFmpegEngineImpl;

/**
 * Play framework implementation.
 *
 * @author Sikke303
 * @since 1.0
 * @see org.ffmpeg.api.extension.FFmpegExtension
 */
public class FFplayImpl extends FFmpegEngineImpl implements FFplay {

    public FFplayImpl(@NonNull FFmpegExecutorFactory executorFactory, @NonNull FFmpegToolkit toolkit) { super(executorFactory, toolkit); }
}
