package org.ffmpeg.core;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.api.extension.play.common.FFplayConstants;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.extension.probe.common.FFprobeConstants;
import org.ffmpeg.api.multimedia.MultiMediaResponse;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;
import static org.ffmpeg.api.extension.probe.output.OutputFormat.JSON;

/**
 * Default FFMPEG platform.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegContext
 */
@ParametersAreNonnullByDefault
@Slf4j
public class FFmpegPlatformImpl implements FFmpegPlatform {

    private final Set<FFmpegExtension> extensions;

    @Getter
    private final FFmpeg framework;

    @Getter
    private final FFmpegToolkit toolkit;

    @Builder
    private FFmpegPlatformImpl(@NonNull FFmpeg framework,
                               @NonNull FFmpegToolkit toolkit,
                               @NonNull Set<FFmpegExtension> extensions) {
        this.framework = framework;
        this.toolkit = toolkit;
        this.extensions = unmodifiableSet(extensions);
    }

    /**
     * Gets multimedia information from given resource.
     *
     * If ffprobe is configured, it will use ffprobe in favor of the default resolving !
     *
     * @param resource resource (required)
     * @return optional multi media info (never null)
     */
    @Nonnull
    public Optional<MultiMediaResponse> getMultiMediaInformation(@NonNull final Path resource) {
        log.debug("Get multimedia information: {}", resource.toAbsolutePath());
        return getProbeFramework()
                .map(ffprobe -> ffprobe.getMultiMediaParser().parse(ProbeMultiMediaRequest.builder().resource(resource).outputFormat(JSON).build()))
                .orElseGet(() -> framework.getMultiMediaParser().parse(DefaultMultiMediaRequest.builder().resource(resource).build()));
    }

    @Nonnull
    @Override
    public Optional<FFprobe> getProbeFramework() {
        Optional<FFmpegExtension> extension = getExtension(FFprobeConstants.FFPROBE_QUALIFIER);
        if(extension.isPresent() && extension.get() instanceof FFprobe) {
            return Optional.of((FFprobe) extension.get());
        }
        return Optional.empty();
    }

    @Nonnull
    @Override
    public Optional<FFplay> getPlayFramework() {
        Optional<FFmpegExtension> extension = getExtension(FFplayConstants.FFPLAY_QUALIFIER);
        if(extension.isPresent() && extension.get() instanceof FFplay) {
            return Optional.of((FFplay) extension.get());
        }
        return Optional.empty();
    }

    @Override
    public boolean containsExtension(@NonNull final String name) {
        return getExtension(name).isPresent();
    }

    @Nonnull
    @Override
    public Optional<FFmpegExtension> getExtension(@NonNull final String name) {
        return extensions.stream().filter(extensions -> extensions.getName().equals(name)).findAny();
    }

    @Nonnegative
    @Override
    public int getExtensionCount() { return extensions.size(); }
}
