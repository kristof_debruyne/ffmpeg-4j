package org.ffmpeg.core.toolkit;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;
import org.ffmpeg.api.core.converter.FFmpegConverter;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.options.FFmpegOption;
import org.ffmpeg.api.core.options.GenericOption;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.dsl.SimpleFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.core.AbstractFFmpegSupport;
import org.ffmpeg.core.annotation.Tracing;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Stream;

import static org.ffmpeg.api.core.common.FFmpegConstant.NO_VALUE_SET;
import static org.ffmpeg.api.core.options.GenericOption.MAX_ALLOCATE;
import static org.ffmpeg.core.converter.DurationConverter.DURATION_CONVERTER_NAME;
import static org.ffmpeg.core.validator.BitDepthValidator.BIT_DEPTH_VALIDATOR_NAME;
import static org.ffmpeg.core.validator.BitRateValidator.BIT_RATE_VALIDATOR_NAME;
import static org.ffmpeg.core.validator.ChannelValidator.CHANNEL_VALIDATOR_NAME;
import static org.ffmpeg.core.validator.SampleRateValidator.SAMPLE_RATE_VALIDATOR_NAME;

@ParametersAreNonnullByDefault
public class DefaultFFmpegToolkit extends AbstractFFmpegSupport implements FFmpegToolkit {

    @Getter
    private final Collection<FFmpegConverter<?, ?>> converters = new HashSet<>();

    @Getter
    private final Collection<FFmpegConverter<?, ?>> customConverters = new HashSet<>();

    @Getter
    private final Collection<FFmpegValidator<?>> customValidators = new HashSet<>();

    @Getter
    private final Collection<FFmpegValidator<?>> validators = new HashSet<>();

    public DefaultFFmpegToolkit(@NonNull FFmpegExecutorFactory executorFactory,
                                @NonNull Collection<FFmpegConverter<?, ?>> converters,
                                @NonNull Collection<FFmpegValidator<?>> validators) {
        super(executorFactory);
        this.converters.addAll(converters);
        this.validators.addAll(validators);
    }

    @Override
    public boolean isComplexOption(@NonNull final String argument) {
        return !isSimpleOption(argument);
    }

    @Override
    public boolean isSimpleOption(@NonNull final String argument) {
        Validate.isTrue(argument.startsWith("-"), "Argument needs to start with dash (-)");
        return Arrays.stream(GenericOption.values())
                .map(FFmpegOption::getProperties)
                .anyMatch(option -> option.getArgument().equals(argument) || option.getAliases().contains(argument));
    }

    @Tracing
    @Nonnull
    @Override
    public String maxAllocate(@Nonnegative final long bytes) {
        if(bytes < 1) {
            throw new FFmpegValidationException("Max allocates requires a number > 0");
        }
        return execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(MAX_ALLOCATE, bytes)).asValue();
    }

    //*** CONVERTERS ***//

    @Tracing
    @Nonnull
    @Override
    public <T extends FFmpegConverter<?, ?>> Optional<T> getConverter(@NonNull final String name) {
        //noinspection unchecked
        return (Optional<T>) Stream.concat(converters.stream(), customConverters.stream())
                .filter(converter -> converter.getName().equals(name))
                .findAny();
    }

    @Tracing
    @Override
    public <T extends FFmpegConverter<?, ?>> void registerConverter(@NonNull final T converter) {
        if(customConverters.stream().noneMatch(customConverter -> customConverter.getName().equals(converter.getName()))) {
            customConverters.add(converter);
        }
    }

    @Tracing
    @Override
    public long convertDuration(@NonNull final String duration) {
        Optional<FFmpegConverter<String, Long>> result = getConverter(DURATION_CONVERTER_NAME);
        return result.map(stringLongFFmpegConverter -> stringLongFFmpegConverter.convert(duration)).orElse(NO_VALUE_SET);
    }

    //*** VALIDATORS ***//

    @Tracing
    @Nonnull
    @Override
    public <T extends FFmpegValidator<?>> Optional<T> getValidator(@NonNull final String name) {
        //noinspection unchecked
        return (Optional<T>) Stream.concat(validators.stream(), customValidators.stream())
                .filter(validator -> validator.getName().equals(name))
                .findAny();
    }

    @Tracing
    @Override
    public <T extends FFmpegValidator<?>> void registerValidator(@NonNull final T validator) {
        if(customValidators.stream().noneMatch(customValidator -> customValidator.getName().equals(validator.getName()))) {
            customValidators.add(validator);
        }
    }

    @Tracing
    @Override
    public void validateBitDepth(@Nonnegative final int bitDepth) {
        Optional<FFmpegValidator<Integer>> result = getValidator(BIT_DEPTH_VALIDATOR_NAME);
        result.ifPresent(validator -> validator.validate(bitDepth));
    }

    @Tracing
    @Override
    public void validateBitRate(@Nonnegative final int bitRate) {
        Optional<FFmpegValidator<Integer>> result = getValidator(BIT_RATE_VALIDATOR_NAME);
        result.ifPresent(validator -> validator.validate(bitRate));
    }

    @Tracing
    @Override
    public void validateChannel(@Nonnegative final int channel) {
        Optional<FFmpegValidator<Integer>> result = getValidator(CHANNEL_VALIDATOR_NAME);
        result.ifPresent(validator -> validator.validate(channel));
    }

    @Tracing
    @Override
    public void validateSampleRate(@Nonnegative final int sampleRate) {
        Optional<FFmpegValidator<Integer>> result = getValidator(SAMPLE_RATE_VALIDATOR_NAME);
        result.ifPresent(validator -> validator.validate(sampleRate));
    }
}
