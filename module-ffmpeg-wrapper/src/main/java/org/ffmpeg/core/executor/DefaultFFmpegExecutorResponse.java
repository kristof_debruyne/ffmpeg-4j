package org.ffmpeg.core.executor;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.core.executor.FFmpegExecutorResponse;
import org.ffmpeg.api.core.process.FFmpegProcessStatus;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Default executor result. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorResponse
 */
@RequiredArgsConstructor
@Builder
public class DefaultFFmpegExecutorResponse implements FFmpegExecutorResponse {

    private static final long serialVersionUID = 0L;

    @Getter
    private final FFmpegProcessStatus status;

    private final List<String> items;

    @Nonnull
    public List<String> asValues() { return Collections.unmodifiableList(items); }

    @Nonnull
    public String asValue() {
        return toSingleValue("\n");
    }

    @Nonnull
    public String asRawValue() { return toSingleValue(""); }

    @Nonnull
    public Stream<String> asStream(boolean parallel) {
        if(items == null || items.isEmpty()) {
            return Stream.empty();
        }
        return parallel ? items.parallelStream() : items.stream();
    }

    @Override
    public String toString() {
        return asValue();
    }

    @Nonnull
    private String toSingleValue(@NonNull final String separator) {
        if(items != null && !items.isEmpty()) {
            return String.join(separator, items);
        }
        return EMPTY;
    }
}
