package org.ffmpeg.core.executor;


import lombok.Getter;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.core.executor.AbstractFFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorThread;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.core.annotation.Tracing;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Optional;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

/**
 * Default FFMPEG executor implementation. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractFFmpegExecutor
 */
@Slf4j
public class DefaultFFmpegExecutor extends AbstractFFmpegExecutor {

    @Getter
    private boolean executed;
    private FFmpegExecutorThread processThread;

    @Nonnull
    @Override
    public Optional<InputStream> getErrorStream() {
        return ofNullable(processThread).flatMap(thread -> of(thread.getProcess().getErrorStream()));
    }

    @Nonnull
    @Override
    public Optional<InputStream> getInputStream() {
        return ofNullable(processThread).flatMap(thread -> of(thread.getProcess().getInputStream()));
    }

    @Nonnull
    @Override
    public Optional<OutputStream> getOutputStream() {
        return ofNullable(processThread).flatMap(thread -> of(thread.getProcess().getOutputStream()));
    }

    @Tracing
    @Synchronized
    @Nonnull
    @Override
    public FFmpegBufferedReader execute() {
        if(isExecuted()) {
            throw new FFmpegExecutionException("FFmpeg executor is already consumed and can't be reused !");
        }
        log.debug("Executing FFMPEG process instantly ...");
        try {
            Process process = start(); //PROCESS
            processThread = new FFmpegExecutorThread(process); //PROCESS THREAD
            registerShutdownHook(processThread);
            return new FFmpegBufferedReader(new InputStreamReader(process.getInputStream()));
        } catch(Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            throw new FFmpegExecutionException("FFmpeg execution has failed ! Reason: " + ex.getMessage());
        } finally {
            executed = true;
        }
    }

    @Tracing
    @Synchronized
    @Override
    public void destroy() {
        log.debug("Destroying FFMPEG process instantly ...");
        if(isExecuted()) {
            unregisterShutdownHook(processThread);
        }
    }

    @Override
    public final void close() { destroy(); }
}
