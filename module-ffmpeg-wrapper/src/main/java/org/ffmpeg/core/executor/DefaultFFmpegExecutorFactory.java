package org.ffmpeg.core.executor;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.exception.FFmpegExecutionException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;

/**
 * Default FFMPEG executor factory implementation. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorFactory
 */
@RequiredArgsConstructor
@Getter
@ParametersAreNonnullByDefault
@Slf4j
public class DefaultFFmpegExecutorFactory implements FFmpegExecutorFactory {

    private final boolean redirectErrorStream;

    @NonNull
    private final Path executorPath;

    @Nonnull
    @Override
    public FFmpegExecutor newInstance(@NonNull final String ...commands) {
        return newInstance(getExecutorPath(), commands);
    }

    @Nonnull
    @Override
    public FFmpegExecutor newInstance(@NonNull final Path path, @NonNull final String ...commands) {
        return newInstance(DefaultFFmpegExecutor.class, path, commands);
    }

    @Nonnull
    @Override
    public FFmpegExecutor newInstance(@NonNull final Class<? extends FFmpegExecutor> clazz, @NonNull final Path path, @NonNull final String ...commands) {
        log.trace("Attempt to create executor instance of type: {}", clazz.getName());
        try {
            FFmpegExecutor executor = clazz.getDeclaredConstructor().newInstance();
            executor.setExecutorPath(path); //EXECUTOR PATH
            executor.setCommands(commands); //EXECUTOR COMMANDS
            log.info("Collecting executor commands: {}", ArrayUtils.toString(commands));
            if(isRedirectErrorStream()) {
                executor.setRedirectErrorStream(true); //REDIRECT ERROR
            }
            log.trace("Created new executor of type: {}", executor.getClass().getName());
            return executor;
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            throw new FFmpegExecutionException("Failed to construct executor of type: " + clazz.getName());
        }
    }
}