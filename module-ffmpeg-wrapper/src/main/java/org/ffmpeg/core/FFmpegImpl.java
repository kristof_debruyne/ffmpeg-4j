package org.ffmpeg.core;

import lombok.Getter;
import lombok.NonNull;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;

@Getter
public class FFmpegImpl extends FFmpegEngineImpl implements FFmpeg {

    private final FFmpegProcessListener processListener;
    private final MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser;

    public FFmpegImpl(@NonNull FFmpegExecutorFactory executorFactory,
                      @NonNull FFmpegProcessListener processListener,
                      @NonNull FFmpegToolkit toolkit,
                      @NonNull MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser) {
        super(executorFactory, toolkit);
        this.multiMediaParser = multiMediaParser;
        this.processListener = processListener;
    }
}
