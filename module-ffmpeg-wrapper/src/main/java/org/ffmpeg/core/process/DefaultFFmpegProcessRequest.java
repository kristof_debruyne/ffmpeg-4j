package org.ffmpeg.core.process;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import org.ffmpeg.api.core.process.FFmpegProcessAttributes;
import org.ffmpeg.api.core.process.FFmpegProcessCallback;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;

import java.nio.file.Path;
import java.util.Map;
import java.util.UUID;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@ToString
public class DefaultFFmpegProcessRequest implements FFmpegProcessRequest {

    private final UUID uuid = UUID.randomUUID();
    private final boolean hideBanner;
    private final boolean overwriteOutputFiles;
    private final Path source;
    private final Path target;
    private final FFmpegProcessCallback callback;
    private final FFmpegProcessAttributes attributes;

    @Singular("addGlobalOption")
    private final Map<String, Object> globalOptions;

    @Singular("addInputOption")
    private final Map<String, Object> inputOptions;

    @Singular("addOutputOption")
    private final Map<String, Object> outputOptions;
}
