package org.ffmpeg.core.process;


import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.core.process.FFmpegProcessAttributes;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegCallbackException;
import org.ffmpeg.api.multimedia.MultiMediaInfo;
import org.ffmpeg.core.AbstractFFmpegSupport;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.apache.commons.lang3.StringUtils.stripEnd;
import static org.apache.commons.lang3.StringUtils.stripStart;
import static org.ffmpeg.api.core.options.MainOption.INPUT_SOURCE;
import static org.ffmpeg.api.core.options.MainOption.OVERWRITE_OUTPUT_FILES;
import static org.ffmpeg.api.core.options.SharedOptions.HIDE_BANNER;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.CANCELLED;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.ERROR;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.FAILED;
import static org.ffmpeg.api.core.process.FFmpegProcessStatus.SUCCESS;

/**
 * Abstract class for all processors. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractFFmpegSupport
 * @see FFmpegProcessor
 */
@ParametersAreNonnullByDefault
@Getter
@Slf4j
public abstract class AbstractFFmpegProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse>
		extends AbstractFFmpegSupport implements FFmpegProcessor<Request, Response> {

	private final FFmpegPlatform platform;

	protected AbstractFFmpegProcessor(@NonNull FFmpegPlatform platform) {
		super(platform.getFramework().getExecutorFactory());
		this.platform = platform;
	}

	/**
	 * Prepares builder.
	 *
	 * @return builder (never null)
	 */
	@Nonnull
	protected Function<FFmpegProcessRequest, ComplexFFmpegExecutorBuilder> parseRequest() {
		return (FFmpegProcessRequest request) -> {
			log.trace("Preparing process: {}", request.getSource().toAbsolutePath());

			ComplexFFmpegExecutorBuilder builder = newExecutorBuilder(ComplexFFmpegExecutorBuilder.class)
					.registerOption(INPUT_SOURCE, request.getSource().toAbsolutePath().toString());

			request.getGlobalOptions().forEach(builder::registerGlobalOption); //GLOBAL
			request.getInputOptions().forEach(builder::registerInputOption); //INPUT
			request.getOutputOptions().forEach(builder::registerOutputOption); //OUTPUT

			if(request.isHideBanner()) {
				builder.registerOption(HIDE_BANNER);
			}
			if(request.isOverwriteOutputFiles()) {
				builder.registerOption(OVERWRITE_OUTPUT_FILES);
			}
			return builder;
		};
	}

	/**
	 * Executes process.
	 *
	 * - SUCCESS: process ended successfully
	 * - SUCCESS_WITH_WARNINGS: process ended succssfully, but with warings (should be fixed preferrably)
	 * - FAILED: process failed for some reason (wrong argument, ...)
	 * - ERROR: error occurred during process (system)
	 * - IGNORED: process is ignored by the system
	 * - CANCELLED: process is cancelled by the user (callback)
	 *
	 * @param request request (required)
	 * @return optional response (never null)
	 */
	protected Function<FFmpegExecutorBuilder, Optional<DefaultFFmpegProcessResponse>> execute(@NonNull FFmpegProcessRequest request) {
		return (FFmpegExecutorBuilder builder) -> {
			log.info("Executing process request ! Request UUID: {}", request.getUuid());
			if(StringUtils.endsWithIgnoreCase(request.getSource().toString(), ".txt")) {
				try {
					return Optional.of(executeInternal(request, builder).apply(getDurationFromInputFile(request.getSource())));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			return Optional.of(executeInternal(request, builder).apply(getDuration(request.getSource())));
		};
	}

	@Nonnegative
	private long getDurationFromInputFile(@NonNull final Path source) throws IOException {
		return Files.readAllLines(source).stream()
				.map(line -> stripEnd(stripStart(line, "file '"), "'"))
				.map(Paths::get)
				.map(this::getDuration)
				.reduce(0L, Long::sum);
	}

	@Nonnegative
	private long getDuration(@NonNull final Path path) {
		return getPlatform().getMultiMediaInformation(path)
				.map(DefaultMultiMediaResponse.class::cast)
				.map(DefaultMultiMediaResponse::getBody)
				.map(MultiMediaInfo::getDuration)
				.orElse(0L);
	}

	@Nonnull
	private Function<Long, DefaultFFmpegProcessResponse> executeInternal(final FFmpegProcessRequest request, final FFmpegExecutorBuilder builder) {
		return (Long duration) -> {
			DefaultFFmpegProcessResponse.DefaultFFmpegProcessResponseBuilder responseBuilder = DefaultFFmpegProcessResponse.builder()
					.uuid(request.getUuid()).target(request.getTarget());
			try (FFmpegExecutor executor = builder.build();
				 FFmpegBufferedReader reader = executor.execute()) {

				FFmpegProcessListener processListener = getPlatform().getFramework().getProcessListener();
				if(streamAll(reader).anyMatch(processListener.onProcessLine(request.getCallback(), duration))) {
					log.info("Executed process request ! Request UUID: {}", request.getUuid());
					responseBuilder.status(SUCCESS);
				} else {
					responseBuilder.status(FAILED);
				}
			} catch (Exception ex) {
				if(ex instanceof FFmpegCallbackException) {
					responseBuilder.status(CANCELLED);
				} else {
					log.error(ExceptionUtils.getStackTrace(ex), ex);
					responseBuilder.status(ERROR);
				}
			}
			return responseBuilder.build();
		};
	}

	/**
	 * Configures builder for given request.
	 *
	 * @param request request (required)
	 * @return function (never null)
	 */
	@Nonnull
	protected abstract Function<ComplexFFmpegExecutorBuilder, FFmpegExecutorBuilder> configure(@NonNull DefaultFFmpegProcessRequest request);

	/**
	 * Validates attributes.
	 *
	 * @param <T> attributes type
	 * @return predicate (never null)
	 */
	@Nonnull
	protected abstract <T extends FFmpegProcessAttributes> Predicate<T> validateAttributes();
}
