package org.ffmpeg.core.process;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.process.FFmpegProcessCallback;

import javax.annotation.Nullable;

import static org.ffmpeg.core.process.DefaultFFmpegProcessListener.MAXIMUM_PROGRESS;
import static org.ffmpeg.core.process.DefaultFFmpegProcessListener.MINIMUM_PROGRESS;

/**
 * Default FFMPEG process callback. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessCallback
 */
@Slf4j
public class DefaultFFmpegProcessCallback implements FFmpegProcessCallback {

    @Getter
    @Setter
    private boolean cancelled;

    @Override
    public void update(final long progress, @Nullable String text) {
        if(progress < MINIMUM_PROGRESS) {
            throw new IllegalArgumentException("Minimum process value should be: 0");
        } else if(progress > MAXIMUM_PROGRESS) {
            throw new IllegalArgumentException("Maximum process value should be: 100");
        }
        log.trace("Process: {} / 100", progress);
    }
}
