package org.ffmpeg.core.process;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.core.process.FFmpegProcessStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Optional;
import java.util.UUID;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class DefaultFFmpegProcessResponse implements FFmpegProcessResponse {

    @NonNull
    private final UUID uuid;

    @Nullable
    private final Path target;

    @NonNull
    private final FFmpegProcessStatus status;

    @Singular("message")
    private final Collection<String> messages;

    @Override
    public boolean isFailed() {
        return status == FFmpegProcessStatus.FAILED;
    }

    @Override
    public boolean isSuccess() {
        return EnumSet.of(FFmpegProcessStatus.SUCCESS, FFmpegProcessStatus.SUCCESS_WITH_WARNINGS).contains(status);
    }

    @Nonnull
    @Override
    public Collection<String> getMessages() {
        return emptyIfNull(messages);
    }

    @Nonnull
    public Optional<Path> getTarget() {
        return Optional.ofNullable(target);
    }

    @Override
    public boolean hasWarnings() {
        return !this.getMessages().isEmpty();
    }
}
