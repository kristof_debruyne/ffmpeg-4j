package org.ffmpeg.core.process;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.core.annotation.Tracing;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@ParametersAreNonnullByDefault
@Slf4j
public class DefaultFFmpegProcessor extends AbstractFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> {

	public DefaultFFmpegProcessor(FFmpegPlatform platform) {
		super(platform);
	}

	@Tracing
	@Nonnull
	@Override
	public Optional<DefaultFFmpegProcessResponse> process(@NonNull final DefaultFFmpegProcessRequest request) {
		return parseRequest()
				.andThen(configure(request))
				.andThen(execute(request))
				.apply(request);
	}

	//*** HELPER FUNCTIONS ***//

	@Nonnull
	protected Function<ComplexFFmpegExecutorBuilder, FFmpegExecutorBuilder> configure(@NonNull DefaultFFmpegProcessRequest request) {
		return (ComplexFFmpegExecutorBuilder builder) -> builder.withOutputPath(request.getTarget().toAbsolutePath().toString());
	}

	@SuppressWarnings("unchecked")
	@Nonnull
	protected Predicate<?> validateAttributes() {
		return (Object attributes) -> true;
	}
}
