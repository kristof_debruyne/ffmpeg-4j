package org.ffmpeg.core.process;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.process.FFmpegProcessor;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.api.subtitle.processor.SubtitleProcessor;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.ffmpeg.audio.DefaultAudioProcessor;
import org.ffmpeg.subtitle.DefaultSubtitleProcessor;
import org.ffmpeg.video.DefaultVideoProcessor;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Default FFMPEG processor factory implementation. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessorFactory
 */
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class DefaultFFmpegProcessorFactory implements FFmpegProcessorFactory {

    @NonNull
    private final FFmpegPlatform platform;

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends FFmpegProcessor<?, ?>> T newInstance(@NonNull final Class<T> type) {
        if(type.equals(AudioProcessor.class)) {
            return (T) new DefaultAudioProcessor(platform);
        } else if(type.equals(SubtitleProcessor.class)) {
            return (T) new DefaultSubtitleProcessor(platform);
        } else if(type.equals(VideoProcessor.class)) {
            return (T) new DefaultVideoProcessor(platform);
        } else if(type.equals(DefaultFFmpegProcessor.class)) {
            return (T) new DefaultFFmpegProcessor(platform);
        } else {
            throw new FFmpegExecutionException("No factory found for type: " + type.getName());
        }
    }
}
