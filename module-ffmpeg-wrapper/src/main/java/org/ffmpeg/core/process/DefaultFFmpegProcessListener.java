package org.ffmpeg.core.process;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.process.FFmpegProcessCallback;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.exception.FFmpegCallbackException;
import org.ffmpeg.core.converter.DurationConverter;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.nonNull;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;
import static org.ffmpeg.api.core.common.FFmpegConstant.NO_VALUE_SET;

/**
 * Default process listener.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
@Slf4j
public class DefaultFFmpegProcessListener implements FFmpegProcessListener {

    public static final int MINIMUM_PROGRESS = 0;
    public static final int MAXIMUM_PROGRESS = 100;

    private static final Pattern PROGRESS_PATTERN = compile("^.*time=(.*?)\\s.*$", CASE_INSENSITIVE);
    private static final Pattern PROCESS_SUCCESS_PATTERN = compile("^.*?video:.*?audio:.*?subtitle.*$", CASE_INSENSITIVE);
    private static final String PROCESSING_MASK = "Processing ... (%d / 100)";

    @NonNull
    private final DurationConverter durationConverter;

    /**
     * Checks if process is finished or not.
     *
     * @param callback callback (optional)
     * @param duration duration (required)
     * @return predicate that checks if process is finished or not
     */
    @Override
    @Nonnull
    public Predicate<String> onProcessLine(@Nullable final FFmpegProcessCallback callback, @Nonnegative final long duration) {
        return (String line) -> {
            if(nonNull(callback) && callback.isCancelled()) {
                throw new FFmpegCallbackException("FFmpeg process has been cancelled by user !");
            }
            long progress = MINIMUM_PROGRESS;
            if(duration > MINIMUM_PROGRESS) {
                progress = calculateProgress(line, duration);
            }
            if(nonNull(callback) && progress != NO_VALUE_SET) {
                callback.update(progress, String.format(PROCESSING_MASK, progress));
            }
            return PROCESS_SUCCESS_PATTERN.matcher(line).matches();
        };
    }

    /**
     * Calculates progress. <br>
     *
     * @param line progress line (required)
     * @param totalDuration total duration (not negative)
     * @return int progress
     */
    long calculateProgress(@NonNull final String line, final long totalDuration) {
        if(totalDuration > 0) {
            Matcher matcher = PROGRESS_PATTERN.matcher(line);
            if(matcher.matches()) {
                long duration = durationConverter.convert(trimToEmpty(matcher.group(1)));
                if(duration != NO_VALUE_SET) {
                    int result = new BigDecimal(Long.toString(duration))
                            .divide(new BigDecimal(Long.toString(totalDuration)), 2, BigDecimal.ROUND_HALF_UP)
                            .multiply(new BigDecimal("100.00"))
                            .intValue();
                    return Math.min(result, MAXIMUM_PROGRESS);
                }
            }
        }
        return NO_VALUE_SET;
    }
}
