package org.ffmpeg.core;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.executor.FFmpegExecutorResponse;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.startsWithIgnoreCase;
import static org.ffmpeg.dsl.FFmpegExecutorBuilderFactoryImpl.newExecutorBuilderFactory;

/**
 * Abstract FFMPEG support. <br><br>
 *
 * This class provides extra support for FFMPEG execution. <br><br>
 *
 * There are some helper collect methods which accept a builder so you <br>
 * have the ability as a consumer to populate any builder you desire. <br><br>
 *
 * Besides that, it is also more safe to use those collect methods as <br>
 * they are wrapped into a try-with-resources block so they are closed nicely. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorFactory
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@ParametersAreNonnullByDefault
@Slf4j
public abstract class AbstractFFmpegSupport {

    protected static final String DEFAULT_EVALUATOR = "-";

    @Getter
    @Nonnull
    @NonNull
    private final FFmpegExecutorFactory executorFactory;

    @Nonnull
    protected final <T extends FFmpegExecutorBuilder> T newExecutorBuilder(@NonNull final Class<T> type) {
        return newExecutorBuilderFactory(getExecutorFactory()).ofType(type);
    }

    /**
     * Executes builder and returns response.
     *
     * @param builder executor builder (required)
     * @return response (never null)
     */
    @Nonnull
    protected final FFmpegExecutorResponse execute(@NonNull FFmpegExecutorBuilder builder) {
        try(FFmpegExecutor executor = builder.build();
            FFmpegBufferedReader reader = executor.execute()) {
            return DefaultFFmpegExecutorResponse.builder().items(streamAll(reader).collect(toList())).build();
        } catch (Exception ex) {
            throw new FFmpegExecutionException("Failed to execute commands ! Reason: " + ex.getMessage(), ex);
        }
    }

    //*** COLLECT METHODS ***//

    /**
     * Executes builder and collects results in a streaming fashion (+ callback)
     *
     * @param builder builder (required)
     * @param callback callback function (required)
     * @param <T> type
     * @return set of items of type T
     */
    @Nonnull
    protected final <T> Set<T> collect(@NonNull FFmpegExecutorBuilder builder, @NonNull Function<String, Optional<T>> callback) {
        return collect(builder, callback, DEFAULT_EVALUATOR);
    }

    /**
     * Executes builder and collects results in a streaming fashion (+ callback)
     *
     * @param builder builder (required)
     * @param callback callback function (required)
     * @param evaluator legend/value evaluator (required)
     * @param <T> type
     * @return set of items of type T
     */
    @Nonnull
    protected final <T> Set<T> collect(@NonNull FFmpegExecutorBuilder builder, @NonNull Function<String, Optional<T>> callback,
                                 @NonNull String evaluator) {
        try(FFmpegExecutor executor = builder.build();
            FFmpegBufferedReader reader = executor.execute()) {
            return streamIf(reader, callback, evaluator).collect(toCollection(LinkedHashSet::new));
        } catch (Exception ex) {
            throw new FFmpegExecutionException("Failed to collect results ! Reason: " + ex.getMessage(), ex);
        }
    }

    //*** STREAM METHODS ***//

    @Nonnull
    protected final Stream<String> streamAll(@NonNull FFmpegBufferedReader reader) {
        return reader.lines()
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .peek(this::log);
    }

    @Nonnull
    protected final <T> Stream<T> streamIf(@NonNull FFmpegBufferedReader reader, @NonNull Function<String, Optional<T>> callback,
                                           @NonNull String evaluator) {
        AtomicBoolean EVALUATED = new AtomicBoolean(false);
        return streamAll(reader)
                .peek(line -> {
                    if (startsWithIgnoreCase(line, evaluator)) {
                        EVALUATED.set(true);
                    }
                })
                .filter(line -> EVALUATED.get())
                .skip(1)
                .flatMap(line -> callback.apply(line).map(Stream::of).orElse(Stream.empty()));
    }

    private void log(@NonNull final String message) { log.info(message); }
}
