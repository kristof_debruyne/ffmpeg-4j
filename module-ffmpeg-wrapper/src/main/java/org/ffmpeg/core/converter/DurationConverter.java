package org.ffmpeg.core.converter;

import lombok.NonNull;
import org.ffmpeg.api.core.converter.FFmpegConverter;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.rightPad;
import static org.ffmpeg.api.core.common.FFmpegConstant.NO_VALUE_SET;

@ParametersAreNonnullByDefault
public class DurationConverter implements FFmpegConverter<String, Long> {

    public static final String DURATION_CONVERTER_NAME = "DEFAULT_DURATION_CONVERTER";
    private static final Pattern DURATION_PATTERN = compile("^(\\d{1,2}):(\\d\\d):(\\d\\d)\\.(\\d{1,6})$");

    @Nonnull
    @Override
    public String getName() { return DURATION_CONVERTER_NAME; }

    @Nonnull
    @Override
    public Long convert(@NonNull final String source) {
        Matcher matcher = DURATION_PATTERN.matcher(source);
        if(matcher.matches()) {
            int hours = Integer.parseInt(matcher.group(1));
            int minutes = Integer.parseInt(matcher.group(2));
            int seconds = Integer.parseInt(matcher.group(3));
            int milliSeconds = Integer.parseInt(rightPad(matcher.group(4), 6, "0")) / 1000;
            return calculateDurationInMillis(hours, minutes, seconds, milliSeconds);
        }
        return NO_VALUE_SET;
    }

    /**
     * Parses given time in milliseconds.
     *
     * @param hours hours
     * @param minutes minutes
     * @param seconds seconds
     * @param milliSeconds milliseconds
     * @return duration in milliseconds or -1 (not available)
     */
    private static long calculateDurationInMillis(final int hours, final int minutes, final int seconds, final int milliSeconds) {
        return milliSeconds + (seconds * 1000L) + (minutes * 60L * 1000L) + (hours * 60L * 60L * 1000L);
    }
}
