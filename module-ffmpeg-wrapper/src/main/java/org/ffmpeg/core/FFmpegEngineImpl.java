package org.ffmpeg.core;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.FFmpegEngine;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.core.codec.muxing.demuxer.DeMuxer;
import org.ffmpeg.api.core.codec.muxing.muxer.Muxer;
import org.ffmpeg.api.core.common.Color;
import org.ffmpeg.api.core.common.Device;
import org.ffmpeg.api.core.common.Filter;
import org.ffmpeg.api.core.common.Format;
import org.ffmpeg.api.core.common.HelpMode;
import org.ffmpeg.api.core.common.Layout;
import org.ffmpeg.api.core.common.PixelFormat;
import org.ffmpeg.api.core.common.Protocol;
import org.ffmpeg.api.core.common.SampleFormat;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.executor.FFmpegExecutorResponse;
import org.ffmpeg.api.core.options.GenericOption;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.SimpleFFmpegExecutorBuilder;
import org.ffmpeg.core.annotation.Tracing;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.startsWithIgnoreCase;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;
import static org.ffmpeg.api.core.common.FFmpegConstant.BAND_SUPPORT;
import static org.ffmpeg.api.core.common.FFmpegConstant.BITSTREAM_FORMAT;
import static org.ffmpeg.api.core.common.FFmpegConstant.COMMAND_SUPPORT;
import static org.ffmpeg.api.core.common.FFmpegConstant.DECODER;
import static org.ffmpeg.api.core.common.FFmpegConstant.DEMUXING;
import static org.ffmpeg.api.core.common.FFmpegConstant.DIRECT_RENDERING_SUPPORT;
import static org.ffmpeg.api.core.common.FFmpegConstant.ENCODER;
import static org.ffmpeg.api.core.common.FFmpegConstant.EXPERIMENTAL;
import static org.ffmpeg.api.core.common.FFmpegConstant.FRAME_LEVEL_THREADING;
import static org.ffmpeg.api.core.common.FFmpegConstant.HARDWARE_ACCELERATED_FORMAT;
import static org.ffmpeg.api.core.common.FFmpegConstant.INPUT_SUPPORT;
import static org.ffmpeg.api.core.common.FFmpegConstant.INTRA_FRAME_ONLY;
import static org.ffmpeg.api.core.common.FFmpegConstant.LOSSLESS_COMPRESSION;
import static org.ffmpeg.api.core.common.FFmpegConstant.LOSSY_COMPRESSION;
import static org.ffmpeg.api.core.common.FFmpegConstant.MUXING;
import static org.ffmpeg.api.core.common.FFmpegConstant.NOT_AVAILABLE;
import static org.ffmpeg.api.core.common.FFmpegConstant.OUTPUT_SUPPORT;
import static org.ffmpeg.api.core.common.FFmpegConstant.PALETTED_FORMAT;
import static org.ffmpeg.api.core.common.FFmpegConstant.SLICE_LEVEL_THREADING;
import static org.ffmpeg.api.core.common.FFmpegConstant.TIME_LINE_SUPPORT;
import static org.ffmpeg.api.core.options.GenericOption.BITSTREAM_FILTERS;
import static org.ffmpeg.api.core.options.GenericOption.BUILD_CONFIGURATION;
import static org.ffmpeg.api.core.options.GenericOption.CODECS;
import static org.ffmpeg.api.core.options.GenericOption.COLORS;
import static org.ffmpeg.api.core.options.GenericOption.CPU_FLAGS;
import static org.ffmpeg.api.core.options.GenericOption.DECODERS;
import static org.ffmpeg.api.core.options.GenericOption.DEMUXERS;
import static org.ffmpeg.api.core.options.GenericOption.DEVICES;
import static org.ffmpeg.api.core.options.GenericOption.ENCODERS;
import static org.ffmpeg.api.core.options.GenericOption.FILTERS;
import static org.ffmpeg.api.core.options.GenericOption.FORMATS;
import static org.ffmpeg.api.core.options.GenericOption.HELP;
import static org.ffmpeg.api.core.options.GenericOption.LAYOUTS;
import static org.ffmpeg.api.core.options.GenericOption.LICENSE;
import static org.ffmpeg.api.core.options.GenericOption.MUXERS;
import static org.ffmpeg.api.core.options.GenericOption.PIXEL_FORMATS;
import static org.ffmpeg.api.core.options.GenericOption.PROTOCOLS;
import static org.ffmpeg.api.core.options.GenericOption.REPORT;
import static org.ffmpeg.api.core.options.GenericOption.SAMPLE_FORMATS;
import static org.ffmpeg.api.core.options.GenericOption.VERSION;

/**
 * FFmpeg engine implementation. <br><br>
 *
 * This class provides a set of methods that return meta data of the framework. <br>
 * The {@link GenericOption} entries are implemented by this class. Each method maps to a value in that enumeration. <br><br>
 *
 * Besides that, you also have the ability to just execute raw commands (advanced users only). <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractFFmpegSupport
 * @see FFmpegEngine
 */
@SuppressWarnings("all")
@ParametersAreNonnullByDefault
@Slf4j
public class FFmpegEngineImpl extends AbstractFFmpegSupport implements FFmpegEngine {

    private static final Pattern CODEC_PATTERN = compile("^([D.])([E.])([ASV])([I.])([L.])([S.]) (.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern COLOR_PATTERN = compile("^(.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern DEVICE_PATTERN = compile("^([D ])([E ]) (.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern FORMAT_PATTERN = compile("^([D ])([E ]) (.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern DEMUXING_PATTERN = compile("^([D])\\s+(.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern ENCODER_DECODER_PATTERN = compile("^([ASV])([F.])([S.])([X.])([B.])([D.]) (.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern FILTER_PATTERN = compile("^([T.])([S.])([C.]) (.+?)\\s+(.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern LAYOUT_PATTERN = compile("^(.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern MUXING_PATTERN = compile("^([E])\\s+(.+?)\\s+(.+)$", CASE_INSENSITIVE);
    private static final Pattern PIXEL_FORMAT_PATTERN = compile("^([I.])([O.])([H.])([P.])([B.]) (.+?)\\s+(\\d+)\\s+(\\d+)$", CASE_INSENSITIVE);
    private static final Pattern SAMPLE_FORMAT_PATTERN = compile("^(.+?)\\s+(\\d+)$", CASE_INSENSITIVE);
    private static final Pattern VERSION_PATTERN = compile("^(.+?) version (.+?)\\s+(.*)$", CASE_INSENSITIVE);

    @Getter
    private final FFmpegToolkit toolkit;

    public FFmpegEngineImpl(@NonNull FFmpegExecutorFactory executorFactory, @NonNull FFmpegToolkit toolkit) {
        super(executorFactory);
        this.toolkit = toolkit;
    }

    @Tracing
    @Nonnull
    @Override
    public Set<String> getBitStreamFilters() {
        log.trace("Loads bit stream filters.");
        return collect(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(BITSTREAM_FILTERS), optionalize(), "bitstream filters:");
    }

    @Tracing
    @Nonnull
    @Override
    public Set<String> getBuildConfigurations() {
        log.trace("Loads build configurations.");
        return collect(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(BUILD_CONFIGURATION), optionalize(), "configuration:");
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Codec> getCodecs(@NonNull final CodecType codecType) {
        log.trace("Loads codecs for codec type: {}", codecType.name());
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(CODECS);
        return collect(builder, line -> {
            Matcher matcher = CODEC_PATTERN.matcher(line);
            if (matcher.matches() && equalsIgnoreCase(codecType.getValue(), matcher.group(3))) {
                return of(Codec.builder()
                        .name(matcher.group(7))
                        .description(matcher.group(8))
                        .codecType(CodecType.getByValue(matcher.group(3)))
                        .decodingSupport(matcher.group(1).equalsIgnoreCase(DECODER))
                        .encodingSupport(matcher.group(2).equalsIgnoreCase(ENCODER))
                        .intraFrameOnly(matcher.group(4).equalsIgnoreCase(INTRA_FRAME_ONLY))
                        .lossyCompression(matcher.group(5).equalsIgnoreCase(LOSSY_COMPRESSION))
                        .losslessCompression(matcher.group(6).equalsIgnoreCase(LOSSLESS_COMPRESSION))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Color> getColors() {
        log.trace("Loads colors.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(COLORS);
        return collect(builder, line -> {
            Matcher matcher = COLOR_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Color.builder().name(matcher.group(1)).value(matcher.group(2)).build());
            }
            return empty();
        }, "name");
    }

    @Tracing
    @Nonnull
    @Override
    public List<String> getCpuFlags(@NonNull final String option) {
        log.trace("Loads CPU flags: {}", option);
        return execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(CPU_FLAGS, option)).asValues();
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Decoder> getDecoders(@NonNull final CodecType codecType) {
        log.trace("Loads decoders for codec type: {}", codecType.name());
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(DECODERS);
        return collect(builder, line -> {
            Matcher matcher = ENCODER_DECODER_PATTERN.matcher(line);
            if (matcher.matches() && equalsIgnoreCase(codecType.getValue(), matcher.group(1))) {
                return of(Decoder.builder()
                        .name(matcher.group(7))
                        .codecType(codecType)
                        .description(matcher.group(8))
                        .frameLevelThreading(matcher.group(2).equalsIgnoreCase(FRAME_LEVEL_THREADING))
                        .sliceLevelThreading(matcher.group(3).equalsIgnoreCase(SLICE_LEVEL_THREADING))
                        .experimental(matcher.group(4).equalsIgnoreCase(EXPERIMENTAL))
                        .bandSupport(matcher.group(5).equalsIgnoreCase(BAND_SUPPORT))
                        .directRenderingSupport(matcher.group(6).equalsIgnoreCase(DIRECT_RENDERING_SUPPORT))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<DeMuxer> getDemuxers() {
        log.trace("Loads demuxers.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(DEMUXERS);
        return collect(builder, line -> {
            Matcher matcher = DEMUXING_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(DeMuxer.builder().name(matcher.group(2)).description(matcher.group(3)).build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Device> getDevices() {
        log.trace("Loads devices.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(DEVICES);
        return collect(builder, line -> {
            Matcher matcher = DEVICE_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Device.builder()
                        .name(matcher.group(3))
                        .description(matcher.group(4))
                        .demuxingSupport(matcher.group(1).equalsIgnoreCase(DEMUXING))
                        .muxingSupport(matcher.group(2).equalsIgnoreCase(MUXING))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Encoder> getEncoders(@NonNull final CodecType codecType) {
        log.trace("Loads encoders for codec type: {}", codecType.name());
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(ENCODERS);
        return collect(builder, line -> {
            Matcher matcher = ENCODER_DECODER_PATTERN.matcher(line);
            if (matcher.matches() && equalsIgnoreCase(codecType.getValue(), matcher.group(1))) {
                return of(Encoder.builder()
                        .name(matcher.group(7))
                        .codecType(codecType)
                        .description(matcher.group(8))
                        .frameLevelThreading(matcher.group(2).equalsIgnoreCase(FRAME_LEVEL_THREADING))
                        .sliceLevelThreading(matcher.group(3).equalsIgnoreCase(SLICE_LEVEL_THREADING))
                        .experimental(matcher.group(4).equalsIgnoreCase(EXPERIMENTAL))
                        .bandSupport(matcher.group(5).equalsIgnoreCase(BAND_SUPPORT))
                        .directRenderingSupport(matcher.group(6).equalsIgnoreCase(DIRECT_RENDERING_SUPPORT))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Filter> getFilters() {
        log.trace("Loads filters.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(FILTERS);
        return collect(builder, line -> {
            Matcher matcher = FILTER_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Filter.builder()
                        .name(matcher.group(4))
                        .description(matcher.group(6))
                        .transition(matcher.group(5))
                        .timeLineSupport(matcher.group(1).equalsIgnoreCase(TIME_LINE_SUPPORT))
                        .sliceThreading(matcher.group(2).equalsIgnoreCase(SLICE_LEVEL_THREADING))
                        .commandSupport(matcher.group(3).equalsIgnoreCase(COMMAND_SUPPORT))
                        .build());
            }
            return empty();
        }, "| =");
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Format> getFormats() {
        log.trace("Loads formats.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(FORMATS);
        return collect(builder, line -> {
            Matcher matcher = FORMAT_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Format.builder()
                        .name(matcher.group(3))
                        .description(matcher.group(4))
                        .demuxingSupport(matcher.group(1).equalsIgnoreCase(DEMUXING))
                        .muxingSupport(matcher.group(2).equalsIgnoreCase(MUXING))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public String getHelp() { return getHelp(HelpMode.FULL); }

    @Tracing
    @Nonnull
    @Override
    public String getHelp(@NonNull final HelpMode helpMode) {
        log.trace("Loads help.");
        return execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(HELP, helpMode)).asValue();
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Layout> getLayouts() {
        log.trace("Loads layouts.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(LAYOUTS);
        return collect(builder, line -> {
            Matcher matcher = LAYOUT_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Layout.builder().name(matcher.group(1)).decomposition(matcher.group(2)).build());
            }
            return empty();
        }, "name");
    }

    @Tracing
    @Nonnull
    @Override
    public String getLicense() {
        log.trace("Loads license.");
        return execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(LICENSE)).asValue();
    }

    @Tracing
    @Nonnull
    @Override
    public String getName() {
        String version = getVersion(true);
        if(StringUtils.isNotBlank(version)) {
            return StringUtils.substring(version, 0, version.indexOf(" "));
        }
        return NOT_AVAILABLE;
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Muxer> getMuxers() {
        log.trace("Loads muxers.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(MUXERS);
        return collect(builder, line -> {
            Matcher matcher = MUXING_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(Muxer.builder().name(matcher.group(2)).description(matcher.group(3)).build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<PixelFormat> getPixelFormats() {
        log.trace("Loads pixel formats.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(PIXEL_FORMATS);
        return collect(builder, line -> {
            Matcher matcher = PIXEL_FORMAT_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(PixelFormat.builder()
                        .name(matcher.group(6))
                        .inputSupported(matcher.group(1).equalsIgnoreCase(INPUT_SUPPORT))
                        .outputSupported(matcher.group(2).equalsIgnoreCase(OUTPUT_SUPPORT))
                        .hardwareAcceleratedFormat(matcher.group(3).equalsIgnoreCase(HARDWARE_ACCELERATED_FORMAT))
                        .palettedFormat(matcher.group(4).equalsIgnoreCase(PALETTED_FORMAT))
                        .bitStreamFormat(matcher.group(5).equalsIgnoreCase(BITSTREAM_FORMAT))
                        .numberOfComponents(Integer.parseInt(matcher.group(7)))
                        .bitsPerPixel(Integer.parseInt(matcher.group(8)))
                        .build());
            }
            return empty();
        });
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Protocol> getProtocols(final boolean input) {
        log.trace("Loads protocols.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(PROTOCOLS);
        AtomicBoolean START_PROCESSING = new AtomicBoolean(false);
        return collect(builder, line -> {
            if(input && startsWithIgnoreCase(line, "Output:")) {
                START_PROCESSING.set(false);
            }
            if(START_PROCESSING.get()) {
                return of(Protocol.builder().name(trimToEmpty(line)).input(input).build());
            }
            if(startsWithIgnoreCase(line, input ? "Input:" : "Output:")) {
                START_PROCESSING.set(true);
            }
            return empty();
        }, "supported file protocols:");
    }

    @Tracing
    @Nonnull
    @Override
    public Set<SampleFormat> getSampleFormats() {
        log.trace("Loads sample formats.");
        FFmpegExecutorBuilder builder = newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(SAMPLE_FORMATS);
        return collect(builder, line -> {
            Matcher matcher = SAMPLE_FORMAT_PATTERN.matcher(line);
            if (matcher.matches()) {
                return of(SampleFormat.builder().name(matcher.group(1)).depth(Integer.parseInt(matcher.group(2))).build());
            }
            return empty();
        }, "name");
    }

    @Tracing
    @Nonnull
    @Override
    public String getReport() {
        log.trace("Loads report.");
        return execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(REPORT)).asValue();
    }

    @Tracing
    @Nonnull
    @Override
    public String getVersion() {
        return getVersion(false);
    }

    @Tracing
    @Nonnull
    @Override
    public String getVersion(boolean full) {
        log.trace("Loads version.");
        FFmpegExecutorResponse result = execute(newExecutorBuilder(SimpleFFmpegExecutorBuilder.class).registerOption(VERSION));
        if(full) {
            return result.asValue();
        }
        return result.asStream(false)
                .map(VERSION_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(matcher -> trimToEmpty(matcher.group(2)))
                .findAny()
                .orElse(DEFAULT_EVALUATOR);
    }

    @Nonnull
    private  <T> Function<T, Optional<T>> optionalize() { return Optional::ofNullable; }
}
