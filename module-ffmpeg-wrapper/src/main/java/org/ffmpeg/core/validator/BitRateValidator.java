package org.ffmpeg.core.validator;

import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class BitRateValidator implements FFmpegValidator<Integer> {

    public static final String BIT_RATE_VALIDATOR_NAME = "DEFAULT_BIT_RATE_VALIDATOR";
    private static final int[] SUPPORTED_VALUES = { 96, 128, 160, 192, 256, 320 };

    @Nonnull
    @Override
    public String getName() { return BIT_RATE_VALIDATOR_NAME; }

    /**
     * Validates bit rate value&#46; <br><br>
     *
     * Possible values are: <br>
     * - 96: 96 kbps <br>
     * - 128: 128 kbps <br>
     * - 160: 160 kbps <br>
     * - 192: 192 kbps <br>
     * - 256: 256 kbps <br>
     * - 320: 320 kbps <br>
     *
     * @param bitRate bit rate (required)
     * @throws org.ffmpeg.api.exception.FFmpegValidationException if value is invalid
     */
    @Override
    public void validate(@NonNull final Integer bitRate) {
        if(!ArrayUtils.contains(SUPPORTED_VALUES, bitRate)) {
            throw new FFmpegValidationException("Bit rate value is invalid ! Value: " + bitRate);
        }
    }
}
