package org.ffmpeg.core.validator;

import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class BitDepthValidator implements FFmpegValidator<Integer> {

    public static final String BIT_DEPTH_VALIDATOR_NAME = "DEFAULT_BIT_DEPTH_VALIDATOR";
    private static final int[] SUPPORTED_VALUES = {  8, 16, 24 };

    @Nonnull
    @Override
    public String getName() { return BIT_DEPTH_VALIDATOR_NAME; }

    /**
     * Validates bit depth value&#46; <br><br>
     *
     * Possible values are: <br>
     * - 16: 16-bit depth <br>
     * - 24: 24-bit depth <br> (not supported by standard Java API)
     * - 32: 32-bit depth <br> (not supported by standard Java API)
     *
     * @param bitDepth bit depth (required)
     * @throws org.ffmpeg.api.exception.FFmpegValidationException if value is invalid
     */
    @Override
    public void validate(@NonNull final Integer bitDepth) {
        if(!ArrayUtils.contains(SUPPORTED_VALUES, bitDepth)) {
            throw new FFmpegValidationException("Bit depth value is invalid ! Value: " + bitDepth);
        }
    }
}
