package org.ffmpeg.core.validator;

import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class SampleRateValidator implements FFmpegValidator<Integer> {

    public static final String SAMPLE_RATE_VALIDATOR_NAME = "DEFAULT_SAMPLE_RATE_VALIDATOR";
    private static final int[] SUPPORTED_VALUES = { 11025, 22050, 32000, 44100, 48000 };

    @Nonnull
    @Override
    public String getName() { return SAMPLE_RATE_VALIDATOR_NAME; }

    /**
     * Validates sample rate value&#46; <br><br>
     *
     * Possible values are: <br>
     * - 11025: 11 Hz <br>
     * - 22050: 22 kHz <br>
     * - 32000: 32 kHz <br>
     * - 44100: 44 kHz <br>
     * - 48000: 48 kHz <br>
     *
     * @param sampleRate sample rate (required)
     * @throws org.ffmpeg.api.exception.FFmpegValidationException if sample rate is invalid
     */
    @Override
    public void validate(@NonNull final Integer sampleRate) {
        if(!ArrayUtils.contains(SUPPORTED_VALUES, sampleRate)) {
            throw new FFmpegValidationException("Sample rate value is invalid ! Value: " + sampleRate);
        }
    }
}
