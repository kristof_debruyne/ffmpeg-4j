package org.ffmpeg.core.validator;

import lombok.NonNull;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class ChannelValidator implements FFmpegValidator<Integer> {

    public static final String CHANNEL_VALIDATOR_NAME = "DEFAULT_CHANNEL_VALIDATOR";

    @Nonnull
    @Override
    public String getName() { return CHANNEL_VALIDATOR_NAME; }

    /**
     * Validates channels value&#46; <br><br>
     *
     * Possible values are: <br>
     * - 1: mono channel <br>
     * - 2: stereo channel <br>
     *
     * @param channels channels (required)
     * @throws FFmpegValidationException if channel is invalid
     */
    @Override
    public void validate(@NonNull final Integer channels) {
        if(channels < 1 || channels > 2) {
            throw new FFmpegValidationException("Channel value is invalid ! Value: " + channels);
        }
    }
}
