package org.ffmpeg.core.validator;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Validator that validates executor paths. <br><br>
 *
 * Checks performed: <br>
 * - class path executor <br>
 * - file system executor <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegValidator
 */
@NoArgsConstructor(staticName = "of")
@ParametersAreNonnullByDefault
public class PathValidator implements FFmpegValidator<Path> {

    public static final String PATH_VALIDATOR_NAME = "DEFAULT_PATH_VALIDATOR_NAME";

    @Nonnull
    @Override
    public String getName() { return PATH_VALIDATOR_NAME; }

    @Override
    public void validate(@NonNull final Path input) {
        String absolutePath = input.toAbsolutePath().toString();
        if(!(isClassPathExecutor(absolutePath) || isFileSystemExecutor(absolutePath))) {
            throw new FFmpegValidationException("Executor path is not class or file system path ! Path: " + absolutePath);
        } else if(StringUtils.containsIgnoreCase(absolutePath, "-all")) {
            throw new FFmpegValidationException("Wrapper doesn't support all executables ! Path: " + absolutePath);
        }
    }

    /**
     * Checks if executor path is a class path executor.
     *
     * @param path path (required)
     * @return boolean classpath executor (true) or not (false)
     */
    private boolean isClassPathExecutor(@NonNull final String path) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(path);
        if(isNull(resource)) {
            return nonNull(PathValidator.class.getResource(path));
        }
        return true;
    }

    /**
     * Checks if executor path is a file system executor.
     *
     * @param path path (required)
     * @return boolean file system executor (true) or not (false)
     */
    private boolean isFileSystemExecutor(@NonNull final String path) {
        return Files.exists(Paths.get(path));
    }
}
