package org.ffmpeg.audio;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import static java.util.Objects.nonNull;

/**
 * Audio attributes container. <br>
 * 
 * @author Sikke303
 * @since v3.03.09
 * @see AudioAttributes
 */
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class AudioAttributes implements Serializable {

	private static final long serialVersionUID = 0L;

	private final Integer bitRate;
	private final Integer channels;
	private final Integer sampleRate;
	private final Integer volume;
	private final String codec;

	public final boolean hasCodec() {
		return StringUtils.isNotBlank(codec);
	}
	
	public final boolean hasBitRate() {
		return nonNull(bitRate) && bitRate > 0;
	}
	
	public final boolean hasChannels() {
		return nonNull(channels) && (channels == 1 || channels == 2);
	}
	
	public final boolean hasSampleRate() { return nonNull(sampleRate) && sampleRate > 0; }
	
	public final boolean hasVolume() { return nonNull(volume); }
}
