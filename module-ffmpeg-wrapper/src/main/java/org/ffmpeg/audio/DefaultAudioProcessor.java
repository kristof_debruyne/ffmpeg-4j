package org.ffmpeg.audio;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.core.process.AbstractFFmpegProcessor;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.isNull;
import static org.ffmpeg.api.audio.option.AudioOption.BIT_RATE;
import static org.ffmpeg.api.audio.option.AudioOption.CHANNELS;
import static org.ffmpeg.api.audio.option.AudioOption.CODEC;
import static org.ffmpeg.api.audio.option.AudioOption.SAMPLE_RATE;
import static org.ffmpeg.api.core.codec.CodecType.AUDIO;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.MainOption.DURATION;
import static org.ffmpeg.api.core.options.MainOption.FORCE_FORMAT;
import static org.ffmpeg.api.core.options.MainOption.SEEK_POSITION;


/**
 * Default audio processor. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AudioProcessAttributes
 */
@ParametersAreNonnullByDefault
@Slf4j
public class DefaultAudioProcessor extends AbstractFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>
		implements AudioProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> {

	public DefaultAudioProcessor(FFmpegPlatform platform) { super(platform); }

	@Tracing
	@Override
	public boolean isAudioCodec(@NonNull final String name) {
		return getAudioCodecs().stream().anyMatch(codec -> codec.getName().equals(name));
	}

	@Tracing
	@Override
	public boolean isAudioDecoder(@NonNull final String name) {
		return getAudioDecoders().stream().anyMatch(decoder -> decoder.getCodec().equals(name));
	}

	@Tracing
	@Override
	public boolean isAudioEncoder(@NonNull final String name) {
		return getAudioEncoders().stream().anyMatch(encoder -> encoder.getCodec().equals(name));
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Codec> getAudioCodecs() {
		return getPlatform().getFramework().getCodecs(AUDIO);
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Decoder> getAudioDecoders() {
		return getPlatform().getFramework().getDecoders(AUDIO);
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Encoder> getAudioEncoders() {
		return getPlatform().getFramework().getEncoders(AUDIO);
	}

	@Tracing
	@Nonnull
	@Override
	public Optional<DefaultFFmpegProcessResponse> process(@NonNull final DefaultFFmpegProcessRequest request) {
		return Optional.of(request.getAttributes())
				.filter(AudioProcessAttributes.class::isInstance)
				.map((AudioProcessAttributes.class::cast))
				.filter(validateAttributes())
				.flatMap(attributes -> parseRequest()
						.andThen(configure(request))
						.andThen(execute(request))
						.apply(request)
				);
	}

	//*** HELPER FUNCTIONS ***//

	@Nonnull
	protected Function<ComplexFFmpegExecutorBuilder, FFmpegExecutorBuilder> configure(@NonNull DefaultFFmpegProcessRequest request) {
		return (ComplexFFmpegExecutorBuilder builder) -> {
			final AudioProcessAttributes attributes = (AudioProcessAttributes) request.getAttributes();
			builder.registerOption(attributes.hasOffset(), SEEK_POSITION, attributes.getOffset(), OUTPUT);
			builder.registerOption(attributes.hasDuration(), DURATION, attributes.getDuration(), OUTPUT);
			builder.registerOption(attributes.hasFormat(), FORCE_FORMAT, attributes.getFormat(), OUTPUT);
			final AudioAttributes audioAttributes = attributes.getAudioAttributes();
			builder.registerOption(audioAttributes.hasCodec(), CODEC, audioAttributes.getCodec(), OUTPUT);
			builder.registerOption(audioAttributes.hasBitRate(), BIT_RATE, String.format("%dk", audioAttributes.getBitRate()), OUTPUT);
			builder.registerOption(audioAttributes.hasChannels(), CHANNELS, audioAttributes.getChannels(), OUTPUT);
			builder.registerOption(audioAttributes.hasSampleRate(), SAMPLE_RATE, audioAttributes.getSampleRate(), OUTPUT);
			return builder.withOutputPath(request.getTarget().toAbsolutePath().toString());
		};
	}

	@SuppressWarnings("unchecked")
	@Nonnull
	protected Predicate<AudioProcessAttributes> validateAttributes() {
		return (AudioProcessAttributes attributes) -> {
			if(isNull(attributes.getAudioAttributes())) {
				throw new IllegalArgumentException("Illegal argument detected ! Audio attributes can't be null ...");
			}
			return true;
		};
	}
}
