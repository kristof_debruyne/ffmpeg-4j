package org.ffmpeg.audio;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.process.FFmpegProcessAttributes;

import static java.util.Objects.nonNull;

/**
 * Audio process attributes. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessAttributes
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class AudioProcessAttributes implements FFmpegProcessAttributes {

	private static final long serialVersionUID = 0L;

	private final Float duration;
	private final Float offset;
	private final String format;
	private final AudioAttributes audioAttributes;

	public boolean hasDuration() {
		return nonNull(duration) && duration > 0;
	}

	public boolean hasOffset() {
		return nonNull(offset) && offset > -1;
	}

	public boolean hasFormat() {
		return StringUtils.isNotBlank(format);
	}
}
