package org.ffmpeg.audio;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.ffmpeg.api.core.FFmpegObject;
import org.ffmpeg.api.core.common.Stream;
import org.ffmpeg.api.core.common.Tag;

/**
 * Audio stream container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@SuperBuilder
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AudioStream extends Stream {

    private final int channels;
    private final int sampleRate;
    private final Tag tag;
}
