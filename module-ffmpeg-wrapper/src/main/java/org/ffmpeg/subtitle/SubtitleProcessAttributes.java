package org.ffmpeg.subtitle;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.core.process.FFmpegProcessAttributes;

/**
 * Subtitle process attributes. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessAttributes
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class SubtitleProcessAttributes implements FFmpegProcessAttributes {

	private static final long serialVersionUID = 0L;
}
