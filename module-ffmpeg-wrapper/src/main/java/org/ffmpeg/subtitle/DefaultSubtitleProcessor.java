package org.ffmpeg.subtitle;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.subtitle.processor.SubtitleProcessor;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.core.process.AbstractFFmpegProcessor;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Default subtitle processor. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractFFmpegProcessor
 * @see SubtitleProcessor
 */
@ParametersAreNonnullByDefault
@Slf4j
public class DefaultSubtitleProcessor extends AbstractFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>
        implements SubtitleProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> {

    public DefaultSubtitleProcessor(FFmpegPlatform platform) { super(platform); }

    @Tracing
    @Override
    public boolean isSubtitleCodec(@NonNull final String name) {
        return getSubtitleCodecs().stream().anyMatch(codec -> codec.getName().equals(name));
    }

    @Tracing
    @Override
    public boolean isSubtitleDecoder(@NonNull final String name) {
        return getSubtitleDecoders().stream().anyMatch(decoder -> decoder.getCodec().equals(name));
    }

    @Tracing
    @Override
    public boolean isSubtitleEncoder(@NonNull final String name) {
        return getSubtitleEncoders().stream().anyMatch(encoder -> encoder.getCodec().equals(name));
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Codec> getSubtitleCodecs() {
        return getPlatform().getFramework().getCodecs(CodecType.SUBTITLE);
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Decoder> getSubtitleDecoders() {
        return getPlatform().getFramework().getDecoders(CodecType.SUBTITLE);
    }

    @Tracing
    @Nonnull
    @Override
    public Set<Encoder> getSubtitleEncoders() {
        return getPlatform().getFramework().getEncoders(CodecType.SUBTITLE);
    }

    @Tracing
    @Override
    public Optional<DefaultFFmpegProcessResponse> process(@NonNull final DefaultFFmpegProcessRequest request) {
        throw new UnsupportedOperationException("Currently not supported ! Coming soon ...");
    }

    @Nonnull
    @Override
    protected Function<ComplexFFmpegExecutorBuilder, FFmpegExecutorBuilder> configure(@NonNull DefaultFFmpegProcessRequest request) {
        return (ComplexFFmpegExecutorBuilder bulder) -> bulder.withOutputPath(request.getTarget().toAbsolutePath().toString());
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    @Override
    protected Predicate<SubtitleProcessAttributes> validateAttributes() {
        return (SubtitleProcessAttributes attributes) -> true;
    }
}
