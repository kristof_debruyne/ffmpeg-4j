package org.ffmpeg.dsl;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.core.common.LogLevel;
import org.ffmpeg.api.core.common.LogMode;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;

import static java.util.Arrays.stream;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.ffmpeg.api.core.options.SharedOptions.LOG_LEVEL;

/**
 * Abstract FFMPEG executor builder.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegExecutorBuilder
 */
@ParametersAreNonnullByDefault
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
abstract class AbstractFFmpegExecutorBuilder implements FFmpegExecutorBuilder {

    private LogLevel logLevel = LogLevel.INFO;
    private LogMode[] logModes = LogMode.values();

    @Getter
    private String outputPath;

    @Getter
    private Path executorPath;

    @NonNull
    private final FFmpegExecutorFactory executorFactory;

    @Nonnull
    @Override
    public FFmpegExecutor build() {
        String[] commands = addAll(getLogCommands(), getCommands());
        if(nonNull(getOutputPath() )) {
            commands = addAll(commands, getOutputPath());
        }
        log.debug("Collected commands for execution: {}", ArrayUtils.toString(commands));
        if(nonNull(getExecutorPath())) {
            return executorFactory.newInstance(getExecutorPath(), commands);
        }
        return executorFactory.newInstance(commands);
    }

    @Nonnull
    @Override
    public final FFmpegExecutorBuilder overrideExecutorPath(@NonNull final Path executorPath) {
        this.executorPath = executorPath;
        return this;
    }

    @Nonnull
    @Override
    public final FFmpegExecutorBuilder withLogLevel(@NonNull final LogLevel logLevel) {
        this.logLevel = logLevel;
        return this;
    }

    @Nonnull
    @Override
    public final FFmpegExecutorBuilder withLogModes(@NonNull final LogMode ... logModes) {
        this.logModes = logModes;
        return this;
    }

    @Nonnull
    @Override
    public final FFmpegExecutorBuilder withOutputPath(@NonNull final String outputPath) {
        this.outputPath = outputPath;
        return this;
    }

    @Nonnull
    protected final String[] getLogCommands() {
        StringBuilder builder = new StringBuilder(LOG_LEVEL.getProperties().getArgument().concat(" "));
        stream(logModes).forEach(logMode -> builder.append(logMode.getValue()).append("+"));
        builder.append(logLevel.getLevel());
        log.trace("Setting log level: {}", builder);
        return builder.toString().split(" ");
    }
}