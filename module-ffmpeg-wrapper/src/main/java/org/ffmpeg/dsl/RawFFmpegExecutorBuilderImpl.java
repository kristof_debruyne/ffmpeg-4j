package org.ffmpeg.dsl;

import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.RawFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class RawFFmpegExecutorBuilderImpl extends AbstractFFmpegExecutorBuilder implements RawFFmpegExecutorBuilder {

    private String[] commands;

    @Builder(buildMethodName = "and")
    private RawFFmpegExecutorBuilderImpl(@NonNull final FFmpegExecutorFactory withExecutorFactory) { super(withExecutorFactory); }

    @Nonnull
    @Override
    public String[] getCommands() {
        if(ArrayUtils.isEmpty(commands)) {
            throw new FFmpegValidationException("Please register a raw option(s) before executing ...");
        }
        return commands;
    }

    @Nonnull
    @Override
    public FFmpegExecutorBuilder withCommands(@NonNull String... commands) {
        this.commands = commands;
        return this;
    }
}