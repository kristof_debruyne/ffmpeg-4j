package org.ffmpeg.dsl;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilderFactory;
import org.ffmpeg.api.dsl.RawFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.SimpleFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegExecutionException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class FFmpegExecutorBuilderFactoryImpl implements FFmpegExecutorBuilderFactory {

    @Getter
    private final FFmpegExecutorFactory executorFactory;

    @Nonnull
    public static FFmpegExecutorBuilderFactory newExecutorBuilderFactory(@NonNull final FFmpegExecutorFactory executorFactory) {
        return new FFmpegExecutorBuilderFactoryImpl(executorFactory);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends FFmpegExecutorBuilder> T ofType(@NonNull final Class<T> type) {
        log.trace("Creating executor builder of type: {}", type.getName());
        if(type == SimpleFFmpegExecutorBuilder.class) {
            return (T) SimpleFFmpegExecutorBuilderImpl.builder().withExecutorFactory(executorFactory).and();
        } else if(type == ComplexFFmpegExecutorBuilder.class) {
            return (T) ComplexFFmpegExecutorBuilderImpl.builder().withExecutorFactory(executorFactory).and();
        } else if(type == RawFFmpegExecutorBuilder.class) {
            return (T) RawFFmpegExecutorBuilderImpl.builder().withExecutorFactory(executorFactory).and();
        }
        throw new FFmpegExecutionException("No factory found for type: " + type.getName());
    }
}
