package org.ffmpeg.dsl;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.options.SimpleFFmpegOption;
import org.ffmpeg.api.core.options.vo.FFmpegOptionValue;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.SimpleFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.isNull;

@ParametersAreNonnullByDefault
@Slf4j
public class SimpleFFmpegExecutorBuilderImpl extends AbstractFFmpegExecutorBuilder implements SimpleFFmpegExecutorBuilder {

    @Getter
    private FFmpegOptionValue registeredOption;

    @Builder(buildMethodName = "and")
    private SimpleFFmpegExecutorBuilderImpl(@NonNull final FFmpegExecutorFactory withExecutorFactory) { super(withExecutorFactory); }

    @Nonnull
    @Override
    public String[] getCommands() {
        if(isNull(registeredOption)) {
            throw new FFmpegValidationException("Please register a generic option before executing ...");
        }
        return registeredOption.toArray();
    }

    @Nonnull
    @Override
    public FFmpegExecutorBuilder registerOption(@NonNull final SimpleFFmpegOption option) {
        log.trace("Setting generic option [ {} ] for execution ...", option);
        this.registeredOption = new FFmpegOptionValue(option);
        return this;
    }

    @Nonnull
    @Override
    public FFmpegExecutorBuilder registerOption(@NonNull final SimpleFFmpegOption option, @Nullable final Object value) {
        if(isNull(option.getProperties().getValuePredicate())) {
            return registerOption(option);
        }
        log.trace("Setting generic option [ {} ] with value [ {} ] for execution ...", option, value);
        this.registeredOption = new FFmpegOptionValue(option, value);
        return this;
    }
}
