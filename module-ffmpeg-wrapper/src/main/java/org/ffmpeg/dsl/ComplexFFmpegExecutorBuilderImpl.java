package org.ffmpeg.dsl;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.options.ComplexFFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOption;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;
import org.ffmpeg.api.core.options.FFmpegOptionProperties;
import org.ffmpeg.api.core.options.MainOption;
import org.ffmpeg.api.core.options.vo.AbstractFFmpegOptionValue;
import org.ffmpeg.api.core.options.vo.FFmpegOptionValue;
import org.ffmpeg.api.core.options.vo.FFmpegRawOptionValue;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegValidationException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Arrays.stream;
import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toCollection;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.ANYWHERE;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.GLOBAL;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.INPUT;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;

@ParametersAreNonnullByDefault
@Slf4j
public class ComplexFFmpegExecutorBuilderImpl extends AbstractFFmpegExecutorBuilder implements ComplexFFmpegExecutorBuilder {

    @Getter
    private final Set<AbstractFFmpegOptionValue<?>> registeredOptions = new LinkedHashSet<>();

    @Builder(buildMethodName = "and")
    private ComplexFFmpegExecutorBuilderImpl(@NonNull final FFmpegExecutorFactory withExecutorFactory) { super(withExecutorFactory); }

    @Nonnull
    @Override
    public String[] getCommands() {
        if(registeredOptions.isEmpty()) {
            throw new FFmpegValidationException("No options registered ! Please add options before executing ...");
        }

        String[] commands = collectOptions(ANYWHERE) //ANYWHERE
                .andThen(collectOptions(GLOBAL)) //GLOBAL
                .andThen(collectOptions(INPUT)) //INPUT
                .andThen(collectOptions(OUTPUT)) //OUTPUT
                .andThen(convertToStringArray())
                .apply(emptySet());

        if(!ArrayUtils.contains(commands, MainOption.INPUT_SOURCE.getProperties().getArgument())) {
            throw new FFmpegValidationException("Required input option (-i) not registered !");
        }
        log.trace("Collected [ {} ] options for execution !", commands.length);
        return commands;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(@NonNull final ComplexFFmpegOption option) {
        return registerOption(option, null, option.getProperties().getLocations().get(0));
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(@NonNull final ComplexFFmpegOption option, @NonNull final FFmpegOptionLocation location) {
        return registerOption(option, null, location);
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(@NonNull final ComplexFFmpegOption option, @Nullable final Object value) {
        return registerOption(option, value, option.getProperties().getLocations().get(0));
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(@NonNull final ComplexFFmpegOption option, @Nullable final Object value, @NonNull final FFmpegOptionLocation location) {
        validate(option, value, location); //VALIDATE
        if(registeredOptions.add(new FFmpegOptionValue(option, value, location))) {
            log.trace("Registered option: {}", option.getProperties().getArgument());
        }
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option) {
        if(condition) {
            return registerOption(option);
        }
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nonnull FFmpegOptionLocation location) {
        if(condition) {
            return registerOption(option, location);
        }
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nullable Object value) {
        if(condition) {
            return registerOption(option, value);
        }
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOption(boolean condition, @Nonnull ComplexFFmpegOption option, @Nullable Object value, @Nonnull FFmpegOptionLocation location) {
        if(condition) {
            return registerOption(option, value, location);
        }
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerGlobalOption(@NonNull String option, @Nullable Object value) {
        this.registeredOptions.add(new FFmpegRawOptionValue(option, value, GLOBAL, 0));
        log.trace("Registered global option: {}", option);
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerInputOption(@NonNull String option, @Nullable Object value) {
        int order = 0;
        if(StringUtils.equals(option, MainOption.INPUT_SOURCE.getProperties().getArgument())) {
            order = MainOption.INPUT_SOURCE.getProperties().getOrder();
        }
        this.registeredOptions.add(new FFmpegRawOptionValue(option, value, INPUT, order));
        log.trace("Registered input option: {}", option);
        return this;
    }

    @Nonnull
    @Override
    public ComplexFFmpegExecutorBuilder registerOutputOption(@NonNull String option, @Nullable Object value) {
        this.registeredOptions.add(new FFmpegRawOptionValue(option, value, OUTPUT, 0));
        log.trace("Registered output option: {}", option);
        return this;
    }

    @Nonnull
    private Function<Set<AbstractFFmpegOptionValue<?>>, Set<AbstractFFmpegOptionValue<?>>> collectOptions(@NonNull final FFmpegOptionLocation location) {
        return (Set<AbstractFFmpegOptionValue<?>> options) -> {
            log.trace("Collecting options of locaiton type: {}", location);
            final Set<AbstractFFmpegOptionValue<?>> newOptions = new LinkedHashSet<>(options);
            newOptions.addAll(getAllByLocation(location));
            return unmodifiableSet(newOptions);
        };
    }

    @Nonnull
    private Function<Set<AbstractFFmpegOptionValue<?>>, String[]> convertToStringArray() {
        return (Set<AbstractFFmpegOptionValue<?>> options) -> options.stream()
                .flatMap((AbstractFFmpegOptionValue<?> value) -> stream(value.toArray()))
                .toArray(String[]::new);
    }

    @Nonnull
    private Set<AbstractFFmpegOptionValue<?>> getAllByLocation(@NonNull final FFmpegOptionLocation location) {
        return registeredOptions.stream().filter(getByLocation(location)).sorted().collect(toCollection(LinkedHashSet::new));
    }

    @Nonnull
    private Predicate<AbstractFFmpegOptionValue<?>> getByLocation(@NonNull final FFmpegOptionLocation location) {
        return (AbstractFFmpegOptionValue<?> optionValue) -> {
            if (optionValue instanceof FFmpegOptionValue) {
                List<FFmpegOptionLocation> locations = ((FFmpegOptionValue) optionValue).getOption().getProperties().getLocations();
                return locations.contains(location) && optionValue.getLocation() == location;
            }
            return optionValue.getLocation() == location;
        };
    }

    private void validate(@NonNull final FFmpegOption option, @Nullable final Object value, @Nullable final FFmpegOptionLocation location) {
        FFmpegOptionProperties properties = option.getProperties();
        if(nonNull(properties.getValuePredicate())) {
            if(isNull(value)) {
                throw new FFmpegValidationException(option.toString() + " requires a value ...");
            } else if(!properties.getValuePredicate().test(value)) {
                throw new FFmpegValidationException(option.toString() + " does not have correct value format ...");
            }
        }
        if(isNull(properties.getValuePredicate()) && nonNull(value)) {
            throw new FFmpegValidationException(option.toString() + " does not require a value ...");
        }
        if(properties.getLocations().size() > 1) {
            if(isNull(location)) {
                throw new FFmpegValidationException(option.toString() + " requires a location ...");
            } else if(properties.getLocations().contains(ANYWHERE)) {
                throw new FFmpegValidationException(option.toString() + " can't contain anywhere location ...");
            }
        }
    }
}
