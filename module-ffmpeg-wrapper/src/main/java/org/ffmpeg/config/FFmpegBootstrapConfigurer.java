package org.ffmpeg.config;

import lombok.NonNull;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.common.FFmpegConstant;
import org.ffmpeg.api.core.converter.FFmpegConverter;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegConfigurationException;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.context.FFmpegContextImpl;
import org.ffmpeg.core.FFmpegImpl;
import org.ffmpeg.core.FFmpegPlatformImpl;
import org.ffmpeg.core.converter.DurationConverter;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.core.process.DefaultFFmpegProcessListener;
import org.ffmpeg.core.process.DefaultFFmpegProcessorFactory;
import org.ffmpeg.core.toolkit.DefaultFFmpegToolkit;
import org.ffmpeg.core.validator.BitDepthValidator;
import org.ffmpeg.core.validator.BitRateValidator;
import org.ffmpeg.core.validator.ChannelValidator;
import org.ffmpeg.core.validator.PathValidator;
import org.ffmpeg.core.validator.SampleRateValidator;
import org.ffmpeg.multimedia.DefaultMultiMediaParser;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;
import static org.ffmpeg.core.converter.DurationConverter.DURATION_CONVERTER_NAME;

/**
 * FFmpeg configuration support (template). <br><br>
 *
 * Helper template when creating a ffmpeg configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see BootstrapConfigurer
 */
public interface FFmpegBootstrapConfigurer extends BootstrapConfigurer {

    /**
     * Gets home variable.
     *
     * @return home variable (never null)
     */
    @Nonnull
    default String getFFmpegHomeVariable() { return FFmpegConstant.FFMPEG_HOME; }

    /**
     * Creates framework.
     *
     * @param executorFactory executor factory (required)
     * @param processListener process listener (required)
     * @param toolkit toolkit (required)
     * @param multiMediaParser multimedia parser (required)
     * @return framework (never null)
     */
    @Nonnull
    default FFmpeg createFFmpegFramework(@NonNull FFmpegExecutorFactory executorFactory,
                                         @NonNull FFmpegProcessListener processListener,
                                         @NonNull FFmpegToolkit toolkit,
                                         @NonNull MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser) {
        return new FFmpegImpl(executorFactory, processListener, toolkit, multiMediaParser);
    }

    /**
     * Creates default multimedia parser.
     *
     * @param executorFactory executor factory (required)
     * @return multimedia parser (never null)
     */
    @Nonnull
    default MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> createMultiMediaParser(
            @NonNull FFmpegExecutorFactory executorFactory, @NonNull FFmpegToolkit toolkit) {
        return new DefaultMultiMediaParser(executorFactory, toolkit);
    }

    /**
     * Creates executor factory.
     *
     * @return executor factory (never null)
     */
    @Nonnull
    default FFmpegExecutorFactory createExecutorFactory() {
        PathValidator.of().validate(Paths.get(getProperties().getExecutorPath())); //VALIDATE
        return new DefaultFFmpegExecutorFactory(getProperties().isRedirectErrorStream(), Paths.get((getProperties().getExecutorPath())));
    }

    /**
     * Creates processor factory.
     *
     * @param platform platform (required)
     * @return processor factory (never null)
     */
    @Nonnull
    default FFmpegProcessorFactory createProcessorFactory(@NonNull FFmpegPlatform platform) {
        return new DefaultFFmpegProcessorFactory(platform);
    }

    /**
     * Creates process listener.
     *
     * @param toolkit toolkit (required)
     * @return process listener (never null)
     */
    @Nonnull
    default FFmpegProcessListener createProcessListener(@NonNull FFmpegToolkit toolkit) {
        DurationConverter durationConverter = (DurationConverter) toolkit.getConverter(DURATION_CONVERTER_NAME)
                .orElseThrow(() -> new FFmpegConfigurationException("No duration converter configured !"));
        return new DefaultFFmpegProcessListener(durationConverter);
    }

    /**
     * Creates toolkit with default validators.
     *
     * @param executorFactory executor factory (required)
     * @return toolkit (never null)
     */
    @Nonnull
    default FFmpegToolkit createToolkit(@NonNull FFmpegExecutorFactory executorFactory) {
        return createToolkit(executorFactory, createDefaultConverterCollection(), createDefaultValidatorCollection());
    }

    /**
     * Creates toolkit.
     *
     * @param executorFactory executor factory (required)
     * @param converters converters (optional)
     * @param validators validators (optional)
     * @return toolkit (never null)
     */
    @Nonnull
    default FFmpegToolkit createToolkit(@NonNull FFmpegExecutorFactory executorFactory,
                                        @Nullable Set<FFmpegConverter<?, ?>> converters,
                                        @Nullable Set<FFmpegValidator<?>> validators) {
        return new DefaultFFmpegToolkit(executorFactory, ofNullable(converters).orElse(emptySet()), ofNullable(validators).orElse(emptySet()));
    }

    /**
     * Create default converter collection. <br><br>
     *
     * Contains: <br>
     * - {@link DurationConverter} <br>
     *
     * @return set of converters (never null)
     */
    @Nonnull
    default Set<FFmpegConverter<?, ?>> createDefaultConverterCollection() {
        Set<FFmpegConverter<?, ?>> converters = new HashSet<>();
        converters.add(new DurationConverter());
        return Collections.unmodifiableSet(converters);
    }

    /**
     * Create default validator collection. <br><br>
     *
     * Contains: <br>
     * - {@link BitDepthValidator} <br>
     * - {@link BitRateValidator} <br>
     * - {@link ChannelValidator} <br>
     * - {@link SampleRateValidator} <br>
     *
     * @return set of validators (never null)
     */
    @Nonnull
    default Set<FFmpegValidator<?>> createDefaultValidatorCollection() {
        Set<FFmpegValidator<?>> validators = new HashSet<>();
        validators.add(new BitDepthValidator());
        validators.add(new BitRateValidator());
        validators.add(new ChannelValidator());
        validators.add(new SampleRateValidator());
        return Collections.unmodifiableSet(validators);
    }

    /**
     * Creates platform instance.
     *
     * @param framework framework (required)
     * @param toolkit toolkit (required)
     * @param extensions extensions (optional)
     * @return platform (never null)
     */
    @Nonnull
    default FFmpegPlatform createPlatform(@NonNull FFmpeg framework,
                                          @NonNull FFmpegToolkit toolkit,
                                          @NonNull Set<FFmpegExtension> extensions) {
        return FFmpegPlatformImpl.builder()
                .framework(framework)
                .extensions(extensions)
                .toolkit(toolkit)
                .build();
    }

    /**
     * Create context instance.
     *
     * @param platform platform (required)
     * @param executorFactory executor factory (required)
     * @param processorFactory processor factory (required)
     * @return context (never null)
     */
    @Nonnull
    default FFmpegContext createContext(@NonNull FFmpegPlatform platform,
                                        @NonNull FFmpegExecutorFactory executorFactory,
                                        @NonNull FFmpegProcessorFactory processorFactory) {
        return FFmpegContextImpl.builder().platform(platform)
                .executorFactory(executorFactory)
                .processorFactory(processorFactory)
                .build();
    }
}
