package org.ffmpeg.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.extension.probe.common.FFprobeConstants;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;
import org.ffmpeg.api.extension.probe.output.OutputFormatParserRegistry;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.core.validator.PathValidator;
import org.ffmpeg.extension.probe.FFprobeImpl;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaParser;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;
import org.ffmpeg.extension.probe.output.DefaultOutputFormatParserRegistry;
import org.ffmpeg.extension.probe.output.JsonOutputFormatParser;
import org.ffmpeg.extension.probe.output.XmlOutputFormatParser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

/**
 * FFprobe configuration support (template). <br><br>
 *
 * Helper template when creating a probe configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see BootstrapConfigurer
 */
public interface FFprobeBootstrapConfigurer extends BootstrapConfigurer {

    /**
     * Checks if probe is loaded or not.
     *
     * @return boolean loaded (true) or not (false)
     */
    default boolean isProbeLoaded() {
        return StringUtils.isNotBlank(getProperties().getProbeExecutorPath());
    }

    /**
     * Gets probe home variable.
     *
     * @return home variable (never null)
     */
    @Nonnull
    default String getProbeHomeVariable() { return FFprobeConstants.FFPROBE_HOME; }

    /**
     * Creates probe framework.
     *
     * @param executorFactory executor factory (required)
     * @param toolkit toolkit (required)
     * @param multiMediaParser multimedia parser (required)
     * @return probe framework (never null)
     */
    @Nullable
    default FFprobe createProbeFramework(FFmpegExecutorFactory executorFactory,
                                         FFmpegToolkit toolkit,
                                         MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> multiMediaParser) {
        if(isProbeLoaded()) {
            return new FFprobeImpl(executorFactory, toolkit, multiMediaParser);
        }
        return null;
    }

    /**
     * Creates probe multimedia parser.
     *
     * @param executorFactory executor factory (required)
     * @param registry output format parser registry (required)
     * @return multimedia parser (never null)
     */
    @Nullable
    default MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> createProbeMultiMediaParser(
            @Nullable FFmpegExecutorFactory executorFactory, @Nullable OutputFormatParserRegistry<OutputFormatParser> registry) {
        if(nonNull(executorFactory) && nonNull(registry)) {
            return new ProbeMultiMediaParser(executorFactory, registry);
        }
        return null;
    }

    /**
     * Creates probe executor factory.
     *
     * @return executor factory (never null)
     */
    @Nullable
    default FFmpegExecutorFactory createProbeExecutorFactory() {
        if(isProbeLoaded()) {
            PathValidator.of().validate(Paths.get(getProperties().getProbeExecutorPath())); //VALIDATE
            return new DefaultFFmpegExecutorFactory(getProperties().isRedirectErrorStream(), Paths.get(getProperties().getProbeExecutorPath()));
        }
        return null;
    }

    /**
     * Create output format parser registry. <br><br>
     *
     * - JSON: objectmapper <br>
     * - XML: document builder factory + xsd schema <br>
     *
     * @param objectMapper object mapper (optional)
     * @param documentBuilderFactory document builder factory (optional)
     * @param xsdSchema xsd scheme (optional)
     * @return registry (never null)
     */
    @Nonnull
    default OutputFormatParserRegistry<OutputFormatParser> createOutputFormatParserRegistry(@NonNull FFmpegToolkit toolkit,
                                                                                            @Nullable ObjectMapper objectMapper,
                                                                                            @Nullable DocumentBuilderFactory documentBuilderFactory,
                                                                                            @Nullable Schema xsdSchema) {
        OutputFormatParserRegistry<OutputFormatParser> registry = new DefaultOutputFormatParserRegistry();
        if(nonNull(objectMapper)) {
            registry.register(new JsonOutputFormatParser(objectMapper, toolkit)); //JSON
        }
        if(nonNull(documentBuilderFactory) && nonNull(xsdSchema)) {
            registry.register(new XmlOutputFormatParser(getProperties().isProbeStrictXmlValidation(), documentBuilderFactory, xsdSchema, toolkit)); //XML
        }
        return registry;
    }

    /**
     * Create schema for given XSD path.
     *
     * @param xsdPath xsd path (optional)
     * @return schema (never null)
     * @throws Exception if any error occurs
     */
    default Schema createSchema(@Nullable String xsdPath) throws Exception {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        if(isNull(xsdPath)) {
            return schemaFactory.newSchema();
        }
        final InputStream resource = getClass().getResourceAsStream(xsdPath);
        if(nonNull(resource)) {
            return schemaFactory.newSchema(new StreamSource(new InputStreamReader(resource)));
        }
        return null;
    }

    /**
     * Creates document builder factory for given schema. <br><br>
     *
     * Bean required for output format XML ! <br>
     *
     * @param schema xsd schema (optional)
     * @return document builder factory or null
     */
    default DocumentBuilderFactory createDocumentBuilderFactory(@Nullable Schema schema) {
        if(nonNull(schema)) {
            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setCoalescing(false);
            documentBuilderFactory.setExpandEntityReferences(true);
            documentBuilderFactory.setIgnoringComments(false);
            documentBuilderFactory.setIgnoringElementContentWhitespace(false);
            documentBuilderFactory.setNamespaceAware(true);
            documentBuilderFactory.setSchema(schema);
            documentBuilderFactory.setValidating(false);
            documentBuilderFactory.setXIncludeAware(false);
            return documentBuilderFactory;
        }
        return null;
    }

    /**
     * Creates object mapper. <br><br>
     *
     * Bean required for output format JSON ! <br>
     *
     * @return object mapper (never null)
     */
    @Nonnull
    default ObjectMapper createObjectMapper() { return new ObjectMapper(); }
}