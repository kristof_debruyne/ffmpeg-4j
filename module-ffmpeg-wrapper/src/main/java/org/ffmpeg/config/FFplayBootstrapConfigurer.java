package org.ffmpeg.config;

import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.api.extension.play.common.FFplayConstants;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.core.validator.PathValidator;
import org.ffmpeg.extension.play.FFplayImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Paths;

/**
 * FFplay configuration support (template). <br><br>
 *
 * Helper template when creating a play configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see BootstrapConfigurer
 */
public interface FFplayBootstrapConfigurer extends BootstrapConfigurer {

    /**
     * Checks if play is loaded or not.
     *
     * @return boolean loaded (true) or not (false)
     */
    default boolean isPlayLoaded() {
        return StringUtils.isNotBlank(getProperties().getPlayExecutorPath());
    }

    /**
     * Gets play home variable.
     *
     * @return home variable (never null)
     */
    @Nonnull
    default String getPlayHomeVariable() { return FFplayConstants.FFPLAY_HOME; }

    /**
     * Creates play framework.
     *
     * @param executorFactory executor factory (required)
     * @param toolkit toolkit (required)
     * @return probe framework (never null)
     */
    @Nullable
    default FFplay createPlayFramework(FFmpegExecutorFactory executorFactory, FFmpegToolkit toolkit) {
        if(isPlayLoaded()) {
            return new FFplayImpl(executorFactory, toolkit);
        }
        return null;
    }

    /**
     * Creates play executor factory.
     *
     * @return executor factory (never null)
     */
    @Nullable
    default FFmpegExecutorFactory createPlayExecutorFactory() {
        if(isPlayLoaded()) {
            PathValidator.of().validate(Paths.get(getProperties().getPlayExecutorPath())); //VALIDATE
            return new DefaultFFmpegExecutorFactory(getProperties().isRedirectErrorStream(), Paths.get(getProperties().getPlayExecutorPath()));
        }
        return null;
    }
}