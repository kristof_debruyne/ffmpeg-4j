package org.ffmpeg.config;

import org.ffmpeg.api.config.FFmpegProperties;

import javax.annotation.Nonnull;

/**
 * Configurer interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProperties
 */
public interface BootstrapConfigurer {

    /**
     * Properties prefix.
     *
     * E.g. ffmpeg.executorPath (prefix is ffmpeg)
     */
    String FFMPEG_PROPERTY_PREFIX = "ffmpeg";

    /**
     * Gets properties.
     *
     * @return properties (never null)
     */
    @Nonnull
    FFmpegProperties getProperties();
}
