package org.ffmpeg.rx.context;

import io.reactivex.rxjava3.core.Scheduler;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.rx.api.context.ReactiveFFmpegContext;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueManager;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Configurable reactive FFMPEG context.
 *
 * @author Sikke303
 * @since 1.0
 * @see ReactiveFFmpegContext
 */
@RequiredArgsConstructor
@Getter
@ParametersAreNonnullByDefault
@Slf4j
public class ReactiveFFmpegContextImpl implements ReactiveFFmpegContext {

    @NonNull
    private final FFmpegContext context;

    @NonNull
    private final ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor;

    @NonNull
    private final ReactiveFFmpegQueueManager<DefaultFFmpegProcessRequest> queueManager;

    @NonNull
    private final Scheduler scheduler;
}
