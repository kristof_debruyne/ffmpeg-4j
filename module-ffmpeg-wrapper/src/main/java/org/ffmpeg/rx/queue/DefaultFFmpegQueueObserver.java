package org.ffmpeg.rx.queue;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueObserver;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.ffmpeg.api.core.process.FFmpegProcessStatus.ERROR;

/**
 * Default reactive FFmpeg queue observer. <br>
 *
 * @author Sikke303
 * @since 2.0
 * @see ReactiveFFmpegQueueObserver
 */
@Slf4j
public class DefaultFFmpegQueueObserver implements ReactiveFFmpegQueueObserver<List<DefaultFFmpegProcessRequest>> {

    private static final AtomicBoolean PROCESSING = new AtomicBoolean(false);
    private static final CompositeDisposable COMPOSITE_DISPOSABLE = new CompositeDisposable();

    @NonNull
    private final ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor;

    @NonNull
    private final Scheduler scheduler;

    public DefaultFFmpegQueueObserver(@NonNull ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> processor,
                                      @NonNull Scheduler scheduler) {
        this.processor = processor;
        this.scheduler = scheduler;
    }

    @Override
    public boolean isIdle() { return !isProcessing(); }

    @Override
    public boolean isProcessing() { return PROCESSING.get(); }

    @Override
    public void onSubscribe(@NonNull Disposable subscription) { COMPOSITE_DISPOSABLE.add(subscription); }

    @Override
    public void onNext(@NonNull List<DefaultFFmpegProcessRequest> requests) {
        log.trace("Processing {} requests from queue.", requests.size());
        if(requests.isEmpty()) {
            return;
        }
        try {
            if(PROCESSING.compareAndSet(false, true)) {
                Disposable subscription = processor.process(() -> Flowable.fromIterable(requests), scheduler)
                        .onErrorReturnItem(DefaultFFmpegProcessResponse.builder().status(ERROR).build())
                        .doOnTerminate(() -> PROCESSING.set(false))
                        .subscribe((DefaultFFmpegProcessResponse response) -> {

                        });
                COMPOSITE_DISPOSABLE.add(subscription);
            }
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
        }
    }

    @Override
    public void onError(@NonNull Throwable error) {
        log.error("Queue observer crashed ! Error: {}", error.getMessage());
    }

    @Override
    public void onComplete() {
        log.trace("Queue observer has been completed ! Cleaning up ...");
    }

    @Override
    public void close() { COMPOSITE_DISPOSABLE.clear(); }
}
