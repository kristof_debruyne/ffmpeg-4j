package org.ffmpeg.rx.queue;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueManager;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueObserver;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

/**
 * Default reactive FFmpeg queue observer. <br>
 *
 * @author Sikke303
 * @since 2.0
 * @see ReactiveFFmpegQueueObserver
 */
@Slf4j
public class BufferedFFmpegQueueManager implements ReactiveFFmpegQueueManager<DefaultFFmpegProcessRequest> {

    private static final Subject<DefaultFFmpegProcessRequest> QUEUE = PublishSubject.create();

    /**
     * Constructor
     * @param observer observer (required)
     * @param scheduler scheduler (required)
     * @param bufferTimeInSeconds buffer time in seconds
     */
    public BufferedFFmpegQueueManager(@Nonnull ReactiveFFmpegQueueObserver<List<DefaultFFmpegProcessRequest>> observer,
                                      @NonNull Scheduler scheduler, long bufferTimeInSeconds) {
        /*QUEUE.observeOn(scheduler)
                .buffer(bufferTimeInSeconds, TimeUnit.SECONDS)
                .subscribe(observer);*/
    }

    @Override
    public void queue(@Nonnull final DefaultFFmpegProcessRequest request) {
        if (QUEUE.hasObservers()) {
            log.trace("Queued request for processing ! UUID: {}", request.getUuid());
            QUEUE.onNext(request);
        }
    }

    @Override
    public void queue(@Nonnull final Collection<DefaultFFmpegProcessRequest> requests) { requests.forEach(this::queue); }

    @Override
    public void close() { QUEUE.onComplete(); }
}