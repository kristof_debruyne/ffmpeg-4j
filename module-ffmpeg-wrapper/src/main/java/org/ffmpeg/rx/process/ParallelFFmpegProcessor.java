package org.ffmpeg.rx.process;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.reactivestreams.Subscription;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Supplier;

/**
 * Parallel FFmpeg processor.
 *
 * @author Sikke303
 * @since 2.0
 * @see ReactiveFFmpegProcessor
 */
@ParametersAreNonnullByDefault
@Slf4j
public class ParallelFFmpegProcessor extends AbstractReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>
        implements ReactiveFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> {

    public ParallelFFmpegProcessor(@NonNull FFmpegContext context) { super(context); }

    @Tracing
    @Nonnull
    @Override
    public Flowable<DefaultFFmpegProcessResponse> process(@NonNull Supplier<Flowable<DefaultFFmpegProcessRequest>> supplier, 
                                                          @NonNull Scheduler scheduler, int parallelism) {
        return supplier.get()
                .doOnSubscribe((Subscription subscription) -> validateParallelism(parallelism))
                .parallel(parallelism)
                .runOn(scheduler)
                .map((DefaultFFmpegProcessRequest request) -> resolveCodecType(request).andThen(execute(request)).apply(null))
                .sequential();
    }
}