package org.ffmpeg.rx.process;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Function;

import static org.ffmpeg.api.core.common.FFmpegConstant.PRIMARY_STREAM_INDEX;

/**
 * Abstract reactive FFmpeg processor. <br>
 *
 * @author Sikke303
 * @since 2.0
 * @see ReactiveFFmpegProcessor
 */
@RequiredArgsConstructor
public abstract class AbstractReactiveFFmpegProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse>
        implements ReactiveFFmpegProcessor<Request, Response> {

    @Getter
    @NonNull
    private final FFmpegContext context;

    /**
     * Validates parallelism parameter.
     *
     * @param parallelism parallelism count
     * @throws IllegalArgumentException if parallelism is invalid
     */
    protected final void validateParallelism(final int parallelism) {
        Validate.isTrue(parallelism >= 1, "Parallelism must be greater than or equal than 1 !");
        Validate.isTrue(parallelism <=  getAvailableProcessors(), "Parallelism can't be greater than the available processors !");
    }

    /**
     * Resolve codec type for given request.
     *
     * @param request request (required)
     * @return function that resolves codec type for given request
     */
    @Nonnull
    protected final Function<Void, CodecType> resolveCodecType(@NonNull final FFmpegProcessRequest request) {
        return nothing -> context.getPlatform().getMultiMediaInformation(request.getSource())
                .filter(DefaultMultiMediaResponse.class::isInstance)
                .map(DefaultMultiMediaResponse.class::cast)
                .orElseThrow(() -> new FFmpegExecutionException("No codec type detected for given process request !"))
                .getBody()
                .findCodecTypeByIndex(PRIMARY_STREAM_INDEX);
    }

    /**
     * Executes request for incoming codec type.
     *
     * @param request request (required)
     * @return function that converts request for incoming codec type (never null)
     */
    @NonNull
    protected final Function<CodecType, DefaultFFmpegProcessResponse> execute(@NonNull FFmpegProcessRequest request) {
        return (CodecType codecType) -> {
            Optional<?> response;
            switch (codecType) {
                case AUDIO: response = context.getAudioProcessor().flatMap(processor -> processor.process(request)); break;
                case SUBTITLE: response = context.getSubtitleProcessor().flatMap(processor -> processor.process(request)); break;
                case VIDEO: response = context.getVideoProcessor().flatMap(processor -> processor.process(request)); break;
                default: response = Optional.empty();
            }
            return response
                    .filter(DefaultFFmpegProcessResponse.class::isInstance)
                    .map(DefaultFFmpegProcessResponse.class::cast)
                    .orElseThrow(() -> new FFmpegExecutionException("Failed to process resource: " + request.getSource().toAbsolutePath().toString()));
        };
    }
}
