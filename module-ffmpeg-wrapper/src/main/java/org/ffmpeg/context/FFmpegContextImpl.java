package org.ffmpeg.context;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.ffmpeg.api.audio.processor.AudioProcessor;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.common.About;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.dsl.RawFFmpegExecutorBuilder;
import org.ffmpeg.api.subtitle.processor.SubtitleProcessor;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.ffmpeg.core.AbstractFFmpegSupport;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorResponse;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;
import org.ffmpeg.core.process.DefaultFFmpegProcessor;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

/**
 * Configurable FFMPEG context.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegContext
 */
@ParametersAreNonnullByDefault
@Slf4j
public class FFmpegContextImpl extends AbstractFFmpegSupport implements FFmpegContext {

    @Getter
    private final FFmpegPlatform platform;

    @Getter
    private final FFmpegProcessorFactory processorFactory;

    @Builder
    private FFmpegContextImpl(@NonNull FFmpegPlatform platform,
                              @NonNull FFmpegExecutorFactory executorFactory,
                              @NonNull FFmpegProcessorFactory processorFactory) {
        super(executorFactory);
        this.platform = platform;
        this.processorFactory = processorFactory;
    }

    @Nonnull
    @Override
    public About getAboutInformation() { return About.of(); }

    @Tracing
    @Nonnull
    @Override
    public DefaultFFmpegExecutorResponse run(@NonNull final String ...commands) {
        log.info("Executing raw commands: {}", ArrayUtils.toString(commands));
        return (DefaultFFmpegExecutorResponse) execute(newExecutorBuilder(RawFFmpegExecutorBuilder.class).withCommands(commands));
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    @Override
    public Optional<DefaultFFmpegProcessor> getDefaultProcessor() {
        return Optional.of(getProcessorFactory().newInstance(DefaultFFmpegProcessor.class));
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    @Override
    public Optional<AudioProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>> getAudioProcessor() {
        return Optional.of(getProcessorFactory().newInstance(AudioProcessor.class));
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    @Override
    public Optional<SubtitleProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>> getSubtitleProcessor() {
        return Optional.of(getProcessorFactory().newInstance(SubtitleProcessor.class));
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    @Override
    public Optional<VideoProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>> getVideoProcessor() {
        return Optional.of(getProcessorFactory().newInstance(VideoProcessor.class));
    }
}
