package org.ffmpeg.video;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.io.Serializable;

/**
 * Video size container. <br>
 * 
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class VideoSize implements Serializable {

	private static final long serialVersionUID = 0L;

	private final int width;
	private final int height;

	@Nonnull
	public String asString() {
		return String.format("%dx%d", width, height);
	}
}