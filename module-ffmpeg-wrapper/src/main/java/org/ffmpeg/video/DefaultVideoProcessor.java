package org.ffmpeg.video;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.codec.Codec;
import org.ffmpeg.api.core.codec.coding.decoder.Decoder;
import org.ffmpeg.api.core.codec.coding.encoder.Encoder;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.video.processor.VideoProcessor;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.core.process.AbstractFFmpegProcessor;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.core.process.DefaultFFmpegProcessResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.isNull;
import static org.ffmpeg.api.core.codec.CodecType.VIDEO;
import static org.ffmpeg.api.core.options.FFmpegOptionLocation.OUTPUT;
import static org.ffmpeg.api.core.options.MainOption.CODEC;
import static org.ffmpeg.api.core.options.MainOption.DURATION;
import static org.ffmpeg.api.core.options.MainOption.FORCE_FORMAT;
import static org.ffmpeg.api.core.options.MainOption.SEEK_POSITION;
import static org.ffmpeg.api.video.option.VideoOption.FRAME_RATE;
import static org.ffmpeg.api.video.option.VideoOption.FRAME_SIZE;

/**
 * Default video processor. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see VideoProcessAttributes
 */
@ParametersAreNonnullByDefault
@Slf4j
public class DefaultVideoProcessor extends AbstractFFmpegProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse>
		implements VideoProcessor<DefaultFFmpegProcessRequest, DefaultFFmpegProcessResponse> {

	public DefaultVideoProcessor(FFmpegPlatform platform) { super(platform); }

	@Tracing
	@Override
	public boolean isVideoCodec(@NonNull final String name) {
		return getVideoCodecs().stream().anyMatch(codec -> codec.getName().equals(name));
	}

	@Tracing
	@Override
	public boolean isVideoDecoder(@NonNull final String name) {
		return getVideoDecoders().stream().anyMatch(decoder -> decoder.getCodec().equals(name));
	}

	@Tracing
	@Override
	public boolean isVideoEncoder(@NonNull final String name) {
		return getVideoEncoders().stream().anyMatch(encoder -> encoder.getCodec().equals(name));
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Codec> getVideoCodecs() {
		return getPlatform().getFramework().getCodecs(VIDEO);
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Decoder> getVideoDecoders() {
		return getPlatform().getFramework().getDecoders(VIDEO);
	}

	@Tracing
	@Nonnull
	@Override
	public Set<Encoder> getVideoEncoders() {
		return getPlatform().getFramework().getEncoders(VIDEO);
	}

	@Tracing
	@Nonnull
	@Override
	public Optional<DefaultFFmpegProcessResponse> process(@NonNull final DefaultFFmpegProcessRequest request) {
		return Optional.of(request.getAttributes())
				.filter(VideoProcessAttributes.class::isInstance)
				.map(VideoProcessAttributes.class::cast)
				.filter(validateAttributes())
				.flatMap(attributes -> parseRequest()
						.andThen(configure(request))
						.andThen(execute(request))
						.apply(request)
				);
	}

	//*** HELPER FUNCTIONS ***//

	@Nonnull
	protected Function<ComplexFFmpegExecutorBuilder, FFmpegExecutorBuilder> configure(@NonNull DefaultFFmpegProcessRequest request) {
		return (ComplexFFmpegExecutorBuilder builder) -> {
			final VideoProcessAttributes attributes = (VideoProcessAttributes) request.getAttributes();
			builder.registerOption(attributes.hasOffset(), SEEK_POSITION, attributes.getOffset(), OUTPUT);
			builder.registerOption(attributes.hasDuration(), DURATION, attributes.getDuration(), OUTPUT);
			builder.registerOption(attributes.hasFormat(), FORCE_FORMAT, attributes.getFormat(), OUTPUT);
			final VideoAttributes videoAttributes = attributes.getVideoAttributes();
			builder.registerOption(videoAttributes.hasCodec(), CODEC, videoAttributes.getCodec(), OUTPUT);
			builder.registerOption(videoAttributes.hasFrameRate(), FRAME_RATE, videoAttributes.getFrameRate());
			if(videoAttributes.hasFrameSize()) {
				builder.registerOption(FRAME_SIZE, videoAttributes.getFrameSize().asString());
			}
			return builder.withOutputPath(request.getTarget().toAbsolutePath().toString());
		};
	}

	@SuppressWarnings("unchecked")
	@Nonnull
	protected Predicate<VideoProcessAttributes> validateAttributes() {
		return (VideoProcessAttributes attributes) -> {
			if(isNull(attributes.getVideoAttributes())) {
				throw new IllegalArgumentException("Illegal argument detected ! Video attributes can't be null ...");
			}
			return true;
		};
	}
}
