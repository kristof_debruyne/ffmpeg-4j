package org.ffmpeg.video;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.ffmpeg.api.core.process.FFmpegProcessAttributes;

import static java.util.Objects.nonNull;

/**
 * Video process attributes. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see FFmpegProcessAttributes
 * @see VideoAttributes
 */
@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class VideoProcessAttributes implements FFmpegProcessAttributes {

	private static final long serialVersionUID = 0L;

	private final Float duration;
	private final Float offset;
	private final String format;
	private final VideoAttributes videoAttributes;

	public boolean hasDuration() {
		return nonNull(duration) && duration > 0;
	}

	public boolean hasOffset() {
		return nonNull(offset) && offset > -1;
	}

	public boolean hasFormat() {
		return StringUtils.isNotBlank(format);
	}
}
