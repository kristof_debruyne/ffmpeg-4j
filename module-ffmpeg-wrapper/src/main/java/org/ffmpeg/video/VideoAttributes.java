package org.ffmpeg.video;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import static java.util.Objects.nonNull;

/**
 * Video attributes container. <br>
 * 
 * @author Sikke303
 * @since 1.0
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class VideoAttributes implements Serializable {

	private static final long serialVersionUID = 0L;

	private final String codec;
	private final Integer bitRate;
	private final Integer frameRate;
	private final VideoSize frameSize;
	
	public final boolean hasBitRate() { return nonNull(bitRate); }

	public final boolean hasCodec() { return StringUtils.isNotBlank(codec); }

	public final boolean hasFrameRate() { return nonNull(frameRate); }

	public final boolean hasFrameSize() { return nonNull(frameSize); }
}