package org.ffmpeg.video;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.ffmpeg.api.core.FFmpegObject;
import org.ffmpeg.api.core.common.Stream;

/**
 * Video stream container.
 *
 * @author Sikke303
 * @since 1.0
 * @see FFmpegObject
 */
@SuperBuilder
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VideoStream extends Stream {

    private final float frameRate;
    private final VideoSize size;
}
