package org.ffmpeg.multimedia;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.ffmpeg.api.multimedia.MultiMediaRequest;

import java.nio.file.Path;

/**
 * Default multimedia request.
 *
 * @author Sikke303
 * @since 1.0
 * @see MultiMediaRequest
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@Getter
@ToString
public class DefaultMultiMediaRequest implements MultiMediaRequest {

    @NonNull
    private final Path resource;
}
