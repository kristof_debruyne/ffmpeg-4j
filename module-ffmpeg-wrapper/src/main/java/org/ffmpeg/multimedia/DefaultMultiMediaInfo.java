package org.ffmpeg.multimedia;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import org.apache.commons.lang3.Validate;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.common.Stream;
import org.ffmpeg.api.exception.FFmpegExecutionException;
import org.ffmpeg.api.multimedia.MultiMediaInfo;

import java.util.Set;

import static java.util.Optional.ofNullable;
import static org.ffmpeg.api.core.common.FFmpegConstant.NO_VALUE_SET;

/**
 * Default multimedia info.
 *
 * @author Sikke303
 * @since 1.0
 * @see MultiMediaInfo
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@ToString
public class DefaultMultiMediaInfo implements MultiMediaInfo {

    private final int bitRate;
    private final long duration;
    private final String format;

    @Singular("stream")
    private final Set<Stream> streams;

    @NonNull
    @Override
    public CodecType findCodecTypeByIndex(final int index) {
        Validate.isTrue(index > NO_VALUE_SET, "Index must be greater than or equal than zero !");
        return ofNullable(streams)
                .filter(items -> !items.isEmpty())
                .flatMap(items -> items.stream().filter(item -> item.getIndex() == index).findAny())
                .map(Stream::getCodecType)
                .orElseThrow(() -> new FFmpegExecutionException("No stream with index " + index + " found !"));
    }
}
