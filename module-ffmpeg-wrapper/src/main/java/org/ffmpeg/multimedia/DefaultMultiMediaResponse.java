package org.ffmpeg.multimedia;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.multimedia.MultiMediaInfo;

/**
 * Default multimedia response (processed).
 * 
 * @author Sikke303
 * @since 1.0
 * @see ProbeMultiMediaResponse
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Getter
@ToString
public class DefaultMultiMediaResponse implements ProbeMultiMediaResponse {

	@NonNull
	private final MultiMediaInfo body;
}
