package org.ffmpeg.multimedia;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.ffmpeg.api.core.codec.CodecType;
import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.audio.AudioStream;
import org.ffmpeg.core.AbstractFFmpegSupport;
import org.ffmpeg.core.annotation.Tracing;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaParser;
import org.ffmpeg.video.VideoSize;
import org.ffmpeg.video.VideoStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static org.ffmpeg.api.core.common.FFmpegConstant.MONO;
import static org.ffmpeg.api.core.common.FFmpegConstant.STEREO;
import static org.ffmpeg.api.core.io.FFmpegFileSystem.createTempFile;
import static org.ffmpeg.api.core.io.FFmpegFileSystem.deleteFile;
import static org.ffmpeg.api.core.io.FFmpegFileSystem.getFileExtension;
import static org.ffmpeg.api.core.options.MainOption.INPUT_SOURCE;
import static org.ffmpeg.api.core.options.MainOption.OVERWRITE_OUTPUT_FILES;
import static org.ffmpeg.api.core.options.SharedOptions.HIDE_BANNER;

/**
 * Default multimedia parser. <br><br>
 *
 * This is the default multimedia parser. Be aware that if a file contains multiple streams, <br>
 * it will only process the primary stream. Every secondary stream will not be parsed. <br><br>
 *
 * For instance: <br>
 * - audio + images: only the audio stream will be parsed <br>
 * - video + images: only the video stream will be parsed <br><br>
 *
 * The primary stream always has index 0. <br><br>
 *
 * If you intend to get all the information of all the streams, please use a more advanced multimedia <br>
 * parser like the {@link ProbeMultiMediaParser} class. <br>
 * 
 * @author Sikke303
 * @since 1.0
 * @see DefaultMultiMediaRequest
 * @see DefaultMultiMediaResponse
 * @see ProbeMultiMediaParser
 */
@ParametersAreNonnullByDefault
@Slf4j
public class DefaultMultiMediaParser extends AbstractFFmpegSupport implements MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> {

	private static final Pattern MULTI_MEDIA_DURATION_PATTERN = compile("^.*Duration: (\\d\\d:\\d\\d:\\d\\d\\.\\d{1,2}).*bitrate: (\\d*) kb/s$", CASE_INSENSITIVE);
	private static final Pattern MULTI_MEDIA_INPUT_PATTERN = compile("^.*Input #0, (\\w+).+$\\s*$", CASE_INSENSITIVE);
	private static final Pattern MULTI_MEDIA_AUDIO_STREAM_PATTERN = compile("^.*Stream #\\S+: Audio: (.*?), (.*?) Hz, (mono|stereo), (.*?), (\\d*) kb/s$", CASE_INSENSITIVE);
	private static final Pattern MULTI_MEDIA_VIDEO_STREAM_PATTERN = compile("^.*Stream #\\S+: Video: (.*?), (.*?), (\\d*x\\d*), (.*?), (\\d*) kb/s, (\\d*) fps,.*$", CASE_INSENSITIVE);

	private final FFmpegToolkit toolkit;

	public DefaultMultiMediaParser(@NonNull FFmpegExecutorFactory executorFactory, @NonNull FFmpegToolkit toolkit) {
		super(executorFactory);
		this.toolkit = toolkit;
	}

	@Tracing
	@Nonnull
	@Override
	public Optional<DefaultMultiMediaResponse> parse(@NonNull final DefaultMultiMediaRequest request) {
		String resource = request.getResource().toAbsolutePath().toString();
		log.trace("Extracting multi media information from resource: {}", resource);

		Path tempFile = createTempFile("multimedia_", getFileExtension(request.getResource(), true));
		FFmpegExecutor ffmpegExecutor = newExecutorBuilder(ComplexFFmpegExecutorBuilder.class)
				.registerOption(HIDE_BANNER)
				.registerOption(INPUT_SOURCE, resource)
				.registerOption(OVERWRITE_OUTPUT_FILES)
				.withOutputPath(tempFile.toAbsolutePath().toString())
				.build();

		DefaultMultiMediaInfo.DefaultMultiMediaInfoBuilder multiMediaBuilder = DefaultMultiMediaInfo.builder();

		try (FFmpegBufferedReader reader = (FFmpegBufferedReader) ffmpegExecutor.execute()) {
			int step = 1;
			while (true) {
				final String line = StringUtils.trimToNull(reader.readLine());
				if (line == null) {
					break;
				}
				log.trace("{} ", line);
				if (step == 1) {
					String token = resource + ": ";
					if (line.startsWith(token)) {
						throw new FFmpegValidationException(line.substring(token.length()));
					}
					Matcher m = MULTI_MEDIA_INPUT_PATTERN.matcher(line);
					if (m.matches()) {
						log.trace("Retrieving [ FORMAT ] attribute ! - (Step {} of 3) ", step);
						multiMediaBuilder.format(m.group(1));
						log.trace("Multimedia attribute [ FORMAT ] mapped with value [ {} ] ...", m.group(1));
						step++;
					}
				} else if (step == 2) {
					Matcher m = MULTI_MEDIA_DURATION_PATTERN.matcher(line);
					if (m.matches()) {
						log.trace("Retrieving [ DURATION ] attribute ! - (Step {} of 3) ", step);
						long duration = toolkit.convertDuration(m.group(1));
						multiMediaBuilder.duration(duration);
						log.trace("Multimedia attribute [ DURATION ] mapped with value [ {} ] ...", duration);
						step++;
					}
				} else if (step == 3) {
					Matcher audioStreamMatcher = MULTI_MEDIA_AUDIO_STREAM_PATTERN.matcher(line);
					Matcher videoStreamMatcher = MULTI_MEDIA_VIDEO_STREAM_PATTERN.matcher(line);
					if (audioStreamMatcher.matches()) {
						log.trace("Retrieving [ AUDIO ] information ! - (Step {} of 3) ", step);
						Pair<Integer, AudioStream> audioInfo = parseAudioInfo(audioStreamMatcher);
						multiMediaBuilder.bitRate(audioInfo.getKey());
						multiMediaBuilder.stream(audioInfo.getValue());
						log.trace("Audio information successfully parsed from multimedia parser ...");
						step++;
					} else if(videoStreamMatcher.matches()) {
						log.trace("Retrieving [ VIDEO ] information ! - (Step {} of 3) ", step);
						Pair<Integer, VideoStream> videoInfo = parseVideoInfo(videoStreamMatcher);
						multiMediaBuilder.bitRate(videoInfo.getKey());
						multiMediaBuilder.stream(videoInfo.getValue());
						log.trace("Video information successfully parsed from multimedia parser ...");
						step++;
					}
				}
				
				if (step == 4) {
					reader.reloadLine(line);
					break;
				}
			}
		} catch (Exception ex) {
			log.error("Failed to parse multimedia information !", ex);
			return Optional.empty();
		} finally {
			deleteFile(tempFile);
		}
		return Optional.of(DefaultMultiMediaResponse.builder().body(multiMediaBuilder.build()).build());
	}

	@Nonnull
	private Pair<Integer, AudioStream> parseAudioInfo(@NonNull final Matcher matcher) {
		String decoder = matcher.group(1);
		log.trace("Multimedia attribute [ DECODER ] mapped with value [ {} ] ...", decoder);

		int bitRate = Integer.parseInt(matcher.group(5));
		log.trace("Multimedia attribute [ BITRATE ] mapped with value [ {} ] ...", bitRate);

		String channel = matcher.group(3);
		int channels = channel.equalsIgnoreCase(MONO) ? 1 : (channel.equalsIgnoreCase(STEREO) ? 2 : 0);
		log.trace("Multimedia attribute [ CHANNELS ] mapped with value [ {} ] ...", channels);

		int sampleRate = Integer.parseInt(matcher.group(2));
		log.trace("Multimedia attribute [ SAMPLE_RATE ] mapped with value [ {} ] ...", sampleRate);

		return Pair.of(bitRate, AudioStream.builder()
				.codecName(decoder).codecType(CodecType.AUDIO).channels(channels).sampleRate(sampleRate).build()
		);
	}

	@Nonnull
	private Pair<Integer, VideoStream> parseVideoInfo(@NonNull final Matcher matcher) {
		String decoder = matcher.group(1);
		log.trace("Multimedia attribute [ DECODER ] mapped with value [ {} ] ...", decoder);

		int bitRate = Integer.parseInt(matcher.group(5));
		log.trace("Multimedia attribute [ BITRATE ] mapped with value [ {} ] ...", bitRate);

		float frameRate = Float.parseFloat(matcher.group(6));
		log.trace("Multimedia video attribute [ FRAME_RATE ] mapped with value [ {} ] ...", frameRate);

		String[] resolution = matcher.group(3).split("x");
		VideoSize videoSize = new VideoSize(Integer.parseInt(resolution[0]), Integer.parseInt(resolution[1]));
		log.trace("Multimedia video attribute [ SIZE ] mapped with value [ {} ] ...", videoSize);

		return Pair.of(bitRate, VideoStream.builder()
				.codecName(decoder).codecType(CodecType.VIDEO).frameRate(frameRate).size(videoSize).build()
		);
	}
}