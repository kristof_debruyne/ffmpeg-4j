package org.ffmpeg.core.validator;

import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;

public class BitDepthValidatorUnitTest extends AbstractFFmpegTest {

    @InjectMocks
    private final BitDepthValidator validator = new BitDepthValidator();

    @Test(expected = NullPointerException.class)
    public void givenNullBitDepth_whenValidating_thenReturnError() {
        validator.validate(null);
    }

    @Test
    public void givenValidBitDepth_whenValidating_thenReturnSilently() {
        validator.validate(8);
        validator.validate(16);
        validator.validate(24);
    }

    @Test(expected = FFmpegValidationException.class)
    public void givenInvalidBitDepth_whenValidating_thenReturnError() {
        validator.validate(0);
    }
}