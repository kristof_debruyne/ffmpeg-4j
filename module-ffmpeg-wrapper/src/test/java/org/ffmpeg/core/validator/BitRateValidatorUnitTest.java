package org.ffmpeg.core.validator;

import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;

public class BitRateValidatorUnitTest extends AbstractFFmpegTest {

    @InjectMocks
    private final BitRateValidator validator = new BitRateValidator();

    @Test(expected = NullPointerException.class)
    public void givenNullBitRate_whenValidating_thenReturnError() {
        validator.validate(null);
    }

    @Test
    public void givenValidBitRate_whenValidating_thenReturnSilently() {
        validator.validate(96);
        validator.validate(128);
        validator.validate(160);
        validator.validate(256);
        validator.validate(320);
    }

    @Test(expected = FFmpegValidationException.class)
    public void givenInvalidBitRate_whenValidating_thenReturnError() {
        validator.validate(666);
    }
}