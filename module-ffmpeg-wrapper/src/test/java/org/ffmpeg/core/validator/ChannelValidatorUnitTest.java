package org.ffmpeg.core.validator;

import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;

public class ChannelValidatorUnitTest extends AbstractFFmpegTest {

    @InjectMocks
    private final ChannelValidator validator = new ChannelValidator();

    @Test(expected = NullPointerException.class)
    public void givenNullChannel_whenValidating_thenReturnError() {
        validator.validate(null);
    }

    @Test
    public void givenValidChannel_whenValidating_thenReturnSilently() {
        validator.validate(1);
        validator.validate(2);
    }

    @Test(expected = FFmpegValidationException.class)
    public void givenInvalidChannel_whenValidating_thenReturnError() {
        validator.validate(-1);
    }
}