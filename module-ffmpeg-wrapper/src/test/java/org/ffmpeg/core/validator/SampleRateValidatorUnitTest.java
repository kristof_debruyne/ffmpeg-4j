package org.ffmpeg.core.validator;

import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;

public class SampleRateValidatorUnitTest extends AbstractFFmpegTest {

    @InjectMocks
    private final SampleRateValidator validator = new SampleRateValidator();

    @Test(expected = NullPointerException.class)
    public void givenNullSampleRate_whenValidating_thenReturnError() {
        validator.validate(null);
    }

    @Test
    public void givenValidSampleRate_whenValidating_thenReturnSilently() {
        validator.validate(11025);
        validator.validate(22050);
        validator.validate(32000);
        validator.validate(44100);
        validator.validate(48000);
    }

    @Test(expected = FFmpegValidationException.class)
    public void givenInvalidSampleRate_whenValidating_thenReturnError() {
        validator.validate(666);
    }
}