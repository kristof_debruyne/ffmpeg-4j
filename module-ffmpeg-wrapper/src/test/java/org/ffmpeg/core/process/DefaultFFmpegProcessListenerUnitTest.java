package org.ffmpeg.core.process;

import org.ffmpeg.api.core.process.FFmpegProcessCallback;
import org.ffmpeg.api.exception.FFmpegCallbackException;
import org.ffmpeg.core.converter.DurationConverter;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.common.FFmpegConstant.NO_VALUE_SET;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultFFmpegProcessListenerUnitTest extends AbstractFFmpegTest {

    private static final String PROCESS_100_PERCENT =
            "[info ]frame=    1 fps=0.4 q=-0.0 Lsize=    2200kB time=00:00:39.86 bitrate= 452.0kbits/s speed=16.9x";

    private static final String PROCESS_50_PERCENT =
            "[info ]frame=    1 fps=0.4 q=-0.0 Lsize=    2200kB time=00:00:19.93 bitrate= 452.0kbits/s speed=16.9x";

    private static final String PROCESS_DONE =
            "[info] video:639kB audio:1559kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.054814%";

    @InjectMocks
    private DefaultFFmpegProcessListener processListener;

    @Spy
    private final DurationConverter durationConverter = new DurationConverter();

    @Spy
    private final FFmpegProcessCallback callback = new DefaultFFmpegProcessCallback();

    //*** ON CHANGE ***//

    @Test
    public void givenCallbackAndProcssingLine_whenProcessChanged_thenReturnFalse() {
        assertThat(processListener.onProcessLine(callback,39880L).test(PROCESS_100_PERCENT)).isFalse();
        verify(callback, times(1)).update(eq(100L), eq("Processing ... (100 / 100)"));
    }

    @Test
    public void givenCallbackAndProcssingLineWithZeroDuration_whenProcessChanged_thenReturnFalse() {
        assertThat(processListener.onProcessLine(callback,0L).test(PROCESS_100_PERCENT)).isFalse();
        verify(callback, times(1)).update(eq(0L), eq("Processing ... (0 / 100)"));
    }

    @Test
    public void givenCallbackAndTotalProcessingLine_whenProcessChanged_thenReturnTrue() {
        assertThat(processListener.onProcessLine(callback,39880L).test(PROCESS_DONE)).isTrue();
        verify(callback, never()).update(anyInt(), anyString());
    }

    @Test(expected = FFmpegCallbackException.class)
    public void givenCallbackCancelledAndProcssingLine_whenProcessChanged_thenReturnFalse() {
        when(callback.isCancelled()).thenReturn(true);
        processListener.onProcessLine(callback,39880).test(PROCESS_100_PERCENT);
    }

    @Test
    public void givenNoCallbackAndProcssingLine_whenProcessChanged_thenReturnFalse() {
        assertThat(processListener.onProcessLine(null,39880).test(PROCESS_100_PERCENT)).isFalse();
        verify(callback, never()).update(eq(100), eq("Processing ..."));
    }

    @Test
    public void givenNoCallbackAndProcssingLineWithZeroDuration_whenProcessChanged_thenReturnFalse() {
        assertThat(processListener.onProcessLine(null,0).test(PROCESS_100_PERCENT)).isFalse();
        verify(callback, never()).update(eq(0), eq("Processing ..."));
    }

    @Test
    public void givenNoCallbackAndTotalProcessingLine_whenProcessChanged_thenReturnTrue() {
        assertThat(processListener.onProcessLine(null,39880).test(PROCESS_DONE)).isTrue();
        verify(callback, never()).update(anyInt(), anyString());
    }

    //*** CALCULATE ***//

    @Test(expected = NullPointerException.class)
    public void givenNullLine_whenCalculating_thenThrowError() {
        processListener.calculateProgress(null, 666);
    }

    @Test
    public void givenLineAndNegativeTotalDuration_whenCalculating_thenReturnNotAvailable() {
        assertThat(processListener.calculateProgress(PROCESS_100_PERCENT, -1)).isEqualTo(NO_VALUE_SET);
    }

    @Test
    public void givenLineAndZeroTotalDuration_whenCalculating_thenReturnNotAvailable() {
        assertThat(processListener.calculateProgress(PROCESS_100_PERCENT, 0)).isEqualTo(NO_VALUE_SET);
    }

    @Test
    public void givenLineAndDuration_whenCalculating_thenReturnCalculatedDuration() {
        assertThat(processListener.calculateProgress(PROCESS_50_PERCENT, 39860)).isEqualTo(50);
        assertThat(processListener.calculateProgress(PROCESS_100_PERCENT, 39860)).isEqualTo(100);
    }
}
