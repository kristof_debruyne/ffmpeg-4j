package org.ffmpeg.core.toolkit;

import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.io.FFmpegBufferedReader;
import org.ffmpeg.api.core.validator.FFmpegValidator;
import org.ffmpeg.api.exception.FFmpegValidationException;
import org.ffmpeg.core.validator.BitDepthValidator;
import org.ffmpeg.core.validator.BitRateValidator;
import org.ffmpeg.core.validator.ChannelValidator;
import org.ffmpeg.core.validator.SampleRateValidator;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultToolkitUnitTest extends AbstractFFmpegTest {

    @Mock
    private FFmpegExecutorFactory executorFactory;

    @Spy
    private final Collection<FFmpegValidator<?>> validators = new ArrayList<>();

    @InjectMocks
    private DefaultFFmpegToolkit toolkit;

    @Test(expected = NullPointerException.class)
    public void givenListOfValidators_whenGettingValidatorByNullName_thenReturnValidator(){
        toolkit.getValidator(null);
    }

    @Test
    public void givenListOfValidators_whenGettingValidatorByValidName_thenReturnValidator(){
        assertThat(toolkit.getValidator(BitRateValidator.BIT_RATE_VALIDATOR_NAME).isPresent()).isFalse();
    }

    @Test
    public void givenListOfValidators_whenGettingValidatorByInvalidName_thenReturnValidator(){
        toolkit.registerValidator(new BitRateValidator());
        assertThat(toolkit.getValidator(BitRateValidator.BIT_RATE_VALIDATOR_NAME).isPresent()).isTrue();
    }

    @Test(expected = NullPointerException.class)
    public void givenNullValidtor_whenRegisteringValidator_thenThrowError() {
        toolkit.registerValidator(null);
    }

    @Test
    public void givenUnregisteredValidtor_whenRegisteringValidator_thenThrowError() {
        //prepare
        assertThat(toolkit.getCustomValidators()).hasSize(0);

        //act
        toolkit.registerValidator(new BitRateValidator());

        //check
        assertThat(toolkit.getCustomValidators()).hasSize(1);
    }

    @Test
    public void givenAlreadyRegisteredValidtor_whenRegisteringValidator_thenThrowError() {
        //prepare
        assertThat(toolkit.getCustomValidators()).hasSize(0);
        toolkit.registerValidator(new BitRateValidator());
        assertThat(toolkit.getCustomValidators()).hasSize(1);

        //act
        toolkit.registerValidator(new BitRateValidator());

        //check
        assertThat(toolkit.getCustomValidators()).hasSize(1);
    }

    @Test(expected = FFmpegValidationException.class)
    public void givenAllocationOfZeroBytes_whenAllocating_thenThrowError() {
        toolkit.maxAllocate(0);
    }

    @Test
    public void givenAllocationOfThousandBytes_whenAllocating_thenReturnSuccessful() {
        //prepare
        FFmpegExecutor executor = mock(FFmpegExecutor.class);
        FFmpegBufferedReader reader = mock(FFmpegBufferedReader.class);
        when(executorFactory.newInstance(any())).thenReturn(executor);
        when(executor.execute()).thenReturn(reader);

        //act & check
        assertThat(toolkit.maxAllocate(1024)).isNotNull();
    }

    @Test
    public void givenValidBitDepth_whenValidatingBitRate_thenReturnSuccessful() {
        //prepare
        toolkit.registerValidator(new BitDepthValidator());

        //act
        toolkit.validateBitDepth(16);
    }

    @Test
    public void givenValidBitRate_whenValidatingBitRate_thenReturnSuccessful() {
        //prepare
        toolkit.registerValidator(new BitRateValidator());

        //act
        toolkit.validateBitRate(128);
    }

    @Test
    public void givenValidChannel_whenValidatingChannel_thenReturnSuccessful() {
        //prepare
        toolkit.registerValidator(new ChannelValidator());

        //act
        toolkit.validateChannel(1);
    }

    @Test
    public void givenValidSampleRate_whenValidatingSampleRate_thenReturnSuccessful() {
        //prepare
        toolkit.registerValidator(new SampleRateValidator());

        //act
        toolkit.validateSampleRate(48000);
    }
}