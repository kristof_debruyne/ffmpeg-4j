package org.ffmpeg.dsl;

import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.exception.FFmpegRuntimeException;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RawFFmpegExecutorBuilderUnitTest extends AbstractFFmpegTest {

    private final FFmpegExecutorFactory executorFactory = new DefaultFFmpegExecutorFactory(true, getExecutorPath());

    @Test
    public void givenCompleteBuilder_whenCreatingExecutor_thenReturnExecutor() {
        FFmpegExecutor executor = RawFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .withCommands("-version")
                .build();

        assertThat(executor).isNotNull();
        assertThat(executor.getCommands()).containsExactly("-loglevel", "level+repeat+info", "-version");
    }

    @Test(expected = NullPointerException.class)
    public void givenNoExecutorFactorySet_whenCreatingExecutor_thenThrowException() {
        RawFFmpegExecutorBuilderImpl.builder()
                .and()
                .withCommands("-version")
                .build();
    }

    @Test(expected = FFmpegRuntimeException.class)
    public void givenNoCommandsSet_whenCreatingExecutor_thenThrowException() {
        RawFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .build();
    }
}