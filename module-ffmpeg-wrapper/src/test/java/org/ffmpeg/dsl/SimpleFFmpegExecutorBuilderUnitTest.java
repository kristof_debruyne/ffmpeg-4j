package org.ffmpeg.dsl;

import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.options.GenericOption;
import org.ffmpeg.api.core.options.MainOption;
import org.ffmpeg.api.exception.FFmpegRuntimeException;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.Spy;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleFFmpegExecutorBuilderUnitTest extends AbstractFFmpegTest {

    @Spy
    private final FFmpegExecutorFactory executorFactory = new DefaultFFmpegExecutorFactory( true, getExecutorPath());

    @Test
    public void givenCompleteBuilder_whenCreatingExecutor_thenReturnExecutor() {
        FFmpegExecutor executor = SimpleFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .registerOption(GenericOption.VERSION)
                .build();

        assertThat(executor).isNotNull();
        assertThat(executor.getCommands()).containsExactly("-loglevel", "level+repeat+info", "-version");
    }

    @Test(expected = NullPointerException.class)
    public void givenNoExecutorFactorySet_whenCreatingExecutor_thenThrowException() {
        ComplexFFmpegExecutorBuilderImpl.builder()
                .and()
                .registerOption(MainOption.INPUT_SOURCE, getDefaultAudioSample().toString())
                .build();
    }

    @Test(expected = FFmpegRuntimeException.class)
    public void givenNoCommandsSet_whenCreatingExecutor_thenThrowException() {
        ComplexFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .build();
    }
}