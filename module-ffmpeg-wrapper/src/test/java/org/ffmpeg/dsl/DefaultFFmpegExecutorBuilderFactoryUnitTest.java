package org.ffmpeg.dsl;

import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.dsl.ComplexFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.FFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.RawFFmpegExecutorBuilder;
import org.ffmpeg.api.dsl.SimpleFFmpegExecutorBuilder;
import org.ffmpeg.api.exception.FFmpegRuntimeException;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.Spy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.dsl.FFmpegExecutorBuilderFactoryImpl.newExecutorBuilderFactory;

public class DefaultFFmpegExecutorBuilderFactoryUnitTest extends AbstractFFmpegTest {

    @Spy
    private final FFmpegExecutorFactory executorFactory = new DefaultFFmpegExecutorFactory(true, getExecutorPath());

    @Test(expected = NullPointerException.class)
    public void givenFactory_whenPassingNullExecutorFactory_thenThrowException() {
        //act
        newExecutorBuilderFactory(null);
    }

    @Test(expected = FFmpegRuntimeException.class)
    public void givenFactory_whenCreatinUnsupportedClass_thenThrowException() {
        //act
        newExecutorBuilderFactory(executorFactory).ofType(FFmpegExecutorBuilder.class);
    }

    @Test
    public void givenFactory_whenCreatingGenericExecutorBuilder_thenReturnInstance() {
        //act
        SimpleFFmpegExecutorBuilder builder = newExecutorBuilderFactory(executorFactory).ofType(SimpleFFmpegExecutorBuilder.class);

        //check
        assertThat(builder).isNotNull();
    }

    @Test
    public void givenFactory_whenCreatingNonGenericExecutorBuilder_thenReturnInstance() {
        //act
        ComplexFFmpegExecutorBuilder builder = newExecutorBuilderFactory(executorFactory).ofType(ComplexFFmpegExecutorBuilder.class);

        //check
        assertThat(builder).isNotNull();
    }

    @Test
    public void givenFactory_whenCreatingRawExecutorBuilder_thenReturnInstance() {
        //act
        RawFFmpegExecutorBuilder builder = newExecutorBuilderFactory(executorFactory).ofType(RawFFmpegExecutorBuilder.class);

        //check
        assertThat(builder).isNotNull();
    }
}