package org.ffmpeg.dsl;

import org.ffmpeg.api.core.executor.FFmpegExecutor;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.options.FFmpegOptionLocation;
import org.ffmpeg.api.exception.FFmpegRuntimeException;
import org.ffmpeg.core.executor.DefaultFFmpegExecutorFactory;
import org.ffmpeg.test.AbstractFFmpegTest;
import org.junit.Test;
import org.mockito.Spy;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ffmpeg.api.core.options.MainOption.FRAMES;
import static org.ffmpeg.api.core.options.MainOption.INPUT_SOURCE;
import static org.ffmpeg.api.core.options.MainOption.OVERWRITE_OUTPUT_FILES;
import static org.ffmpeg.api.core.options.MainOption.SEEK_POSITION;
import static org.ffmpeg.api.core.options.MainOption.STDIN_DISABLED;

public class ComplexFFmpegExecutorBuilderUnitTest extends AbstractFFmpegTest {

    private static final String SAMPLE_FULL_PATH = Paths.get(System.getProperty("user.dir")).getParent()
            .toAbsolutePath().toString().concat("/etc/data/audio/sample1.flac");

    @Spy
    private final FFmpegExecutorFactory executorFactory = new DefaultFFmpegExecutorFactory(true, getExecutorPath());

    @Test
    public void givenCompleteBuilder_whenGettingCommands_thenReturnCommandsInCorrectOrder() {
        FFmpegExecutor executor = ComplexFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .registerOption(SEEK_POSITION, 0, FFmpegOptionLocation.INPUT) //INPUT - OUTPUT
                .registerOption(SEEK_POSITION, 0, FFmpegOptionLocation.OUTPUT) //INPUT - OUTPUT
                .registerOption(FRAMES, 1024) //OUTPUT
                .registerOption(INPUT_SOURCE, getDefaultAudioSample().toString()) //INPUT
                .registerOption(STDIN_DISABLED) //ANYWHERE
                .registerOption(OVERWRITE_OUTPUT_FILES) //GLOBAL
                .registerOutputOption("-output", 3) //OUTPUT
                .registerInputOption("-input", 2) //INPUT
                .registerGlobalOption("-global", 1) //GLOBAL
                .registerInputOption("-i", SAMPLE_FULL_PATH)
                .withOutputPath("sample.flac")
                .build();

        assertThat(executor).isNotNull();
        assertThat(executor.getCommands()).containsExactly(
                "-loglevel", "level+repeat+info",
                "-nostdin",
                "-y",
                "-global", "1",
                "-ss", "0",
                "-input", "2",
                "-i", SAMPLE_FULL_PATH,
                "-ss", "0",
                "-frames", "1024",
                "-output", "3",
                "sample.flac"
        );
    }

    @Test(expected = NullPointerException.class)
    public void givenNoExecutorFactorySet_whenCreatingExecutor_thenThrowException() {
        ComplexFFmpegExecutorBuilderImpl.builder()
                .and()
                .registerOption(INPUT_SOURCE, getDefaultAudioSample().toString())
                .build();
    }

    @Test(expected = FFmpegRuntimeException.class)
    public void givenNoCommandsSet_whenCreatingExecutor_thenThrowException() {
        ComplexFFmpegExecutorBuilderImpl.builder()
                .withExecutorFactory(executorFactory)
                .and()
                .build();
    }
}
