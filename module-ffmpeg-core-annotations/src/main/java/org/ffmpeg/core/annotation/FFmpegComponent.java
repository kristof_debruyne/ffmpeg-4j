package org.ffmpeg.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * FFmpeg component.
 *
 * @author Sikke303
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FFmpegComponent {

    /**
     * Name of the component.
     *
     * @return name (required)
     */
    String name();

    /**
     * Description of the component.
     *
     * @return description (required)
     */
    String description();

    /**
     * External documentation link.
     *
     * @return link (optional)
     */
    String link() default "";
}
