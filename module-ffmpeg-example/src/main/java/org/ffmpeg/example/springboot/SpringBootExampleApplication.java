package org.ffmpeg.example.springboot;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.audio.AudioAttributes;
import org.ffmpeg.audio.AudioProcessAttributes;
import org.ffmpeg.core.process.DefaultFFmpegProcessRequest;
import org.ffmpeg.rx.api.context.ReactiveFFmpegContext;
import org.ffmpeg.video.VideoAttributes;
import org.ffmpeg.video.VideoProcessAttributes;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import javax.annotation.Nullable;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.ffmpeg.api.core.io.FFmpegFileSystem.createTempFile;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class SpringBootExampleApplication implements ApplicationRunner {

    @NonNull
    private final FFmpegContext context;

    @NonNull
    private final ReactiveFFmpegContext reactiveContext;

    public static void main(@Nullable final String[] args) { new SpringApplicationBuilder(SpringBootExampleApplication.class).run(args); }

    @Override
    public void run(@NonNull final ApplicationArguments args) {
        log.info("FFmpeg version: {}", context.getPlatform().getFramework().getVersion());

        audioExample();
        videoExample();
    }

    private void audioExample() {
        Path source = resolveDataResource("audio/sample1.flac");
        Path target = Paths.get(System.getProperty("user.dir").concat("/target/example.mp3"));

        DefaultFFmpegProcessRequest request = DefaultFFmpegProcessRequest.builder()
                .source(source)
                .target(target)
                .overwriteOutputFiles(true)
                .attributes(AudioProcessAttributes.builder()
                        .format("mp3")
                        .audioAttributes(AudioAttributes.builder()
                                .bitRate(320)
                                .sampleRate(44100)
                                .channels(2)
                                .codec("mp3")
                                .build()
                        ).build()
                )
                .build();

        context.getAudioProcessor().ifPresent(processor -> {
            processor.getAudioCodecs().forEach(codec -> log.info("Audio codec -> {}", codec));
            processor.getAudioDecoders().forEach(decoder -> log.info("Audio decoder -> {}", decoder));
            processor.getAudioEncoders().forEach(encoder -> log.info("Audio encoder -> {}", encoder));
            processor.process(request)
                    .map(FFmpegProcessResponse::isSuccess)
                    .orElseThrow(() -> new RuntimeException("Failed to process audio file !"));
        });
    }

    private void videoExample() {
        Path source = resolveDataResource("video/sample1.avi");
        Path target = Paths.get(System.getProperty("user.dir").concat("/target/example.avi"));

        DefaultFFmpegProcessRequest request = DefaultFFmpegProcessRequest.builder()
                .source(source)
                .target(target)
                .target(createTempFile("test_", ".mp3"))
                .overwriteOutputFiles(true)
                .attributes(VideoProcessAttributes.builder()
                        .format("avi")
                        .videoAttributes(VideoAttributes.builder().codec("copy").build())
                        .build()
                )
                .addGlobalOption("-vsync", 2)
                .build();

        context.getVideoProcessor().ifPresent(processor -> {
            processor.getVideoCodecs().forEach(codec -> log.info("Video codec -> {}", codec));
            processor.getVideoDecoders().forEach(decoder -> log.info("Video decoder -> {}", decoder));
            processor.getVideoEncoders().forEach(encoder -> log.info("Video encoder -> {}", encoder));
            processor.process(request)
                    .map(FFmpegProcessResponse::isSuccess)
                    .orElseThrow(() -> new RuntimeException("Failed to process video file !"));
        });
    }

    private Path resolveDataResource(@NonNull final String resourcePath) {
        return Paths.get(Paths.get(System.getProperty("user.dir")).getParent().toAbsolutePath().toString().concat(String.format("/etc/data/%s", resourcePath)));
    }
}
