package org.ffmpeg.example.vanilla;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.ffmpeg.api.config.FFmpegProperties;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.FFmpeg;
import org.ffmpeg.api.core.FFmpegPlatform;
import org.ffmpeg.api.core.executor.FFmpegExecutorFactory;
import org.ffmpeg.api.core.process.FFmpegProcessListener;
import org.ffmpeg.api.core.process.FFmpegProcessorFactory;
import org.ffmpeg.api.core.toolkit.FFmpegToolkit;
import org.ffmpeg.api.extension.FFmpegExtension;
import org.ffmpeg.api.extension.play.FFplay;
import org.ffmpeg.api.extension.probe.FFprobe;
import org.ffmpeg.api.extension.probe.multimedia.ProbeMultiMediaResponse;
import org.ffmpeg.api.extension.probe.output.OutputFormatParser;
import org.ffmpeg.api.extension.probe.output.OutputFormatParserRegistry;
import org.ffmpeg.api.multimedia.MultiMediaParser;
import org.ffmpeg.config.FFmpegBootstrapConfigurer;
import org.ffmpeg.config.FFplayBootstrapConfigurer;
import org.ffmpeg.config.FFprobeBootstrapConfigurer;
import org.ffmpeg.extension.probe.multimedia.ProbeMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaRequest;
import org.ffmpeg.multimedia.DefaultMultiMediaResponse;
import org.ffmpeg.spring.config.DefaultFFmpegProperties;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;

@Parameters(separators = "=")
@Slf4j
public class VanillaExampleApplication extends Thread implements FFmpegBootstrapConfigurer, FFplayBootstrapConfigurer, FFprobeBootstrapConfigurer {

    @Getter
    private FFmpegProperties properties;

    @Parameter(names = "-ffmpeg", required = true)
    private String ffmpegExecutorPath;

    @Parameter(names = "-ffprobe")
    private String ffprobeExecutorPath;

    @Parameter(names = "-ffplay")
    private String ffplayExecutorPath;

    public static void main(@Nullable final String[] args) {
        VanillaExampleApplication application = new VanillaExampleApplication();
        JCommander.newBuilder()
                .addObject(application)
                .build()
                .parse(args);

        application.start();
    }

    @Override
    public void run() {
        try {
            properties = createProperties();

            FFmpegExecutorFactory executorFactory = createExecutorFactory();
            FFmpegToolkit toolkit = createToolkit(executorFactory);
            MultiMediaParser<DefaultMultiMediaRequest, DefaultMultiMediaResponse> multiMediaParser = createMultiMediaParser(executorFactory, toolkit);
            FFmpegProcessListener processListener = createProcessListener(toolkit);
            FFmpeg framework = createFFmpegFramework(executorFactory, processListener, toolkit, multiMediaParser);
            FFmpegPlatform platform = configurePlatform(framework, toolkit);
            FFmpegProcessorFactory processorFactory = createProcessorFactory(platform);
            FFmpegContext context = createContext(platform, executorFactory, processorFactory);

            log.info("FFmpeg version: {}", context.getPlatform().getFramework().getVersion());
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
        }
    }

    @Nonnull
    private FFmpegProperties createProperties() {
        DefaultFFmpegProperties properties = new DefaultFFmpegProperties();
        properties.setRedirectErrorStream(true);
        properties.setExecutorPath(ffmpegExecutorPath);
        if(nonNull(ffprobeExecutorPath)) {
            properties.setProbeExecutorPath(ffprobeExecutorPath);
        }
        if(nonNull(ffplayExecutorPath)) {
            properties.setPlayExecutorPath(ffplayExecutorPath);
        }
        return properties;
    }

    @Nonnull
    private FFmpegPlatform configurePlatform(@NonNull FFmpeg framework,
                                             @NonNull FFmpegToolkit toolkit) {
        Set<FFmpegExtension> extensions = new HashSet<>();
        ofNullable(ffprobeExecutorPath).flatMap(path -> ofNullable(configureProbe(toolkit))).ifPresent(extensions::add);
        ofNullable(ffplayExecutorPath).flatMap(path -> ofNullable(configurePlay(toolkit))).ifPresent(extensions::add);
        return createPlatform(framework, toolkit, extensions);
    }

    @Nullable
    private FFprobe configureProbe(@NonNull FFmpegToolkit toolkit) {
        try {
            FFmpegExecutorFactory probeExecutorFactory = createProbeExecutorFactory();
            ObjectMapper objectMapper = new ObjectMapper(); //JSON
            Schema schema = createSchema("/ffprobe.xsd"); //XML
            DocumentBuilderFactory documentBuilderFactory = createDocumentBuilderFactory(schema); //XML
            OutputFormatParserRegistry<OutputFormatParser> registry = createOutputFormatParserRegistry(toolkit, objectMapper, documentBuilderFactory, schema); //XML
            MultiMediaParser<ProbeMultiMediaRequest, ProbeMultiMediaResponse> probeMultiMediaParser = createProbeMultiMediaParser(probeExecutorFactory, registry);
            return createProbeFramework(probeExecutorFactory, toolkit, probeMultiMediaParser);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nullable
    private FFplay configurePlay(@NonNull FFmpegToolkit toolkit) {
        FFmpegExecutorFactory playExecutorFactory = createPlayExecutorFactory();
        return createPlayFramework(playExecutorFactory, toolkit);
    }
}
