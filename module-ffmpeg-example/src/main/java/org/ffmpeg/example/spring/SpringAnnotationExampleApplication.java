package org.ffmpeg.example.spring;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.spring.config.EnableFFmpeg;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.annotation.Nullable;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class SpringAnnotationExampleApplication extends Thread {

    public static void main(@Nullable final String[] args) { new SpringAnnotationExampleApplication().start(); }

    @Override
    public void run() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringExampleConfiguration.class);
        FFmpegContext context = applicationContext.getBean(FFmpegContext.class);

        log.info("FFmpeg version: {}", context.getPlatform().getFramework().getVersion());
    }

    @EnableFFmpeg
    public static class SpringExampleConfiguration { }
}
