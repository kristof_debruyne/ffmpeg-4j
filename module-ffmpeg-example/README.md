How to run the examples ?

Create a build configuration in your IDE.

# Supported VM options

### BEAN OVERRIDING (SPRING)

-Dspring.main.allow-bean-definition-overriding=true (required) 

Since we have 2 Spring dependencies in our project (spring and spring starter) we need to enable bean overriding.

### FFPMPEG_HOME
-DFFMPEG_HOME=/Users/sikke303/dev/home/ffmpeg-4j/bin/ffmpeg (required)
 
(Path to your FFMPEG executable which you downloaded from the website) 

### FFPROBE_HOME
-DFFPROBE_HOME=/Users/sikke303/dev/home/ffmpeg-4j/bin/ffprobe (optional)

(Path to your FFPROBE executable which you downloaded from the website)

### JAVA AGENT 
-javaagent:/Users/sikke303/.m2/repository/org/springframework/spring-instrument/5.3.1/spring-instrument-5.3.1.jar (optional)

If you want to enable aspects abilities: pass agent

If you want to disable aspects capabilities: do not pass agent