# README #

FFMPEG wrapper for Java with Spring Boot support which allows you to interact with FFMPEG through Java API very easily.

### FFMPEG ###

Initial release: 20/12/2000
Current version: 4.4-tessus

Platform support: Linux, Mac OS and Windows

Written in: Assembly / C

Website: https://www.ffmpeg.org

FFmpeg is a free and open-source software project consisting of a large suite of libraries and programs for handling video, audio, 
and other multimedia files and streams.

At its core is the FFmpeg program itself, designed for command-line-based processing of video and audio files.

You will need to download the binary for the operating system you are using. This is the only requirement in order to use the wrapper properly.

### Module overview ###

* **module-ffmpeg-aop**: module for making the codebase Aspect Oriented Programming aware (Spring only)
* **module-ffmpeg-bom**: module for managing dependencies and versions
* **module-ffmpeg-core-annotations**: module containing annotations
* **module-ffmpeg-core-api**: module containing interfaces, models and generic components
* **module-ffmpeg-core-rx**: module containing core reactive components for RX
* **module-ffmpeg-core-spring**: module containing core components for Spring
* **module-ffmpeg-core-xsd**: module containing XSD schema's for ffprobe
* **module-ffmpeg-documentation**: module containing documentation (release notes / cookbook)
* **module-ffmpeg-example**: module containing examples on how to use the wrapper (vanilla / spring / spring boot)
* **module-ffmpeg-starter-spring**: module containing bootstrap which you can use from your Spring application
* **module-ffmpeg-starter-springboot**: module containing bootstrap which you can use from your Spring Boot application
* **module-ffmpeg-test**: module containing test capabilities
* **module-ffmpeg-wrapper**: module containing wrapper implementation

### Build ###

MAVEN
- `mvn clean install`

GRADLE
- `gradle clean build`

Since we import a Maven BOM into Gradle, it resolves the BOM from a repository and won't build it (like in Maven build).
To not have any trouble, you have to install the 'module-ffmpeg-bom' into your local repository first before building project.

### Development Requirements ###

- Java 17 (or higher)
- Maven 3.6.0 (or higher)
- IntelliJ IDE + Lombok plugin
- Enable annotation processing

### Executables ###

Currently, the wrapper supports:

- ffmpeg: can be initialized by setting environment variable or passing system property 'FFMPEG_HOME'
- ffprobe: can be initialized by setting environment variable or passing system property 'FFPROBE_HOME'
- ffplay (experimental): can be initialized by setting environment variable or passing system property 'FFPLAY_HOME'

Most likely in the future, more ffmpeg cli tools might be supported.

### Java Agent ###

If you intend to use Spring with the wrapper, you can also use the AOP package.
You will need to pass a Java agent on startup of application. If not passed, no AOP is loaded.

E.g -javaagent:/Users/sikke303/.m2/repository/org/springframework/spring-instrument/5.3.1/spring-instrument-5.3.1.jar

### Reactive Support ###

If you intend to use reactive programming in your application. The wrapper has support for it.
When using the Spring or Spring Boot started, the bootstrapping is done automatically.

Just add the folloing dependency to your project:

```
<dependency>
    <groupId>io.reactivex.rxjava3</groupId>
    <artifactId>rxjava</artifactId>
    <version>3.x.x</version>
</dependency>
```

Be aware, only RX Java version 3 is supported (for now).

### Examples ###

See module examples on how to use the FFMPEG wrapper.

- VanillaExampleApplication: if you use just native Java in your application
- SpringExampleApplication: if you use Spring in your application
- SpringBootExampleApplication: if you use Spring Boot in your application

### Testing ###

- unit test: mvn clean install
- integration test: mvn clean install -Pintegration-test -DFFMPEG_HOME=/Users/sikke303/dev/home/ffmpeg-4j/bin/ffmpeg -DFFPROBE_HOME=/Users/sikke303/dev/home/ffmpeg-4j/bin/ffprobe

### Roadmap ###

- implement all options + tested
- support for newer Java versions
- support ffplay
- support other ffmpeg tools
- ...

### Release history ###

- v1.0: initial release (master)

### Maven Central ###

Currently, the artifacts aren't on the Maven Central repository. Most likely this will be added in the near future.
For now, you'll need to build it yourself. Sorry :-)

### Contact ###

You can always contact me through email: kristof.debruyne@hotmail.com