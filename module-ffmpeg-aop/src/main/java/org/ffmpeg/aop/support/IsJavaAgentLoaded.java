package org.ffmpeg.aop.support;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.instrument.InstrumentationSavingAgent;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Condition that checks if an agent is loaded or not.
 *
 * The following class needs to be loaded:
 * - {@link org.springframework.instrument.InstrumentationSavingAgent}
 *
 * @author Sikke303
 * @since 1.0
 * @see Condition
 */
@ParametersAreNonnullByDefault
@Slf4j
public class IsJavaAgentLoaded implements Condition {

    @Override
    public boolean matches(@NonNull ConditionContext context, @NonNull AnnotatedTypeMetadata metadata) {
        try {
            Class.forName(InstrumentationSavingAgent.class.getName());
        } catch (Throwable ex) {
            log.info("Java agent not loaded ! Aspect configuration is disabled.");
            return false;
        }
        log.info("Java agent loaded ! Aspect configuration is enabled.");
        return true;
    }
}