package org.ffmpeg.aop.support;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.bridge.AbortException;
import org.aspectj.bridge.IMessage;
import org.aspectj.bridge.IMessageHandler;

/**
 * Load time weaver (LTW) message handler. <br><br>
 *
 * This allows you to forward the console output to a logger instead. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see IMessageHandler
 */
@Slf4j
public class LoadTimeWeaverMessageHandler implements IMessageHandler {

    @Override
    public boolean handleMessage(@NonNull final IMessage message) throws AbortException {
        if(log.isTraceEnabled()) {
            log.trace("{}", message.getMessage());
        }
        return true;
    }

    @Override
    public boolean isIgnoring(@NonNull final IMessage.Kind kind) {
        return false;
    }

    @Override
    public void dontIgnore(@NonNull final IMessage.Kind kind) {

    }

    @Override
    public void ignore(@NonNull final IMessage.Kind kind) {

    }
}
