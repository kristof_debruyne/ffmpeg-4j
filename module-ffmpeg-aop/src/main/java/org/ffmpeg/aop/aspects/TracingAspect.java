package org.ffmpeg.aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.ffmpeg.core.annotation.Tracing;
import org.springframework.util.StopWatch;

/**
 * Tracing aspect.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractAspect
 * @see Tracing
 */
@Aspect
@Slf4j
public class TracingAspect extends AbstractAspect {

    @Pointcut("execution(public * org.ffmpeg..*(..)) || execution(protected * org.ffmpeg..*(..))")
    void scope() { }

    @Pointcut("@annotation(tracing)")
    void annotation(Tracing tracing) { }

    @Around(value = "scope() && annotation(tracing)", argNames = "joinPoint,tracing")
    public Object advice(ProceedingJoinPoint joinPoint, Tracing tracing) throws Throwable {
        StopWatch timer = new StopWatch("FFmpegTrace");
        if(log.isTraceEnabled()) {
            log.trace("\n>>>>>>>>>>>>>>> INVOKING HANDLER >>>>>>>>>>>>>>> | method={}(), args={} >>>>>",
                    joinPoint.getSignature().getName(), joinPoint.getArgs());
        }
        timer.start(); //START
        Object object = joinPoint.proceed(); //PROCEED
        timer.stop(); //STOP
        if(log.isTraceEnabled()) {
            log.trace("\n<<<<<<<<<<<<<<< INVOKED HANDLER <<<<<<<<<<<<<<< | method={}(), invocation took about {} ms <<<<<",
                    joinPoint.getSignature().getName(), timer.getTotalTimeMillis());
        }
        return object;
    }
}