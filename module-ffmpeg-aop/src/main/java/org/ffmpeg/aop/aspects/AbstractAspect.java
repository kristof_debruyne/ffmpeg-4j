package org.ffmpeg.aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * Abstract aspect.
 *
 * @author Sikke303
 * @since 1.0
 */
@Aspect
@Slf4j
public abstract class AbstractAspect {

    /**
     * Defines the scope of the aspect.
     */
    @Pointcut
    abstract void scope();
}
