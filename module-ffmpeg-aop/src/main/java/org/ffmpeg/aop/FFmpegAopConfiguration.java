package org.ffmpeg.aop;

import lombok.extern.slf4j.Slf4j;
import org.ffmpeg.aop.support.IsJavaAgentLoaded;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.context.annotation.LoadTimeWeavingConfigurer;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;

import javax.annotation.Nonnull;

/**
 * Spring AOP configuration. <br><br>
 *
 * As we use load time weaving (LTW) to get the aspect right on non Spring managed beans, <br>
 * we are compelled to use instrumentation (java.lang.instrument package) classes. <br><br>
 *
 * Because of that, we need to start with a Java agent. <br><br>
 *
 * E.g. -javaagent:/Users/sikke303/.m2/repository/org/springframework/spring-instrument/5.3.1/spring-instrument-5.3.1.jar <br><br>
 *
 * AspectJ weaving is set to enabled as we provide an 'aop.xml' in the META-INF folder on the classpath. <br>
 * More information about load time weaving (LTW) can be found in the Eclipse foundation web page. <br><br>
 *
 * Website: https://www.eclipse.org/aspectj/doc/released/devguide/ltw-configuration.html
 *
 * As the instrumentation jar is scope provided, we can check if an agent is loaded or not.
 *
 * @author Sikke303
 * @since 1.0
 * @see LoadTimeWeavingConfigurer
 */
@Configuration
@Conditional(IsJavaAgentLoaded.class)
@ComponentScan("org.ffmpeg.aop.aspects")
@EnableAspectJAutoProxy
@EnableLoadTimeWeaving(aspectjWeaving = EnableLoadTimeWeaving.AspectJWeaving.ENABLED)
@Slf4j
public class FFmpegAopConfiguration implements LoadTimeWeavingConfigurer {

    @Nonnull
    @Override
    public LoadTimeWeaver getLoadTimeWeaver() {
        log.trace("Creating new load time weaver of type: {}", InstrumentationLoadTimeWeaver.class.getName());
        return new InstrumentationLoadTimeWeaver();
    }

}
