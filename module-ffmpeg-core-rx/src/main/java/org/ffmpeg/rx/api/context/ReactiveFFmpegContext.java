package org.ffmpeg.rx.api.context;

import io.reactivex.rxjava3.core.Scheduler;
import org.ffmpeg.api.context.FFmpegContext;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.core.annotation.FFmpegComponent;
import org.ffmpeg.rx.api.processor.ReactiveFFmpegProcessor;
import org.ffmpeg.rx.api.queue.ReactiveFFmpegQueueManager;

import javax.annotation.Nonnull;

/**
 * Reactive FFmpeg context interface.
 *
 * @author Sikke303
 * @since 2.0
 * @see FFmpegContext
 * @see ReactiveFFmpegProcessor
 */
@FFmpegComponent(
        name = "Reactive FFmpeg context",
        description = "Each application should have a context running for interacting with FFMPEG."
)
public interface ReactiveFFmpegContext {

    /**
     * Gets FFmpeg context.
     *
     * @return context (never null)
     */
    @Nonnull
    FFmpegContext getContext();

    /**
     * Gets reactive processor.
     *
     * @param <Request> request type
     * @param <Response> response type
     * @return processor (never null)
     */
    @Nonnull
    <Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> ReactiveFFmpegProcessor<Request, Response> getProcessor();

    /**
     * Gets reactive queue manager.
     *
     * @param <Request> request type
     * @return queue manager (never null)
     */
    @Nonnull
    <Request extends FFmpegProcessRequest, Observer> ReactiveFFmpegQueueManager<Request> getQueueManager();

    /**
     * Gets scheduler (default).
     *
     * @return scheduler (never null)
     */
    @Nonnull
    Scheduler getScheduler();
}