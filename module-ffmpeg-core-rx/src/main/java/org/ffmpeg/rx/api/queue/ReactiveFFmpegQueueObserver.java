package org.ffmpeg.rx.api.queue;

import io.reactivex.rxjava3.core.Observer;
import org.ffmpeg.core.annotation.FFmpegComponent;

/**
 * Reactive FFmpeg queue observer interface. <br>
 *
 * @author Sikke303
 * @since 2.0
 * @param <T> type
 */
@FFmpegComponent(
        name = "Reactive FFmpeg queue observer",
        description = "A reactive queue observer allows you to process the results."
)
public interface ReactiveFFmpegQueueObserver<T> extends AutoCloseable, Observer<T> {

    /**
     * Checks if queue is currently idle or not.
     *
     * @return boolean idle (true) or not (false)
     */
    boolean isIdle();

    /**
     * Checks if queue is currently processing any items or not.
     *
     * @return boolean processing (true) or not (false)
     */
    boolean isProcessing();
}