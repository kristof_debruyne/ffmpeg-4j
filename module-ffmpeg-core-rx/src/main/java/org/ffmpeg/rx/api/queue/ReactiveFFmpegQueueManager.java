package org.ffmpeg.rx.api.queue;

import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Reactive FFmpeg queue manager interface. <br>
 *
 * @author Sikke303
 * @since 2.0
 * @param <Request> request type
 */
@FFmpegComponent(
        name = "Reactive FFmpeg queue manager",
        description = "A reactive queue manager allows you to queue requests for processing."
)
public interface ReactiveFFmpegQueueManager<Request extends FFmpegProcessRequest> extends AutoCloseable {

    /**
     * Queues request for processing.
     *
     * @param request request (required)
     */
    void queue(@Nonnull Request request);

    /**
     * Queuest requests for processing.
     *
     * @param requests requests (required)
     */
    void queue(@Nonnull Collection<Request> requests);
}