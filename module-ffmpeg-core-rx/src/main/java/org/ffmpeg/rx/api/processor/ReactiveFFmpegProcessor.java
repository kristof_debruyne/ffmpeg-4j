package org.ffmpeg.rx.api.processor;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import org.ffmpeg.api.core.process.FFmpegProcessRequest;
import org.ffmpeg.api.core.process.FFmpegProcessResponse;
import org.ffmpeg.core.annotation.FFmpegComponent;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

/**
 * Reactive FFmpeg processor interface. <br>
 *
 * @author Sikke303
 * @since 2.0
 */
@FFmpegComponent(
        name = "Reactive FFmpeg processor",
        description = "A reactive processor allows you to process in a reactive fashion (parallel)."
)
@FunctionalInterface
public interface ReactiveFFmpegProcessor<Request extends FFmpegProcessRequest, Response extends FFmpegProcessResponse> {

    /**
     * Gets available processors.
     *
     * @return int available processors (required)
     */
    default int getAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * Processes given requests on given scheduler with backpressure (flowable).
     *
     * @param supplier flowable supplier (required)
     * @param scheduler scheduler (required)
     * @return flowable (never null)
     */
    @Nonnull
    default Flowable<Response> process(@Nonnull Supplier<Flowable<Request>> supplier, @Nonnull Scheduler scheduler) {
        return process(supplier, scheduler, getAvailableProcessors());
    }

    /**
     * Processes given requests on given scheduler with backpressure (flowable).
     *
     * @param supplier flowable supplier (required)
     * @param scheduler scheduler (required)
     * @param parallelism parallel count (at least 1)
     * @return flowable (never null)
     */
    @Nonnull
    Flowable<Response> process(@Nonnull Supplier<Flowable<Request>> supplier, @Nonnull Scheduler scheduler, int parallelism);
}